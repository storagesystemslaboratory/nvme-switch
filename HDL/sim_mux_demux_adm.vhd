--------------------------------------------------------------------------------
--
-- Module Name: sim_mux_demux_adm
-- Create Date: 09/06/2019 (MM/DD/YYYY)
--
-- Company: ittChip - Unisinos
-- Engineer: Vinícius Gabriel LINDEN
-- 
-- Description: Stimulae for the mux demux adm submodule
-- 
--------------------------------------------------------------------------------
-- Recommended span of 300 ns for period of 10 ns
-- TODO:


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
--use IEEE.NUMERIC_STD.ALL;

library UNISIM;
use UNISIM.VComponents.all;

--library STD;
--use STD.TEXTIO.ALL;

--library work;
--use work.kratus_pkg.all;

entity sim_mux_demux_adm is
	end sim_mux_demux_adm;


architecture Behavioral of sim_mux_demux_adm is

	-- DO NOT DELETE
	constant period 				: time := 5 ns;
	constant hp 					: time := period/2; -- half period

	signal index 					: natural := 1; -- index for redability
	signal clk 						: std_logic := '1'; -- system base clock

	procedure wait_clk(constant T : in natural) is
	begin
		wait for t*period;
	end procedure;
	-- DELETION RESTRICTION ENDED

	-- UUT
	constant GENERIC_PRIORITARY 	: boolean := false;
	constant CLOCK_EDGE 			: std_logic := '1';
	signal arb_ask 					: std_logic := '0';
	signal arb_lock 				: std_logic;
	signal arb_flow 				: std_logic := '0';
	signal generic_ask 				: std_logic := '0';
	signal generic_lock 			: std_logic;
	signal generic_flow 			: std_logic := '0';
	signal host2ssd 				: std_logic;
	signal rst 						: std_logic := '1';
	-- END OF UUT

	-- SIMULATION
	signal prioritary_ask 			: std_logic := '0';
	signal prioritary_lock 			: std_logic;
	signal prioritary_flow 			: std_logic := '0';
	signal secondary_ask 			: std_logic := '0';
	signal secondary_lock 			: std_logic;
	signal secondary_flow 			: std_logic := '0';
	signal test_case 				: string(1 to 20) := "waiting             ";
-- END OF SIMULATION

begin

	UUT : entity work.rtl_mux_demux_adm
	generic map
	(
		GENERIC_PRIORITARY => GENERIC_PRIORITARY,
		CLOCK_EDGE => CLOCK_EDGE
	)
	port map
	(
		arb_ask => arb_ask,
		arb_lock => arb_lock,
		arb_flow => arb_flow,
		generic_ask => generic_ask,
		generic_lock => generic_lock,
		generic_flow => generic_flow,
		host2ssd => host2ssd,
		rst => rst,
		clk => clk
	);

	-- DO NOT DELETE
	clk <= not clk after hp;
	index <= index + 1 after period;
	-- DELETION RESTRICTION ENDED
	
	ARBITRATOR_FIRST_SIM : if GENERIC_PRIORITARY = false generate
		arb_ask <= prioritary_ask;
		prioritary_lock <= arb_lock;
		arb_flow <= prioritary_flow;
		generic_ask <= secondary_ask;
		secondary_lock <= generic_lock;
		generic_flow <= secondary_flow;
	end generate ARBITRATOR_FIRST_SIM;

	GENERIC_FIRST_SIM : if GENERIC_PRIORITARY = true generate
		generic_ask <= prioritary_ask;
		prioritary_lock <= generic_lock;
		generic_flow <= prioritary_flow;
		arb_ask <= secondary_ask;
		secondary_lock <= arb_lock;
		arb_flow <= secondary_flow;
	end generate GENERIC_FIRST_SIM;

	secondary_flow <= not prioritary_flow;

	process
	begin
		report "simulation started";
		wait_clk(2);
		rst <= '0';

		test_case <= "clear               ";
		wait_clk(10);

		test_case <= "idle                ";
		wait_clk(1);
		report test_case;
		wait_clk(1);
		prioritary_ask <= '0';
		secondary_ask <= '0';
		wait_clk(2);
		assert prioritary_lock = '1' report "prioritary isn't locked" severity warning;
		assert secondary_lock = '0' report "secondary is locked" severity warning;
		assert host2ssd = prioritary_flow report "host2ssd is not inheriting prioritary" severity warning;
		wait_clk(1);
		prioritary_flow <= '0';
		assert host2ssd = prioritary_flow report "host2ssd is not inheriting prioritary" severity warning;

		test_case <= "clear               ";
		wait_clk(10);

		test_case <= "prioritary lock     ";
		wait_clk(1);
		report test_case;
		wait_clk(1);
		prioritary_ask <= '1';
		prioritary_flow <= '1';
		secondary_ask <= '0';
		wait_clk(2);
		assert prioritary_lock = '1' report "prioritary isn't locked" severity warning;
		assert secondary_lock = '0' report "secondary is locked" severity warning;
		assert host2ssd = prioritary_flow report "host2ssd is not inheriting prioritary" severity warning;
		wait_clk(1);
		prioritary_flow <= '0';
		wait_clk(1);
		assert host2ssd = prioritary_flow report "host2ssd is not inheriting prioritary" severity warning;
		wait_clk(1);
		prioritary_ask <= '0';

		test_case <= "clear               ";
		wait_clk(10);

		test_case <= "secondary l. no race";
		wait_clk(1);
		report test_case;
		wait_clk(1);
		prioritary_ask <= '0';
		prioritary_flow <= '1';
		secondary_ask <= '1';
		wait_clk(2);
		assert prioritary_lock = '0' report "prioritary is locked" severity warning;
		assert secondary_lock = '1' report "secondary isn't locked" severity warning;
		assert host2ssd = secondary_flow report "host2ssd is not inheriting secondary" severity warning;
		wait_clk(1);
		prioritary_flow <= '0';
		wait_clk(1);
		assert host2ssd = secondary_flow report "host2ssd is not inheriting secondary" severity warning;

		test_case <= "clear               ";
		wait_clk(10);

		test_case <= "secondary l. w/ race";
		wait_clk(1);
		report test_case;
		wait_clk(1);
		prioritary_ask <= '0';
		secondary_ask <= '0';
		wait_clk(2);
		assert prioritary_lock = '1' report "prioritary isn't locked" severity warning;
		assert secondary_lock = '0' report "secondary is locked" severity warning;
		wait_clk(1);
		prioritary_ask <= '1';
		prioritary_flow <= '1';
		secondary_ask <= '1';
		wait_clk(2);
		assert prioritary_lock = '1' report "prioritary isn't locked" severity warning;
		assert secondary_lock = '0' report "secondary is locked" severity warning;
		assert host2ssd = prioritary_flow report "host2ssd is not inheriting prioritary" severity warning;
		wait_clk(1);
		secondary_ask <= '0';
		wait_clk(1);
		assert host2ssd = prioritary_flow report "host2ssd is not inheriting prioritary" severity warning;

		test_case <= "clear               ";
		wait_clk(10);

		test_case <= "leaving race cond. 2";
		wait_clk(1);
		report test_case;
		wait_clk(1);
		prioritary_ask <= '1';
		secondary_ask <= '0';
		wait_clk(2);
		assert prioritary_lock = '1' report "prioritary isn't locked" severity warning;
		assert secondary_lock = '0' report "secondary is locked" severity warning;

		test_case <= "clear               ";
		wait_clk(10);

		test_case <= "leaving race cond. 1";
		wait_clk(1);
		report test_case;
		wait_clk(1);
		prioritary_ask <= '1';
		secondary_ask <= '1';
		wait_clk(2);
		assert prioritary_lock = '1' report "prioritary isn't locked" severity warning;
		assert secondary_lock = '0' report "secondary is locked" severity warning;
		wait_clk(1);
		prioritary_ask <= '0';
		secondary_ask <= '1';
		wait_clk(2);
		assert prioritary_lock = '0' report "prioritary is locked" severity warning;
		assert secondary_lock = '1' report "secondary isn't locked" severity warning;
		assert host2ssd = secondary_flow report "host2ssd is not inheriting secondary" severity warning;
		wait_clk(1);
		prioritary_flow <= '0';
		wait_clk(1);
		assert host2ssd = secondary_flow report "host2ssd is not inheriting secondary" severity warning;

		test_case <= "clear               ";
		wait_clk(10);

		test_case <= "simulation ended    ";
		report "simulation ended";
		wait;
	end process;

end Behavioral;


