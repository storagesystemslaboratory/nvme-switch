--------------------------------------------------------------------------------
--
-- Module Name: rtl_full_comparison_operation
-- Create Date: 09/05/2019 (MM/DD/YYYY)
-- Revision: 1
--
-- Company: ittChip - Unisinos
-- Engineer: Vinícius Gabriel LINDEN
-- 
-- Description: Real-time logic for configurable comparison.
--              When using only one value, value2 is ignored.
--              When in range value1 is the lower and value2 is the upper limits.
-- 
--------------------------------------------------------------------------------
-- TODO:

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

library UNISIM;
use UNISIM.VComponents.all;

library work;
use work.operations_pkg.all;
--use work.NVMe_pkg.all;

entity rtl_full_comparison_operation is
	generic
	(
		DATA_WIDTH          : integer := 8; -- width of data [bit]
		VALUE_HIGH          : std_logic := '1'; -- value of output when comparison is true
		INVERT_WHEN_RANGE   : boolean := false -- invert the value then comparison is a range
	);
	port
	(
		-- CONFIGURATION
		config              : in std_logic_vector(7 downto 0); -- configuration for this module
		value1              : in std_logic_vector(DATA_WIDTH-1 downto 0); -- value for compairison
		value2              : in std_logic_vector(DATA_WIDTH-1 downto 0); -- value for compairison

		-- IO
		data                : in std_logic_vector(DATA_WIDTH-1 downto 0); -- data to compare
		oput                : out std_logic -- output
	 );
end rtl_full_comparison_operation;

architecture rtl of rtl_full_comparison_operation is

	-- CONSTANTS ARE DECLARED IN THE PACKAGE (operations_pkg)
	--constant MAKE_LOW                       : std_logic_vector := x"00"; -- force low
	--constant MAKE_HIGH                      : std_logic_vector := x"01"; -- force high
	--constant ASSERT_EQUAL                   : std_logic_vector := x"02"; -- data = value1
	--constant ASSERT_GREATER                 : std_logic_vector := x"03"; -- data > value1
	--constant ASSERT_GREATER_EQUAL           : std_logic_vector := x"04"; -- data >= value1
	--constant ASSERT_LESS                    : std_logic_vector := x"05"; -- data < value1
	--constant ASSERT_LESS_EQUAL              : std_logic_vector := x"06"; -- data <= value1
	--constant ASSERT_GREATER_LESS            : std_logic_vector := x"07"; -- value1 < data < value2
	--constant ASSERT_GREATER_EQUAL_LESS      : std_logic_vector := x"08"; -- value1 <= data < value2
	--constant ASSERT_GREATER_LESS_EQUAL      : std_logic_vector := x"09"; -- value1 < data <= value2
	--constant ASSERT_GREATER_EQUAL_LESS_EQ   : std_logic_vector := x"0A"; -- value1 <= data <= value2

	constant VALUE_LOW          : std_logic := not VALUE_HIGH;

	function inverted_range(x : boolean) return std_logic is
	begin
		if x then
			return VALUE_LOW;
		else
			return VALUE_HIGH;
		end if;
	end function inverted_range;

	constant RANGE_HIGH         : std_logic := inverted_range(INVERT_WHEN_RANGE);
	constant RANGE_LOW          : std_logic := not RANGE_HIGH;

	-- VALUE 1
	signal greater1             : std_logic; -- data > value1
	signal greater_equal1       : std_logic; -- data >= value1
	signal less1                : std_logic; -- data < value1
	signal less_equal1          : std_logic; -- data <= value1
	signal equal1               : std_logic; -- data = value1

	-- VALUE 2
	signal greater2             : std_logic; -- data > value2
	signal greater_equal2       : std_logic; -- data >= value2
	signal less2                : std_logic; -- data < value2
	signal less_equal2          : std_logic; -- data <= value2

	-- RANGE
	signal greater_less         : std_logic; -- value1 < data < value2   | (data > value1)  and (data < value2)
	signal greater_eq_less      : std_logic; -- value1 <= data < value2  | (data >= value1) and (data < value2)
	signal greater_less_eq      : std_logic; -- value1 < data <= value2  | (data > value1)  and (data <= value2)
	signal greater_eq_less_eq   : std_logic; -- value1 <= data <= value2 | (data >= value1) and (data <= value2)

	-- TRANSLATION TO VALUE_HIGH AND VALUE_LOW
	signal greater              : std_logic; -- same as greater1
	signal greater_eq           : std_logic; -- same as greater_equal1
	signal less                 : std_logic; -- same as less1
	signal less_eq              : std_logic; -- same as less_equal1
	signal equal                : std_logic; -- same as equal1

begin

	COMPARE_VALUE1 : entity work.rtl_compare_operation
	generic map
	(
		HAND_MADE => false,
		DATA_WIDTH => DATA_WIDTH
	)
	port map
	(
		value1 => data,
		value2 => value1,
		greater => greater1,
		greater_equal => greater_equal1,
		less => less1,
		less_equal => less_equal1,
		equal => equal1,
		different => open
	);

	COMPARE_VALUE2 : entity work.rtl_compare_operation
	generic map
	(
		HAND_MADE => false,
		DATA_WIDTH => DATA_WIDTH
	)
	port map
	(
		value1 => data,
		value2 => value2,
		greater => greater2,
		greater_equal => greater_equal2,
		less => less2,
		less_equal => less_equal2,
		equal => open,
		different => open
	);

	-- TRANSLATION TO VALUE_HIGH AND NONVALUE_HIGH
	equal <= VALUE_HIGH when equal1 = '1' else VALUE_LOW;
	greater <= VALUE_HIGH when greater1 = '1' else VALUE_LOW;
	greater_eq <= VALUE_HIGH when greater_equal1 = '1' else VALUE_LOW;
	less <= VALUE_HIGH when less1 = '1' else VALUE_LOW;
	less_eq <= VALUE_HIGH when less_equal1 = '1' else VALUE_LOW;

	-- RANGE
	greater_less <= RANGE_HIGH when (greater1 = '1') and (less2 = '1') else RANGE_LOW;
	greater_eq_less <= RANGE_HIGH when (greater_equal1 = '1') and (less2 = '1') else RANGE_LOW;
	greater_less_eq <= RANGE_HIGH when (greater1 = '1') and (less_equal2 = '1') else RANGE_LOW;
	greater_eq_less_eq <= RANGE_HIGH when (greater_equal1 = '1') and (less_equal2 = '1') else RANGE_LOW;

	with config select oput <=
		VALUE_LOW           when MAKE_LOW,
		VALUE_HIGH          when MAKE_HIGH,
		equal               when ASSERT_EQUAL,
		greater             when ASSERT_GREATER,
		greater_eq          when ASSERT_GREATER_EQUAL,
		less                when ASSERT_LESS,
		less_eq             when ASSERT_LESS_EQUAL,
		greater_less        when ASSERT_GREATER_LESS,
		greater_eq_less     when ASSERT_GREATER_EQUAL_LESS,
		greater_less_eq     when ASSERT_GREATER_LESS_EQUAL,
		greater_eq_less_eq  when ASSERT_GREATER_EQUAL_LESS_EQ,
		VALUE_LOW           when others;

end rtl;


