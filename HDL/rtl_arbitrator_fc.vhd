--------------------------------------------------------------------------------
--
-- Module Name: rtl_arbitrator_fc
-- Create Date: 01/07/2020 (MM/DD/YYYY)
-- Revision: 0
--
-- Company: ittChip - Unisinos
-- Engineer: Vinícius Gabriel LINDEN
-- 
-- Description: Real-time logic arbitrator_fc
-- 
--------------------------------------------------------------------------------
-- TODO:

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

library UNISIM;
use UNISIM.VComponents.all;

library work;
use work.common_pkg.all;

entity rtl_arbitrator_fc is
	generic
	(
		AC_CONFIGS		: natural := 1; -- number of parallel field captures
		CLOCK_EDGE		: std_logic := '1' -- '1' for rising, '0' for falling
	);
	port
	(
		-- CONFIGURATION REGISTER
		config			: in std_logic_vector(7 downto 0); -- config for arbitrator
		config_updated	: in std_logic; -- '1' if new config

		-- NVMe PACKAGE
		stream			: in std_logic_vector(7 downto 0); -- byte data size
		valid			: in std_logic; -- sending new data stream
		ready			: out std_logic; -- do not send new stream, module is busy

		-- STORAGE INTERFACE
		field			: out std_logic_vector_64(AC_CONFIGS-1 downto 0); -- captured data
		opcode 			: out std_logic_vector(7 downto 0);

		-- FRAMEWORK
		cmd_id			: out std_logic_vector(15 downto 0); -- command identifier
		evaluate		: out std_logic; -- evaluate result

		-- GENERAL PURPOSE
		rst				: in std_logic;
		error			: out std_logic;
		clk				: in std_logic

	);
end rtl_arbitrator_fc;

architecture rtl of rtl_arbitrator_fc is

	signal stream_delayed	: std_logic_vector(7 downto 0);
	signal opcode_i			: std_logic_vector(7 downto 0);
	signal count			: std_logic_vector(5 downto 0);
	signal last_delayed		: std_logic;
	signal error_fc			: std_logic_vector(AC_CONFIGS-1 downto 0);
	signal error_fc_red		: std_logic;
	signal error_fw			: std_logic;

begin

	opcode <= opcode_i;
	error <= error_fc_red or error_fw;

	OR_ERROR : entity work.rtl_reduce_operations
	generic map
	(
		DATA_WIDTH => AC_CONFIGS,
		OPERATION => 0
	)
	port map
	(
		data => error_fc,
		result => error_fc_red
	);

	SUB_FIELD_CAPTURES : for i in 0 to AC_CONFIGS-1 generate

		SUB_FIELD_CAPTURE : entity work.rtl_sub_field_capture
		generic map
		(
			USE_CONFIG => AC_CONFIGS+2,
			EN_CLEAR => false,
			CLEAR_ON_BOTH => false,
			CLOCK_EDGE => CLOCK_EDGE
		)
		port map
		(
			config => config,
			config_updated => config_updated,
			stream_delayed => stream_delayed,
			count => count,
			opcode => opcode_i,
			last_delayed => last_delayed,
			opcode_match => open,
			new_data => open,
			clear => '0',
			field => field(i),
			rst => rst,
			error => error_fc(i),
			clk => clk
		);

	end generate;

	FRAMEWORK : entity work.rtl_statistics_framework
	generic map
	(
		SUBMISSION_CMD => true,
		CLOCK_EDGE => CLOCK_EDGE
	)
	port map
	(
		stream => stream,
		valid => valid,
		ready => ready,
		stream_delayed => stream_delayed,
		opcode => opcode_i,
		count => count,
		cmd_id => cmd_id,
		last => open,
		last_delayed => last_delayed,
		evaluate => evaluate,
		error => error_fw,
		rst => rst,
		clk => clk
	);

end rtl;


