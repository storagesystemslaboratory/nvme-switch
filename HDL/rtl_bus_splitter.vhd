--------------------------------------------------------------------------------
--
-- Module Name: rtl_bus_splitter
-- Create Date: 09/18/2019 (MM/DD/YYYY)
-- Revision: 0
--
-- Company: ittChip - Unisinos
-- Engineer: Vinícius Gabriel LINDEN
-- 
-- Description: Real-time logic for splitting the NVMe stream BUS.
--              This module will block all other ramifications,
--              if one of the ramifications is not ready to process.
-- 
--------------------------------------------------------------------------------
-- TODO:

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
--use IEEE.NUMERIC_STD.ALL;

library UNISIM;
use UNISIM.VComponents.all;

--library work;
--use work.common_pkg.all;

entity rtl_bus_splitter is
	generic (
				RAMIFICATIONS       : integer := 2 -- Number of ramifications
			);
	port (

		-- SOURCE
			 valid_source        : in std_logic;
			 ready_source        : out std_logic;

		-- DESTINATION
			 valid_destination   : out std_logic_vector(RAMIFICATIONS-1 downto 0);
			 ready_destination   : in std_logic_vector(RAMIFICATIONS-1 downto 0);

		-- CONTROL
			 halt                : in std_logic -- blocks the transmission

		 );
end rtl_bus_splitter;

architecture rtl of rtl_bus_splitter is

	signal ready_dest_and       : std_logic;
	signal valid_dest_single    : std_logic;

begin

	ready_source <= ready_dest_and and (not halt);

	AND_RED : entity work.rtl_reduce_operations
	generic map (
					DATA_WIDTH => RAMIFICATIONS,
					OPERATION => 1
				)
	port map (
				 data => ready_destination,
				 result => ready_dest_and
			 );

	valid_dest_single <= valid_source when (ready_dest_and = '1' and halt = '0') else '0';
	valid_destination <= (others => valid_dest_single);
--valid_destination <= (others => (valid_source and ready_dest_and and not halt));

end rtl;


