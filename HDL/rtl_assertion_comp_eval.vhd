--------------------------------------------------------------------------------
--
-- Module Name: rtl_assertion_comp_eval
-- Create Date: 10/31/2019 (MM/DD/YYYY)
-- Revision: 0
--
-- Company: ittChip - Unisinos
-- Engineer: Vinícius Gabriel LINDEN
-- 
-- Description: Comparison's output evaluation for ruling-out whichever command that
--              does not have the required field or is a fixed value.
-- 
--------------------------------------------------------------------------------
-- TODO:
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
--use IEEE.NUMERIC_STD.ALL;

library UNISIM;
use UNISIM.VComponents.all;

library work;
use work.NVMe_pkg.all;

entity rtl_assertion_comp_eval is
	generic (
				DEFAULT_DIRECTION   : std_logic -- which direction to point, when there is no field to match
			);
	port (

		-- CONFIGURATION
			 config_ac           : in std_logic_vector(7 downto 0); -- input configuration

		-- NVMe
			 opcode              : in std_logic_vector(7 downto 0); -- NVMe opcode

		-- DIRECTION
			 direction           : in std_logic;
			 eval_direction      : out std_logic -- evaluated direction

		 );
end rtl_assertion_comp_eval;

architecture rtl of rtl_assertion_comp_eval is

	signal fixed_output     : std_logic; -- comparison outputs a fixed direction
	signal has_field        : std_logic; -- opcode has the necessary field

	-- LIST OF EVERY FIELD
	signal has_slba         : std_logic; -- opcode has slba field
										 -- END OF LIST OF EVERY FIELD

begin

	with config_ac select fixed_output <=
		'1' when AC_IDLE,
		'1' when AC_SEND_GENERIC,
		'1' when AC_SEND_SSD,
		'0' when others;

	with config_ac select has_field <=
		has_slba when AC_TEST,
		'0' when others; -- mostly necessary for VHDL purposes

	eval_direction <= direction when fixed_output = '1' else
					  direction when has_field = '1' else
					  DEFAULT_DIRECTION;

	-- INCREASE AS NECESSARY BELOW THIS LINE

	with opcode select has_slba <=
		'1' when NVME_READ,
		'0' when others;

end rtl;


