--------------------------------------------------------------------------------
--
-- Module Name: common_pkg.vhd
-- Create Date: XX/XX/XXXX (MM/DD/YYYY)
--
-- Company: ittChip - Unisinos
-- Engineer: Rodrigo Marques de Figueiredo & Vinícius Gabriel Linden
--
-- Description: Types and functions frequently used in projects
--
--------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

package common_pkg is

	-- arrays
	type std_logic_vector_64 is array (integer range <>) of std_logic_vector(63 downto 0);
	type std_logic_vector_32 is array (integer range <>) of std_logic_vector(31 downto 0);
	type std_logic_vector_16 is array (integer range <>) of std_logic_vector(15 downto 0);
	type std_logic_vector_12 is array (integer range <>) of std_logic_vector(11 downto 0);
	type std_logic_vector_10 is array (integer range <>) of std_logic_vector(9 downto 0);
	type std_logic_vector_8 is array (integer range <>) of std_logic_vector(7 downto 0);
	type std_logic_vector_3 is array (integer range <>) of std_logic_vector(2 downto 0);
	type std_logic_vector_2 is array (integer range <>) of std_logic_vector(1 downto 0);
	type std_logic_vector_1 is array (integer range <>) of std_logic_vector(0 downto 0);
	type integer_vector is array (integer range <>) of integer;
	type natural_vector is array (natural range <>) of natural;
	type boolean_vector is array (integer range <>) of boolean;
	type real_vector is array (integer range <>) of real;
	type time_vector is array (integer range <>) of time;

	-- FUNCTION: swap_bits
	-- Bits swap
	function swap_bits (x: std_logic_vector) return std_logic_vector;

	-- FUNCTION: std2bit
	-- Converts std_logic to bit type
	function std2bit(x : std_logic) return bit;

	-- FUNCTION: bool2str
	-- Boolean to string conversion
	function bool2str (x: boolean) return string;

	-- FUNCTION: bool2std_logic
	-- Boolean to std_logic conversion
	function bool2std_logic (x: boolean) return std_logic;

	-- FUNCTION: bool2int
	-- Boolean to integer conversion
	function bool2int (x: boolean) return integer;

	-- FUNCTION: slv_to_vector2
	-- std_logic_vector to std_logic_vector_2 conversion
	function slv_to_vector2 (x: std_logic_vector) return std_logic_vector_2;

	-- FUNCTION: vector2_to_slv
	-- std_logic_vector_2 to std_logic_vector conversion
	function vector2_to_slv (x: std_logic_vector_2) return std_logic_vector;

	-- FUNCTION: slv_to_ddr
	-- std_logic_vector to DDR(std_logic_vector_2) conversion
	function slv_to_ddr (x: std_logic_vector) return std_logic_vector_2;

	-- FUNCTION: ddr_to_slv
	-- DDR(std_logic_vector_2) to std_logic_vector conversion
	function ddr_to_slv (x: std_logic_vector_2) return std_logic_vector;

	-- FUNCTION: vsize
	-- Returns vector size of input argument
	function vsize (x: integer) return integer;

	-- FUNCTION: greater
	-- Returns greater argument
	function greater (x: integer_vector) return integer;

	-- FUNCTION: less
	-- Returns smallest argument
	function less (x: integer_vector) return integer;

	-- FUNCTION: maj_ones
	-- Returns true if majority is of one bits otherwise retuns false
	function maj_ones (x: std_logic_vector) return boolean;

	-- FUNCTION: number_of_ones
	-- Returns the number of one bits
	function number_of_ones (x: std_logic_vector) return integer;

	-- FUNCTION: left_pad
	-- Returns the input std_logic_vector padded with zeros
	function left_pad (x: std_logic_vector; width: integer) return std_logic_vector;

	-- FUNCTION: zero_pad (std_logic_vector)
	-- Returns the input std_logic_vector padded with zeros
	function zero_pad (x: std_logic_vector; width: integer) return std_logic_vector;

	-- FUNCTION: zero_pad (std_logic)
	-- Returns the input std_logic padded with zeros
	function zero_pad (x: std_logic; width: integer) return std_logic_vector;

	-- FUNCTION: bool_and
	-- And logical operation as function
	function bool_and (x: boolean_vector) return boolean;

	-- FUNCTION: bool_or
	-- Or logical operation as function
	function bool_or (x: boolean_vector) return boolean;

	-- The inverse tap function allows for parametrized amount of a fixed data width
	-- FUNCTION: std_logic_vector_1_inverse_tap
	-- Same idea as std_logic_vector_2_inverse_tap
	function std_logic_vector_1_inverse_tap(x : std_logic_vector_1) return std_logic_vector;

	-- FUNCTION: std_logic_vector_2_inverse_tap
	-- Taps the std_logic_vector_2 input invesely
	-- Example: x = (3 => "11", 2 => "10", 1 => "01", 0 => "00")
	-- when n = 0 -> y = "1010"
	-- when n = 1 -> y = "1100"
	function std_logic_vector_2_inverse_tap(x : std_logic_vector_2; n : natural) return std_logic_vector;

	-- FUNCTION: std_logic_vector_8_inverse_tap
	-- Same idea as std_logic_vector_2_inverse_tap
	function std_logic_vector_8_inverse_tap(x : std_logic_vector_8; n : natural) return std_logic_vector;

	-- FUNCTION: std_logic_vector_16_inverse_tap
	-- Same idea as std_logic_vector_2_inverse_tap
	function std_logic_vector_16_inverse_tap(x : std_logic_vector_16; n : natural) return std_logic_vector;

	-- FUNCTION: std_logic_vector_10_inverse_tap
	-- Same idea as std_logic_vector_2_inverse_tap
	function std_logic_vector_10_inverse_tap(x : std_logic_vector_10; n : natural) return std_logic_vector;

	-- FUNCTION: std_logic_vector_12_inverse_tap
	-- Same idea as std_logic_vector_2_inverse_tap
	function std_logic_vector_12_inverse_tap(x : std_logic_vector_12; n : natural) return std_logic_vector;

	-- FUNCTION: std_logic_vector_32_inverse_tap
	-- Same idea as std_logic_vector_2_inverse_tap
	function std_logic_vector_32_inverse_tap(x : std_logic_vector_32; n : natural) return std_logic_vector;

	-- FUNCTION: std_logic_vector_64_inverse_tap
	-- Same idea as std_logic_vector_2_inverse_tap
	function std_logic_vector_64_inverse_tap(x : std_logic_vector_64; n : natural) return std_logic_vector;

end common_pkg;

package body common_pkg is

	-- FUNCTION: swap_bits
	function swap_bits (x: std_logic_vector) return std_logic_vector is
		variable result : std_logic_vector(x'range);
	begin
		if x'high - x'low = 0 then
			result := x;
		else
			for i in 0 to (x'high - x'low) loop
				result(x'high - i) := x(x'low + i);
			end loop;
		end if;
		return result;
	end swap_bits;

	-- FUNCTION: std2bit
	function std2bit(x : std_logic) return bit is
		variable y : bit;
	begin
		if x = '1' then
			y := '1';
		else -- all other values (U, X, H, L, ...)
			y := '0';
		end if;
		return y;
	end std2bit;

	-- FUNCTION: bool2str
	function bool2str (x: boolean) return string is
	begin
		if x then
			return "TRUE";
		else
			return "FALSE";
		end if;
	end bool2str;

	-- FUNCTION: bool2std_logic
	function bool2std_logic (x: boolean) return std_logic is
		variable result : std_logic;
	begin
		if x = true then
			result := '1';
		else
			result := '0';
		end if;
		return result;
	end bool2std_logic;

	-- FUNCTION: bool2int
	function bool2int (x: boolean) return integer is
		variable result : integer;
	begin
		if x = true then
			result := 1;
		else
			result := 0;
		end if;
		return result;
	end bool2int;

	-- FUNCTION: slv_to_vector2
	function slv_to_vector2 (x: std_logic_vector) return std_logic_vector_2 is
		variable result : std_logic_vector_2((x'high - x'low + 1)/2 - 1 downto 0);
	begin
		for i in 0 to (x'high - x'low + 1)/2 - 1 loop
			result(i)(0) := x(x'low + 2*i);
			result(i)(1) := x(x'low + 2*i + 1);
		end loop;
		return result;
	end slv_to_vector2;

	-- FUNCTION: vector2_to_slv
	function vector2_to_slv (x: std_logic_vector_2) return std_logic_vector is
		variable result :std_logic_vector((x'high - x'low + 1)*2 - 1 downto 0);
	begin
		for i in 0 to x'high - x'low loop
			result(2*i)	:= x(x'low + i)(0);
			result(2*i + 1) := x(x'low + i)(1);
		end loop;
		return result;
	end vector2_to_slv;

	-- FUNCTION: slv_to_ddr
	function slv_to_ddr (x: std_logic_vector) return std_logic_vector_2 is
		variable result : std_logic_vector_2((x'high - x'low + 1)/2 - 1 downto 0);
	begin
		for i in 0 to (x'high - x'low + 1)/2 - 1 loop
			result(i)(0) := x(x'low + i);
			result(i)(1) := x((x'high - x'low + 1)/2 + i);
		end loop;
		return result;
	end slv_to_ddr;

	-- FUNCTION: ddr_to_slv
	function ddr_to_slv (x: std_logic_vector_2) return std_logic_vector is
		variable result :std_logic_vector((x'high - x'low + 1)*2 - 1 downto 0);
	begin
		for i in 0 to x'high - x'low loop
			result(i)	:= x(x'low + i)(0);
			result(i + x'high - x'low + 1) := x(x'low + i)(1);
		end loop;
		return result;
	end ddr_to_slv;

	-- FUNCTION: vector_size
	function vsize (x: integer) return integer is
	begin
		if x = 0 then
			return 1;
		else
			return x;
		end if;
	end vsize;

	-- FUNCTION: greater
	function greater (x: integer_vector) return integer is
		variable result	: integer := 0;
	begin
		for i in x'low to x'high loop
			if result < x(i) then
				result := x(i);
			end if;
		end loop;
		return result;
	end greater;

	-- FUNCTION: less
	function less (x: integer_vector) return integer is
		variable result	: integer := 2147483647;
	begin
		for i in x'low to x'high loop
			if result > x(i) then
				result := x(i);
			end if;
		end loop;
		return result;
	end less;

	-- FUNCTION: maj_ones
	function maj_ones(x: std_logic_vector) return boolean is
		variable ones	: integer := 0;
		variable zeros	: integer := 0;
	begin
		-- Counts ones and zeros
		for i in x'low to x'high loop
			if x(i) = '1' then
				ones := ones + 1;
			else
				zeros := zeros + 1;
			end if;
		end loop;

		-- Compares ones and zeros
		if ones > zeros then
			return true;
		else
			return false;
		end if;
	end maj_ones;

	-- FUNCTION: number_of_ones
	function number_of_ones(x: std_logic_vector) return integer is
		variable ones	: integer := 0;
	begin
		-- Counts number of ones
		for i in x'low to x'high loop
			if x(i) = '1' then
				ones := ones + 1;
			end if;
		end loop;
		return ones;
	end number_of_ones;

	-- FUNCTION: left_pad
	function left_pad (x: std_logic_vector; width: integer) return std_logic_vector is
		variable pad : std_logic_vector(width-1 downto 0);
	begin
		if x'length < width then
			pad(width-1 downto x'length) := (others => '0');
			pad(x'length-1 downto 0) := x;
		else
			pad := x(width-1 downto 0);
		end if;
		return pad;
	end left_pad;

	-- FUNCTION: zero_pad (std_logic_vector)
	function zero_pad (x: std_logic_vector; width: integer) return std_logic_vector is
		variable pad : std_logic_vector(width-1 downto 0) := (others => '0');
	begin
		if x'ascending then
			pad(x'RIGHT downto x'LEFT) := x;
		else
			pad(x'range) := x;
		end if;
		return pad;
	end zero_pad;

	-- FUNCTION: zero_pad (std_logic)
	function zero_pad (x: std_logic; width: integer) return std_logic_vector is
		variable pad : std_logic_vector(width-1 downto 0);
	begin
		pad(width-1 downto 1) := (others => '0');
		pad(0) := x;
		return pad;
	end zero_pad;

	-- FUNCTION: bool_and
	function bool_and (x: boolean_vector) return boolean is
	begin
		for i in x'low to x'high loop
			if x(i) = false then
				return false;
			end if;
		end loop;
		return true;
	end bool_and;

	-- FUNCTION: bool_or
	function bool_or (x: boolean_vector) return boolean is
	begin
		for i in x'low to x'high loop
			if x(i) = true then
				return true;
			end if;
		end loop;
		return false;
	end bool_or;

	-- FUNCTION: std_logic_vector_1_inverse_tap
	function std_logic_vector_1_inverse_tap(x : std_logic_vector_1) return std_logic_vector is
		variable y : std_logic_vector(x'range);
	begin
		for i in x'range loop
			y(i) := x(i)(0);
		end loop;
		return y;
	end std_logic_vector_1_inverse_tap;

	-- FUNCTION: std_logic_vector_2_inverse_tap
	function std_logic_vector_2_inverse_tap(x : std_logic_vector_2; n : natural) return std_logic_vector is
		variable y : std_logic_vector(x'range);
	begin
		for i in x'range loop
			y(i) := x(i)(n);
		end loop;
		return y;
	end std_logic_vector_2_inverse_tap;

	-- FUNCTION: std_logic_vector_8_inverse_tap
	function std_logic_vector_8_inverse_tap(x : std_logic_vector_8; n : natural) return std_logic_vector is
		variable y : std_logic_vector(x'range);
	begin
		for i in x'range loop
			y(i) := x(i)(n);
		end loop;
		return y;
	end std_logic_vector_8_inverse_tap;

	-- FUNCTION: std_logic_vector_10_inverse_tap
	function std_logic_vector_10_inverse_tap(x : std_logic_vector_10; n : natural) return std_logic_vector is
		variable y : std_logic_vector(x'range);
	begin
		for i in x'range loop
			y(i) := x(i)(n);
		end loop;
		return y;
	end std_logic_vector_10_inverse_tap;

	-- FUNCTION: std_logic_vector_12_inverse_tap
	function std_logic_vector_12_inverse_tap(x : std_logic_vector_12; n : natural) return std_logic_vector is
		variable y : std_logic_vector(x'range);
	begin
		for i in x'range loop
			y(i) := x(i)(n);
		end loop;
		return y;
	end std_logic_vector_12_inverse_tap;

	-- FUNCTION: std_logic_vector_16_inverse_tap
	function std_logic_vector_16_inverse_tap(x : std_logic_vector_16; n : natural) return std_logic_vector is
		variable y : std_logic_vector(x'range);
	begin
		for i in x'range loop
			y(i) := x(i)(n);
		end loop;
		return y;
	end std_logic_vector_16_inverse_tap;

	-- FUNCTION: std_logic_vector_32_inverse_tap
	function std_logic_vector_32_inverse_tap(x : std_logic_vector_32; n : natural) return std_logic_vector is
		variable y : std_logic_vector(x'range);
	begin
		for i in x'range loop
			y(i) := x(i)(n);
		end loop;
		return y;
	end std_logic_vector_32_inverse_tap;

	-- FUNCTION: std_logic_vector_64_inverse_tap
	function std_logic_vector_64_inverse_tap(x : std_logic_vector_64; n : natural) return std_logic_vector is
		variable y : std_logic_vector(x'range);
	begin
		for i in x'range loop
			y(i) := x(i)(n);
		end loop;
		return y;
	end std_logic_vector_64_inverse_tap;

end common_pkg;
