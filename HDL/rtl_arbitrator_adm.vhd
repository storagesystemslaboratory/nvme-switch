--------------------------------------------------------------------------------
--
-- Module Name: rtl_arbitrator_adm
-- Create Date: 09/06/2019 (MM/DD/YYYY)
-- Revision: 0
--
-- Company: ittChip - Unisinos
-- Engineer: Vinícius Gabriel LINDEN
-- 
-- Description: Real-time logic arbitrator administrator.
--              It takes the direction from the assertion module, asks for
--              priority over the package flow from host to SSD,
--              unloads the FIFO NVMe & submits CMD ID to the filter module.
-- 
--------------------------------------------------------------------------------
-- TODO:

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

library UNISIM;
use UNISIM.VComponents.all;

library work;
use work.NVMe_pkg.all;

entity rtl_arbitrator_adm is
	generic
	(
		CLOCK_EDGE 			: std_logic := '1' -- '1' for rising, '0' for falling
	);
	port
	(
		-- ASSERTION
		direction 			: in std_logic; -- direction of the field (to generic or to SSD)
		new_available 		: in std_logic; -- there is new field to fetch
		next_direction 		: out std_logic; -- next from inline

		-- MUX DEMUX
		adm_ask 			: out std_logic;
		adm_lock 			: in std_logic;
		adm_flow 			: out std_logic;

		-- FIFO NVMe
		full 				: in std_logic;
		empty 				: in std_logic;
		unload 				: out std_logic;
		unloaded 			: in std_logic;
		processing 			: in std_logic;
		number_pkgs 		: in std_logic_vector(7 downto 0);

		-- FILTER
		submit 				: out std_logic; -- submit to filter module
		response 			: in std_logic; -- acknowledgement from filter module

		-- OTHER
		error 				: out std_logic;
		rst 				: in std_logic;
		clk 				: in std_logic
	);
end rtl_arbitrator_adm;

architecture rtl of rtl_arbitrator_adm is

	type fsm_state is (IDLE,
					   FETCH,
					   WAIT_LOCK,
					   LATCH,
					   RELEASE_CMD
					   );
	signal state_fsm 		: fsm_state;
	signal adm_flow_i 		: std_logic;
	signal submit_raw 		: std_logic;
	signal response_raw 	: std_logic;
	signal unload_i 		: std_logic;

begin

	adm_flow <= adm_flow_i;
	response_raw <= response when adm_flow_i = TO_GENERIC else submit_raw; -- this is a work-around
	submit <= submit_raw when adm_flow_i = TO_GENERIC else '0'; -- this is a work-around
	unload <= unload_i;
	error <= '0';

	process(rst,clk)
	begin
		if rst = '1' then
			adm_flow_i <= TO_SSD;
			unload_i <= '0';
			next_direction <= '0';
			submit_raw <= '0';
			state_fsm <= IDLE;
		elsif clk'event and clk = CLOCK_EDGE then
			case state_fsm is
				when IDLE =>
					if new_available = '1' then
						next_direction <= '1';
						state_fsm <= FETCH;
					end if;
				when FETCH =>
					next_direction <= '0';
					if adm_lock = '1' then
						state_fsm <= LATCH;
					else
						state_fsm <= WAIT_LOCK;
					end if;
				when WAIT_LOCK =>
					if adm_lock = '1' then
						state_fsm <= LATCH;
					end if;
				when LATCH =>
					adm_flow_i <= direction;
					unload_i <= '1';
					submit_raw <= '1';
					state_fsm <= RELEASE_CMD;
				when RELEASE_CMD =>
					if response_raw = '1' then
						submit_raw <= '0';
					end if;
					if processing = '1' then
						unload_i <= '0';
					end if;
					if unloaded = '1' and submit_raw = '0' and unload_i = '0' then
						if new_available = '1' then
							next_direction <= '1';
							state_fsm <= FETCH;
						else
							state_fsm <= IDLE;
						end if;
					end if;
				when others =>
					adm_flow_i <= TO_SSD;
					unload_i <= '0';
					next_direction <= '0';
					submit_raw <= '0';
					state_fsm <= IDLE;
			end case;
		end if;
	end process;
	adm_ask <= '0' when state_fsm = IDLE else '1';

end rtl;


