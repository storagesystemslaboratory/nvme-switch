--------------------------------------------------------------------------------
--
-- Module Name: sim_compare_operation
-- Create Date: 01/22/2019 (MM/DD/YYYY)
--
-- Company: ittChip - Unisinos
-- Engineer: Vinícius Gabriel LINDEN
-- 
-- Description: Stimulae compare_operation
-- 
--------------------------------------------------------------------------------
-- Recommended span of 300 ns for period of 10 ns
-- TODO:


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library UNISIM;
use UNISIM.VComponents.all;

--library STD;
--use STD.TEXTIO.ALL;

library work;
--use work.common_pkg.all;
--use work.kratus_pkg.all;

entity sim_compare_operation is
	end sim_compare_operation;


architecture Behavioral of sim_compare_operation is

	-- DO NOT DELETE
	constant period                 : time := 5 ns;
	constant hp                     : time := period/2; -- half period

	signal index                    : natural := 1; -- index for redability
	signal clk                      : std_logic := '1'; -- system base clock

	procedure wait_clk(constant T : in natural) is
	begin
		wait for t*period;
	end procedure;
	-- DELETION RESTRICTION ENDED

	-- UUT
	constant HAND_MADE      : boolean := false;
	constant DATA_WIDTH     : integer := 8;
	signal value1           : std_logic_vector(DATA_WIDTH-1 downto 0);
	signal value2           : std_logic_vector(DATA_WIDTH-1 downto 0) := x"ab";
	signal greater          : std_logic;
	signal greater_equal    : std_logic;
	signal less             : std_logic;
	signal less_equal       : std_logic;
	signal equal            : std_logic;
	signal different        : std_logic;
-- END OF UUT;

begin

	UUT : entity work.rtl_compare_operation
	generic map (
					HAND_MADE => HAND_MADE,
					DATA_WIDTH => DATA_WIDTH
				)
	port map (
				 value1 => value1,
				 value2 => value2,
				 greater => greater,
				 greater_equal => greater_equal,
				 less => less,
				 less_equal => less_equal,
				 equal => equal,
				 different => different
			 );

	-- DO NOT DELETE
	clk <= not clk after hp;
	index <= index + 1 after period;
	-- DELETION RESTRICTION ENDED

	process
	begin
		for i in 0 to 2**DATA_WIDTH-1 loop
			value1 <= std_logic_vector(to_unsigned(i,DATA_WIDTH));
			wait_clk(1);
		end loop;
		wait;
	end process;

end Behavioral;


