--------------------------------------------------------------------------------
--
-- Module Name: sim_full_comparison_operation
-- Create Date: 09/05/2019 (MM/DD/YYYY)
--
-- Company: ittChip - Unisinos
-- Engineer: Vinícius Gabriel LINDEN
-- 
-- Description: Stimulae for the full comparison operation
-- 
--------------------------------------------------------------------------------
-- Recommended span of 300 ns for period of 10 ns
-- TODO:


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library UNISIM;
use UNISIM.VComponents.all;

--library STD;
--use STD.TEXTIO.ALL;

library work;
--use work.common_pkg.all;
use work.kratus_pkg.all;

entity sim_full_comparison_operation is
	end sim_full_comparison_operation;


architecture Behavioral of sim_full_comparison_operation is

	-- DO NOT DELETE
	constant period                 : time := 5 ns;
	constant hp                     : time := period/2; -- half period

	signal index                    : natural := 1; -- index for redability
	signal clk                      : std_logic := '1'; -- system base clock

	procedure wait_clk(constant T : in natural) is
	begin
		wait for t*period;
	end procedure;
	-- DELETION RESTRICTION ENDED

	-- UUT
	constant DATA_WIDTH     : integer := 64;
	constant VALUE_HIGH     : std_logic := '1';
	constant INVERT_WHEN_RANGE : boolean := false;
	signal config           : std_logic_vector(7 downto 0) := (others => '0');
	signal value1           : std_logic_vector(DATA_WIDTH-1 downto 0) := (others => '0');
	signal value2           : std_logic_vector(DATA_WIDTH-1 downto 0) := (others => '0');
	signal data             : std_logic_vector(DATA_WIDTH-1 downto 0) := (others => '0');
	signal oput             : std_logic;
	-- END OF UUT

	-- SIMULATION
	constant NUMBER_OF_CONFIG : integer := 10; -- maximum value of "config"
	signal test_case        : string(1 to 20) := "waiting             ";
	signal proof            : std_logic;
	-- END OF SIMULATION

	-- FUNCTION FOR COMPARING THE RESULTS
	function assertion(config    : std_logic_vector;
	data      : std_logic_vector;
	value1    : std_logic_vector;
	value2    : std_logic_vector) return std_logic is
	variable res : std_logic := '1'; -- 1: pass-through, 0: generic SSD
	variable data_un : unsigned(data'range);
	variable value1_un : unsigned(value1'range);
	variable value2_un : unsigned(value2'range);
	begin
		data_un := unsigned(data);
		value1_un := unsigned(value1);
		value2_un := unsigned(value2);
		case config(7 downto 0) is
			when "00000000" => -- (0) make low
				res := '0';
			when "00000001" => -- (1) make high
				res := '1';
			when "00000010" => -- (2) equal to "value1"
				if data = value1 then
					res := '1';
				else
					res := '0';
				end if;
			when "00000011" => -- (3) greater than "value1"
				if data > value1 then
					res := '1';
				else
					res := '0';
				end if;
			when "00000100" => -- (4) greater than or equal to "value1"
				if data_un >= value1_un then
					res := '1';
				else
					res := '0';
				end if;
			when "00000101" => -- (5) less than "value1"
				if data_un < value1_un then
					res := '1';
				else
					res := '0';
				end if;
			when "00000110" => -- (6) less than or equal to "value1"
				if data_un <= value1_un then
					res := '1';
				else
					res := '0';
				end if;
			when "00000111" => -- (7) greater than "value1" and less than "value2"
				if data_un > value1_un and data_un < value2_un then
					res := '1';
				else
					res := '0';
				end if;
			when "00001000" => -- (8) greater than or equal to "value1" and less than "value2"
				if data_un >= value1_un and data_un < value2_un then
					res := '1';
				else
					res := '0';
				end if;
			when "00001001" => -- (9) greater than "value1" and less than or equal to "value2"
				if data_un > value1_un and data_un <= value2_un then
					res := '1';
				else
					res := '0';
				end if;
			when "00001010" => -- (10) greater than or equal to "value1" and less than or equal to "value2"
				if data_un >= value1_un and data_un <= value2_un then
					res := '1';
				else
					res := '0';
				end if;
			when others =>
				res := '0';
		end case;
		return res;
	end function assertion;

begin

	UUT : entity work.rtl_full_comparison_operation
	generic map (
					DATA_WIDTH => DATA_WIDTH,
					VALUE_HIGH => VALUE_HIGH,
					INVERT_WHEN_RANGE => INVERT_WHEN_RANGE
				)
	port map (
				 config => config,
				 value1 => value1,
				 value2 => value2,
				 data => data,
				 oput => oput
			 );

	-- DO NOT DELETE
	clk <= not clk after hp;
	index <= index + 1 after period;
	-- DELETION RESTRICTION ENDED

	proof <= assertion(config,data,value1,value2);

	process
	begin
		wait_clk(3);

		test_case <= "clear               ";
		wait_clk(10);

		test_case <= "normal std test     ";
		value1(3 downto 0) <= "0001";
		value2(3 downto 0) <= "0011";
		wait_clk(2);
		for k in 0 to 4 loop
			data <= std_logic_vector(to_unsigned(k,64));
			for i in 0 to NUMBER_OF_CONFIG+1 loop
				config <= std_logic_vector(to_unsigned(i,8));
				wait_clk(1);
			end loop;
		end loop;

		test_case <= "clear               ";
		wait_clk(10);

		test_case <= "inverted std test   ";
		value1(3 downto 0) <= "0011";
		value2(3 downto 0) <= "0001";
		wait_clk(2);
		for k in 0 to 4 loop
			data <= std_logic_vector(to_unsigned(k,64));
			for i in 0 to NUMBER_OF_CONFIG+1 loop
				config <= std_logic_vector(to_unsigned(i,8));
				wait_clk(1);
			end loop;
		end loop;

		test_case <= "clear               ";
		wait_clk(10);

		test_case <= "simulation ended    ";
		wait;
	end process;

end Behavioral;


