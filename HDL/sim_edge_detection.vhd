--------------------------------------------------------------------------------
--
-- Module Name: sim_edge_detection
-- Create Date: 09/08/2019 (MM/DD/YYYY)
--
-- Company: ittChip - Unisinos
-- Engineer: Vinícius Gabriel LINDEN
-- 
-- Description: Stimulae for the edge detection module
-- 
--------------------------------------------------------------------------------
-- Recommended span of 300 ns for period of 10 ns
-- TODO:


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
--use IEEE.NUMERIC_STD.ALL;

library UNISIM;
use UNISIM.VComponents.all;

--library STD;
--use STD.TEXTIO.ALL;

--library work;

entity sim_edge_detection is
end sim_edge_detection;


architecture Behavioral of sim_edge_detection is

	-- DO NOT DELETE
	constant period				: time := 5 ns;
	constant hp					: time := period/2; -- half period

	signal index				: natural := 1; -- index for redability
	signal clk					: std_logic := '1'; -- system base clock

	procedure wait_clk(constant T : in natural) is
	begin
		wait for t*period;
	end procedure;
	-- DELETION RESTRICTION ENDED

	-- SIMULATION
	signal test_case 			: string(1 to 20) := "waiting             ";
	-- END OF SIMULATION

	-- UUT
	constant SIGNALS 			: natural := 4;
	constant BOTH_EDGES 		: boolean := true;
	constant DETECTION_EDGE 	: std_logic := '1';
	constant CLOCK_EDGE 		: std_logic := '1';
	signal sense 				: std_logic_vector(SIGNALS-1 downto 0);
	signal updated_one_edge 	: std_logic_vector(SIGNALS-1 downto 0);
	signal updated_both_edges 	: std_logic_vector(SIGNALS-1 downto 0);
	signal updated_active_low 	: std_logic_vector(SIGNALS-1 downto 0);
-- END OF UUT

begin

	UUT_ONE_EDGE : entity work.rtl_edge_detection
	generic map
	(
		SIGNALS => SIGNALS,
		ACTIVE_LOW => false,
		BOTH_EDGES => false,
		DETECTION_EDGE => DETECTION_EDGE,
		CLOCK_EDGE => CLOCK_EDGE
	)
	port map
	(
		sense => sense,
		updated => updated_one_edge,
		clk => clk
	);

	UUT_BOTH_EDGES : entity work.rtl_edge_detection
	generic map
	(
		SIGNALS => SIGNALS,
		ACTIVE_LOW => false,
		BOTH_EDGES => true,
		DETECTION_EDGE => DETECTION_EDGE,
		CLOCK_EDGE => CLOCK_EDGE
	)
	port map
	(
		sense => sense,
		updated => updated_both_edges,
		clk => clk
	);

	UUT_ACTIVE_LOW : entity work.rtl_edge_detection
	generic map (
		SIGNALS => SIGNALS,
		ACTIVE_LOW => true,
		BOTH_EDGES => false,
		DETECTION_EDGE => '0',
		CLOCK_EDGE => CLOCK_EDGE
	)
	port map
	(
		sense => sense,
		updated => updated_active_low,
		clk => clk
	);

	-- DO NOT DELETE
	process
	begin
		wait for hp;
		clk <= not clk;
	end process;

	process
	begin
		wait until clk'event and clk = '1';
		index <= index + 1;
	end process;
	-- DELETION RESTRICTION ENDED

	process
	begin
		sense <= (others => '0');
		wait_clk(2);
		test_case <= "commit rising edge  ";
		sense(2 downto 0) <= "111";
		wait_clk(3);
		sense(3) <= '1';
		wait_clk(10);
		test_case <= "standard reset      ";
		wait_clk(10);
		test_case <= "commit falling edge ";
		sense(3) <= '0';
		wait_clk(3);
		sense(2 downto 0) <= "000";
		wait_clk(10);
		test_case <= "reset during activat";
		wait_clk(1);
		sense(2 downto 0) <= "010";
		wait_clk(2);
		sense(2 downto 0) <= not sense(2 downto 0);
		wait_clk(64);
		test_case <= "simulation ended    ";
		wait;
	end process;

end Behavioral;


