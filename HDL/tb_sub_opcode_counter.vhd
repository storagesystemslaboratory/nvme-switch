--------------------------------------------------------------------------------
--
-- Module Name: tb_sub_opcode_counter
-- Create Date: 21/01/2020 (MM/DD/YYYY)
--
-- Company: ittChip - Unisinos
-- Engineer: Vinícius Gabriel LINDEN
-- 
-- Description: Testbench sub_opcode_counter
-- 
--------------------------------------------------------------------------------
-- Recommended span of 300 ns for period of 10 ns
-- TODO:


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library UNISIM;
use UNISIM.VComponents.all;

library STD;
use STD.TEXTIO.ALL;

library work;
use work.common_pkg.all;
use work.NVMe_pkg.all;
use work.kratus_pkg.all;

entity tb_sub_opcode_counter is
end tb_sub_opcode_counter;


architecture Behavioral of tb_sub_opcode_counter is

	-- DO NOT DELETE
	constant period				: time := 5 ns;
	constant hp					: time := period/2; -- half period

	signal index				: natural := 1; -- index for redability
	signal clk					: std_logic := '1'; -- system base clock

	procedure wait_clk(constant T : in natural) is
	begin
		wait for t*period;
	end procedure;
	-- DELETION RESTRICTION ENDED

	-- TESTBENCH FILE
	constant WIDTH_INPUT		: integer := 8; -- number of input characters in a line
	file file_input				: text;
	signal read_input			: std_logic_vector(WIDTH_INPUT-1 downto 0);
	signal enable_input			: std_logic := '0'; -- control signal to enable input read
	-- END OF TESTBENCH FILE

	-- INPUT FILE RELATED
	constant F_READ1			: string(1 to 16) := "read1_______.txt";
	constant F_READ2			: string(1 to 16) := "read2_______.txt";
	constant F_FLUSH			: string(1 to 16) := "flush_______.txt";
	constant F_WRITE			: string(1 to 16) := "write_______.txt";
	constant F_PROP1			: string(1 to 16) := "NVMe_SQ_B_01.txt";
	constant F_PROP2			: string(1 to 16) := "NVMe_SQ_B_02.txt";
	signal file_title			: string(1 to 16) := F_READ1; -- file currently being process
	-- END OF INPUT FILE RELATED

	-- UUT
	constant SUBMISSION_CMD		: boolean := true;
	constant CLOCK_EDGE			: std_logic := '1';
	signal config				: std_logic_vector(7 downto 0) := (others => '0');
	signal config_updated		: std_logic := '0';
	signal stream_delayed		: std_logic_vector(7 downto 0);
	signal count				: std_logic_vector(5 downto 0);
	signal opcode				: std_logic_vector(7 downto 0);
	signal last_delayed			: std_logic;
	signal evaluate				: std_logic;
	signal result				: std_logic_vector(7 downto 0);
	signal error				: std_logic;
	signal rst					: std_logic := '1';
	-- END OF UUT

	-- FRAMEWORK
	signal stream				: std_logic_vector(7 downto 0) := (others => '0');
	signal valid				: std_logic := '0';
	signal ready				: std_logic;
	signal error_fw				: std_logic;
	-- END OF FRAMEWORK

	-- SIMULATION
	signal test_case			: string(1 to 20) := "waiting             ";
	signal read_file			: std_logic := '0'; -- on/off control for input file and handshake control
	signal config_edge			: std_logic_vector(7 downto 0) := (others => '0');
	-- END OF SIMULATION

begin

	UUT : entity work.rtl_sub_opcode_counter
	generic map
	(
		CLOCK_EDGE => CLOCK_EDGE
	)
	port map
	(
		config=> config,
		config_updated => config_updated,
		stream_delayed => stream_delayed,
		count => count,
		opcode => opcode,
		last_delayed => last_delayed,
		evaluate => evaluate,
		result => result,
		error => error,
		rst => rst,
		clk => clk
	);

	FRAMEWORK : entity work.rtl_statistics_framework 
	generic map
	(
		SUBMISSION_CMD => true,
		CLOCK_EDGE => CLOCK_EDGE
	)
	port map
	(
		stream => stream,
		valid => valid,
		ready => ready,
		stream_delayed => stream_delayed,
		opcode => opcode,
		count => count,
		last_delayed => last_delayed,
		evaluate => evaluate,
		cmd_id => open,
		last => open,
		error => error_fw,
		rst => rst,
		clk => clk
	);

	-- DO NOT DELETE
	clk <= not clk after hp;
	index <= index + 1 after period;
	-- DELETION RESTRICTION ENDED

	-- TESTBENCH FILE
	READ_PROCESS : process -- read
		variable line_input               : line;
		variable bit_input                : bit_vector(WIDTH_INPUT-1 downto 0);
	begin
		wait until enable_input'event and enable_input = '1';
		file_open(file_input,file_title,read_mode);
		while not endfile(file_input) loop
			if enable_input = '1' then
				readline(file_input,line_input);
				read(line_input,bit_input);
				read_input <= to_stdlogicvector(bit_input);
			end if;
			wait for period;
		end loop;
		file_close(file_input);
	end process READ_PROCESS;
	-- END OF TESTBENCH FILE

	-- INPUT DATA CONTROL
	stream <= read_input;
	-- END OF INPUT DATA CONTROL

	-- READ FILE AUTOMATIC CONTROL
	process
	begin
		enable_input <= '0';
		wait until read_file'event;
		enable_input <= '1';
		wait_clk(64);
	end process;
	-- END OF READ FILE AUTOMATIC CONTROL

	-- RECEIVING CYCLE CONTROL
	process
	begin
		valid <= '0';
		wait until read_file'event;
		valid <= '1';
		wait_clk(1);
	end process;
	-- END OF RECEIVING CYCLE CONTROL

	-- CONFIG_UPDATED AUTOMATIC CONTROL
	config_edge <= config after period;
	config_updated <= '1' when config_edge /= config else '0';
	-- END OF CONFIG_UPDATED AUTOMATIC CONTROL

	process
	begin
		wait_clk(2);
		rst <= '0';
		wait_clk(2);
		report "test bench started";

		test_case <= "clear               ";
		wait_clk(10);

		test_case <= "f_rd oc_rd          ";
		config <= OC_READ;
		file_title <= F_READ1;
		wait_clk(5);
		toggle(read_file);
		wait_clk(100);
		assert result = x"01" and error = '0' report "failed at " & test_case;
		file_title <= F_READ2;
		wait_clk(5);
		toggle(read_file);
		wait_clk(100);
		assert result = x"02" and error = '0' report "failed at " & test_case;

		test_case <= "clear               ";
		wait_clk(10);

		test_case <= "f_wr oc_rd          ";
		file_title <= F_WRITE;
		wait_clk(5);
		toggle(read_file);
		wait_clk(100);
		assert result = x"02" and error = '0' report "failed at " & test_case;

		test_case <= "clear               ";
		wait_clk(10);

		test_case <= "f_wr oc_wr          ";
		config <= OC_WRITE;
		wait_clk(5);
		toggle(read_file);
		wait_clk(100);
		assert result = x"01" and error = '0' report "failed at " & test_case;
		toggle(read_file);
		wait_clk(100);
		assert result = x"02" and error = '0' report "failed at " & test_case;

		test_case <= "clear               ";
		wait_clk(10);

		test_case <= "f_wr oc_prop        ";
		config <= OC_PROPRIETARY;
		file_title <= F_WRITE;
		wait_clk(5);
		toggle(read_file);
		wait_clk(100);
		assert result = x"00" and error = '0' report "failed at " & test_case;

		test_case <= "clear               ";
		wait_clk(10);

		test_case <= "f_prop oc_prop      ";
		config <= OC_PROPRIETARY;
		file_title <= F_PROP1;
		wait_clk(5);
		toggle(read_file);
		wait_clk(100);
		assert result = x"01" and error = '0' report "failed at " & test_case;
		file_title <= F_PROP2;
		wait_clk(5);
		toggle(read_file);
		wait_clk(100);
		assert result = x"02" and error = '0' report "failed at " & test_case;

		test_case <= "clear               ";
		wait_clk(10);

		test_case <= "oc_rd_range         ";
		config <= OC_RD_RANGE;
		file_title <= F_READ1;
		wait_clk(5);
		toggle(read_file);
		wait_clk(100);
		assert result = x"00" and error = '0' report "failed at " & test_case;
		file_title <= F_READ2;
		wait_clk(5);
		toggle(read_file);
		wait_clk(100);
		assert result = x"01" and error = '0' report "failed at " & test_case;
		file_title <= F_WRITE;
		wait_clk(5);
		toggle(read_file);
		wait_clk(100);
		assert result = x"01" and error = '0' report "failed at " & test_case;
		file_title <= F_FLUSH;
		wait_clk(5);
		toggle(read_file);
		wait_clk(100);
		assert result = x"01" and error = '0' report "failed at " & test_case;

		test_case <= "clear               ";
		wait_clk(10);

		test_case <= "oc_rw_range         ";
		config <= OC_RW_RANGE;
		file_title <= F_READ1;
		wait_clk(5);
		toggle(read_file);
		wait_clk(100);
		assert result = x"00" and error = '0' report "failed at " & test_case;
		file_title <= F_READ2;
		wait_clk(5);
		toggle(read_file);
		wait_clk(100);
		assert result = x"01" and error = '0' report "failed at " & test_case;
		file_title <= F_WRITE;
		wait_clk(5);
		toggle(read_file);
		wait_clk(100);
		assert result = x"02" and error = '0' report "failed at " & test_case;
		file_title <= F_FLUSH;
		wait_clk(5);
		toggle(read_file);
		wait_clk(100);
		assert result = x"02" and error = '0' report "failed at " & test_case;

		test_case <= "clear               ";
		wait_clk(10);

		test_case <= "simulation ended    ";
		report "test bench ended";
		wait;
	end process;

end Behavioral;


