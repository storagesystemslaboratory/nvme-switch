--------------------------------------------------------------------------------
--
-- Module Name: sim_assertion_adm
-- Create Date: 09/19/2019 (MM/DD/YYYY)
--
-- Company: ittChip - Unisinos
-- Engineer: Vinícius Gabriel LINDEN
-- 
-- Description: Stimulae assertion_adm
-- 
--------------------------------------------------------------------------------
-- Recommended span of 300 ns for period of 10 ns
-- TODO:


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library UNISIM;
use UNISIM.VComponents.all;

library work;
--use work.common_pkg.all;
--use work.kratus_pkg.all;

entity sim_assertion_adm is
end sim_assertion_adm;


architecture Behavioral of sim_assertion_adm is

	-- DO NOT DELETE
	constant period 		: time := 5 ns;
	constant hp 			: time := period/2; -- half period

	signal index 			: natural := 1; -- index for redability
	signal clk 				: std_logic := '1'; -- system base clock

	procedure wait_clk(constant T : in natural) is
	begin
		wait for t*period;
	end procedure;
	-- DELETION RESTRICTION ENDED

	-- UUT
	constant CLOCK_EDGE 	: std_logic := '1';
	signal wren 			: std_logic;
	signal full 			: std_logic := '0';
	signal direction 		: std_logic := '0';
	signal direction_store 	: std_logic;
	signal evaluate			: std_logic := '0';
	signal error 			: std_logic;
	signal rst 				: std_logic := '1';
	-- END OF UUT

	-- SIMULATION
	signal test_case 		: string(1 to 20) := "waiting             ";
-- END OF SIMULATION

begin

	UUT : entity work.rtl_assertion_adm
	generic map
	(
		CLOCK_EDGE => CLOCK_EDGE
	)
	port map
	(
		wren => wren,
		full => full,
		direction => direction,
		direction_store => direction_store,
		evaluate => evaluate,
		rst => rst,
		error => error,
		clk => clk
	);

	-- DO NOT DELETE
	clk <= not clk after hp;
	index <= index + 1 after period;
	-- DELETION RESTRICTION ENDED

	-- SEQUENTIAL FIELD GENERATOR
	direction <= not direction after period;
	-- END OF SEQUENTIAL FIELD GENERATOR

	process
	begin
		wait_clk(2);
		rst <= '0';
		wait_clk(2);

		test_case <= "clear               ";
		wait_clk(10);

		test_case <= "new_data std field  ";
		evaluate <= '1';
		wait_clk(1);
		evaluate <= '0';

		test_case <= "clear               ";
		wait_clk(10);

		test_case <= "no new_data std proc";
		evaluate <= '1';
		wait_clk(1);
		evaluate <= '0';

		test_case <= "clear               ";
		wait_clk(10);

		test_case <= "block by full       ";
		full <= '1';
		wait_clk(2);
		evaluate <= '1';
		wait_clk(1);
		evaluate <= '0';
		wait_clk(2);
		full <= '0';

		test_case <= "clear               ";
		wait_clk(10);

		test_case <= "full & eval race    "; -- test race condition
		full <= '1';
		evaluate <= '1';
		wait_clk(1);
		evaluate <= '0';
		full <= '0';

		test_case <= "clear               ";
		wait_clk(10);

		test_case <= "simulation ended    ";
		wait;
	end process;

	end Behavioral;


