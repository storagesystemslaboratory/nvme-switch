--------------------------------------------------------------------------------
--
-- Module Name: sim_FIFO
-- Create Date: 06/10/2019 (MM/DD/YYYY)
--
-- Company: ittChip - Unisinos
-- Engineer: Vinícius Gabriel LINDEN
-- 
-- Description: Stimulae for FIFO instantiation module
-- 
--------------------------------------------------------------------------------
-- Recommended span of 300 ns for period of 10 ns
-- TODO:


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library UNISIM;
use UNISIM.VComponents.all;

--library STD;
--use STD.TEXTIO.ALL;

library work;
use work.memory_pkg.all;
use work.kratus_pkg.all;

entity sim_FIFO is
end sim_FIFO;


architecture Behavioral of sim_FIFO is

	-- DO NOT DELETE
	constant period					: time := 5 ns;
	constant hp						: time := period/2; -- half period

	signal index					: natural := 1; -- index for redability
	signal clk						: std_logic := '1'; -- system base clock

	procedure wait_clk(constant T : in natural) is
	begin
		wait for t*period;
	end procedure;
	-- DELETION RESTRICTION ENDED

	-- INSTANTIATION PORTS
	constant FIRST_FALL_THROUGH 	: boolean := true;
	constant INDEPENDENT_CLOCK 		: boolean := true;
	constant DATA_WIDTH 			: integer := 4;
	constant SHORT 					: boolean := false;
	constant PROG_EMPTY_THRESH 		: integer := 2;
	constant PROG_FULL_THRESH 		: integer := 4;
	signal wrdata 					: std_logic_vector(DATA_WIDTH-1 downto 0);
	signal wrmeta 					: std_logic_vector(datawidth2paritywidth(DATA_WIDTH)-1 downto 0) := (others => '0');
	signal wren 					: std_logic;
	signal wrcount 					: std_logic_vector(13 downto 0);
	signal wrrstbusy 				: std_logic;
	signal wrrst 					: std_logic := '0';
	signal wrerror 					: std_logic;
	signal wrclk 					: std_logic;
	signal rddata 					: std_logic_vector(DATA_WIDTH-1 downto 0);
	signal rdmeta 					: std_logic_vector(datawidth2paritywidth(DATA_WIDTH)-1 downto 0);
	signal rden 					: std_logic;
	signal rdcount 					: std_logic_vector(13 downto 0);
	signal rdrstbusy 				: std_logic;
	signal rderror 					: std_logic;
	signal rdclk 					: std_logic;
	signal empty 					: std_logic;
	signal almostempty 				: std_logic;
	signal full 					: std_logic;
	signal almostfull 				: std_logic;
	-- END OF INSTANTIATION PORTS

	-- SIMULATION
	signal test_case 				: string(1 to 20) := "waiting             ";
	signal rd_next 					: std_logic := '0';
	signal wr_next 					: std_logic := '0';
	signal rden_select 				: std_logic := '1'; -- use automatic signal control
	signal wren_select 				: std_logic := '1'; -- use automatic signal control
	signal rden_mux 				: std_logic := '0';
	signal wren_mux 				: std_logic := '0';
	signal rden_ctrl 				: std_logic := '0';
	signal wren_ctrl 				: std_logic := '0';
-- END OF SIMULATION

begin

	UUT : entity work.inst_FIFO
	generic map
	(
		FIRST_FALL_THROUGH => FIRST_FALL_THROUGH,
		INDEPENDENT_CLOCK => INDEPENDENT_CLOCK,
		DATA_WIDTH => DATA_WIDTH,
		SHORT => SHORT,
		PROG_EMPTY_THRESH => PROG_EMPTY_THRESH,
		PROG_FULL_THRESH => PROG_FULL_THRESH
	)
	port map
	(
		wrdata => wrdata,
		wrmeta => wrmeta,
		wren => wren,
		wrcount => wrcount,
		wrrstbusy => wrrstbusy,
		wrrst => wrrst,
		wrerror => wrerror,
		wrclk => wrclk,
		rddata => rddata,
		rdmeta => rdmeta,
		rden => rden,
		rdcount => rdcount,
		rdrstbusy => rdrstbusy,
		rderror => rderror,
		rdclk => rdclk,
		empty => empty,
		almostempty => almostempty,
		full => full,
		almostfull => almostfull
	);

	-- DO NOT DELETE
	clk <= not clk after hp;
	index <= index + 1 after period;
	-- DELETION RESTRICTION ENDED

	-- READ CONTROL
	process
	begin
		wait until rd_next'event;
		rden_mux <= '1';
		wait_clk(1);
		rden_mux <= '0';
	end process;
	rden <= rden_mux when rden_select = '1' else rden_ctrl;
	-- END OF READ CONTROL

	-- WRITE CONTROL
	process
	begin
		wait until wr_next'event;
		wren_mux <= '1';
		wait_clk(1);
		wren_mux <= '0';
	end process;
	wren <= wren_mux when wren_select = '1' else wren_ctrl;
	-- END OF WRITE CONTROL

	-- SEQUENTIAL DATA GENERATOR
	process
	begin
		wait until clk = '1' and wren = '1';
		wrdata <= std_logic_vector(unsigned(wrdata)+1);
	end process;
	-- END OF SEQUENTIAL DATA GENERATOR

	-- WRITE META GENERATOR
	process
	begin
		wait_clk(3);
		wrmeta <= not wrmeta;
	end process;
	-- END OF WRITE META GENERATOR

	-- CLOCK ASSIGNMENT
	wrclk <= clk;
	rdclk <= clk;
	-- END OF CLOCK ASSIGNMENT

	process
	begin
		wait_clk(5);
		test_case <= "quick write         ";
		for i in 1 to 10 loop
			toggle(wr_next);
			wait_clk(1);
		end loop;
		wait_clk(10);
		test_case <= "quick read          ";
		for i in 1 to 10 loop
			toggle(rd_next);
			wait_clk(1);
		end loop;
		wait_clk(10);
		test_case <= "quick test 1        ";
		for i in 1 to 3 loop
			toggle(wr_next);
			wait_clk(1);
		end loop;
		wait_clk(5);
		for i in 1 to 3 loop
			toggle(rd_next);
			wait_clk(5);
		end loop;
		wait_clk(10);
		test_case <= "quick test 2        ";
		for i in 1 to 3 loop
			toggle(wr_next);
			wait_clk(1);
		end loop;
		--wait_clk(1);
		for i in 1 to 3 loop
			toggle(rd_next);
			wait_clk(5);
		end loop;
		toggle(rd_next);
		wait_clk(10);
		test_case <= "write and read      ";
		for i in 1 to 5 loop
			toggle(wr_next);
			wait_clk(1);
			toggle(rd_next);
		end loop;
		wait_clk(10);
		test_case <= "reset with write    ";
		for i in 1 to 5 loop
			toggle(wr_next);
			wait_clk(1);
		end loop;
		wrrst <= '1';
		wait_clk(10);
		wrrst <= '0';
		wait until wrrstbusy = '0';
		wait_clk(5);
		test_case <= "until full write    ";
		while full = '0' loop
			toggle(wr_next);
			wait_clk(1);
		end loop;
		test_case <= "until empty read    ";
		while empty = '0' loop
			toggle(rd_next);
			wait_clk(1);
		end loop;
		wait_clk(10);
		test_case <= "leave as it is      ";
		wren_select <= '0';
		rden_select <= '0';
		wren_ctrl <= '1';
		wait_clk(3);
		rden_ctrl <= '1';
		wait;
	end process;

end Behavioral;



