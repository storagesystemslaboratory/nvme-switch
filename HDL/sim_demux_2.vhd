--------------------------------------------------------------------------------
--
-- Module Name: sim_demux_2
-- Create Date: 08/27/2019 (MM/DD/YYYY)
--
-- Company: ittChip - Unisinos
-- Engineer: Vinícius Gabriel LINDEN
-- 
-- Description: Stimulae for demux_2
-- 
--------------------------------------------------------------------------------
-- Recommended span of 300 ns for period of 10 ns
-- TODO:


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
--use IEEE.NUMERIC_STD.ALL;

library UNISIM;
use UNISIM.VComponents.all;

--library STD;
--use STD.TEXTIO.ALL;

--library work;
--use work.common_pkg.all;
--use work.kratus_pkg.all;

entity sim_demux_2 is
	end sim_demux_2;


architecture Behavioral of sim_demux_2 is

	-- DO NOT DELETE
	constant period                 : time := 5 ns;
	constant hp                     : time := period/2; -- half period

	signal index                    : natural := 1; -- index for redability
	signal clk                      : std_logic := '1'; -- system base clock

	procedure wait_clk(constant T : in natural) is
	begin
		wait for t*period;
	end procedure;
	-- DELETION RESTRICTION ENDED

	-- SIMULATION
	-- END OF SIMULATION

	-- UUT
	constant DATA_WIDTH     : natural := 2;
	signal sel              : std_logic := '1';
	signal iput             : std_logic_vector(DATA_WIDTH-1 downto 0) := (others => '1');
	signal oput1_no         : std_logic_vector(DATA_WIDTH-1 downto 0);
	signal oput2_no         : std_logic_vector(DATA_WIDTH-1 downto 0);
	signal oput1_do         : std_logic_vector(DATA_WIDTH-1 downto 0);
	signal oput2_do         : std_logic_vector(DATA_WIDTH-1 downto 0);
-- END OF UUT

begin

	NO_IDLE : entity work.rtl_demux_2
	generic map(
				   DATA_WIDTH => DATA_WIDTH,
				   NO_IDLE_VALUE => true,
				   UNSELECTED_VALUE_1 => "00",
				   UNSELECTED_VALUE_2 => "00"
			   )
	port map (
				 sel => sel,
				 iput => iput,
				 oput1 => oput1_no,
				 oput2 => oput2_no
			 );

	IDLE : entity work.rtl_demux_2
	generic map(
				   DATA_WIDTH => DATA_WIDTH,
				   NO_IDLE_VALUE => false,
				   UNSELECTED_VALUE_1 => "11",
				   UNSELECTED_VALUE_2 => "00"
			   )
	port map (
				 sel => sel,
				 iput => iput,
				 oput1 => oput1_do,
				 oput2 => oput2_do
			 );

	-- DO NOT DELETE
	clk <= not clk after hp;
	index <= index + 1 after period;
	-- DELETION RESTRICTION ENDED

	process(clk)
	begin
		iput <= not iput;
	end process;

	process(clk)
	begin
		if clk = '1' then
			sel <= not sel;
		end if;
	end process;

end Behavioral;


