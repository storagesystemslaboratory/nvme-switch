--------------------------------------------------------------------------------
--
-- Module Name: sim_demux_NVMe
-- Create Date: 10/03/2019 (MM/DD/YYYY)
--
-- Company: ittChip - Unisinos
-- Engineer: Vinícius Gabriel LINDEN
-- 
-- Description: Stimulae demux_NVMe
-- 
--------------------------------------------------------------------------------
-- Recommended span of 300 ns for period of 10 ns
-- TODO:


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library UNISIM;
use UNISIM.VComponents.all;

--library work;
--use work.common_pkg.all;
--use work.kratus_pkg.all;

entity sim_demux_NVMe is
	end sim_demux_NVMe;


architecture Behavioral of sim_demux_NVMe is

	-- DO NOT DELETE
	constant period                 : time := 5 ns;
	constant hp                     : time := period/2; -- half period

	signal index                    : natural := 1; -- index for redability
	signal clk                      : std_logic := '1'; -- system base clock

	procedure wait_clk(constant T : in natural) is
	begin
		wait for t*period;
	end procedure;
	-- DELETION RESTRICTION ENDED

	-- UUT
	signal stream_source   : std_logic_vector(7 downto 0) := (others => '0');
	signal valid_source         : std_logic := '0';
	signal ready_source         : std_logic;
	signal stream_destin1  : std_logic_vector(7 downto 0);
	signal valid_destin1        : std_logic;
	signal ready_destin1        : std_logic := '0';
	signal stream_destin2  : std_logic_vector(7 downto 0);
	signal valid_destin2        : std_logic;
	signal ready_destin2        : std_logic := '0';
	signal sel                  : std_logic := '0';
-- END OF UUT

-- SIMULATION
-- END OF SIMULATION

begin

	UUT : entity work.rtl_demux_NVMe
	port map (
				 stream_source => stream_source,
				 valid_source => valid_source,
				 ready_source => ready_source,
				 stream_destin1 => stream_destin1,
				 valid_destin1 => valid_destin1,
				 ready_destin1 => ready_destin1,
				 stream_destin2 => stream_destin2,
				 valid_destin2 => valid_destin2,
				 ready_destin2 => ready_destin2,
				 sel => sel
			 );

	-- DO NOT DELETE
	clk <= not clk after hp;
	index <= index + 1 after period;
	-- DELETION RESTRICTION ENDED

	process
	begin
		wait for 1*period;
		sel <= not sel;
	end process;

	process
	begin
		wait for 3*period;
		valid_source <= not valid_source;
	end process;

	process
	begin
		wait for 2*period;
		ready_destin1 <= not ready_destin1;
	end process;

	ready_destin2 <= not ready_destin1;

	process
	begin
		for i in 0 to 255 loop
			stream_source <= std_logic_vector(to_unsigned(i,8));
			wait for 1*period;
		end loop;
	end process;

end Behavioral;


