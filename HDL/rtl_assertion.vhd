--------------------------------------------------------------------------------
--
-- Module Name: rtl_assertion
-- Create Date: 09/19/2019 (MM/DD/YYYY)
-- Revision: 1
--
-- Company: ittChip - Unisinos
-- Engineer: Vinícius Gabriel LINDEN
-- 
-- Description: Real-time logic deciding if the command will go to the SSD or the
--              generic SSD. This is the heart of the arbitrator module.
-- 
--------------------------------------------------------------------------------
-- TODO:

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
--use IEEE.NUMERIC_STD.ALL;

library UNISIM;
use UNISIM.VComponents.all;

library work;
use work.NVMe_pkg.all;
--use work.common_pkg.all;

entity rtl_assertion is
	generic
	(
		CLOCK_EDGE 			: std_logic := '1' -- '1' for rising, '0' for falling
	);
	port
	(
		-- CONFIGURATION REGISTER
		config 				: in std_logic_vector(7 downto 0); -- configuration for arbitration
		config_updated 		: in std_logic; -- '1' if new configuration has been sent

		-- FIELD CAPTURE
		opcode 				: in std_logic_vector(7 downto 0); -- NVMe command
		evaluate			: in std_logic;
		field 				: in std_logic_vector(63 downto 0); -- captured data output for storage
		cmd_id 				: in std_logic_vector(15 downto 0); -- command identifier

		-- ADMINISTRATOR (FROM FIFO)
		direction 			: out std_logic; -- direction of the field (to generic or to SSD)
		cmd_id_adm 			: out std_logic_vector(15 downto 0); -- command identifier
		new_available 		: out std_logic; -- there is new field to fetch
		next_direction 		: in std_logic; -- next from FIFO

		-- GENERAL PURPOSE
		rst					: in std_logic; -- async reset
		error				: out std_logic; -- error
		clk					: in std_logic -- clock
	);
end rtl_assertion;

architecture rtl of rtl_assertion is

	signal config_comp 		: std_logic_vector(7 downto 0);
	signal value1 			: std_logic_vector(63 downto 0);
	signal value2 			: std_logic_vector(63 downto 0);
	signal oput 			: std_logic;
	signal din 				: std_logic;
	signal error_adm 		: std_logic;

	signal wrmeta 			: std_logic_vector(1 downto 0);
	signal rdmeta 			: std_logic_vector(1 downto 0);
	signal wren 			: std_logic;
	signal full 			: std_logic;
	signal empty 			: std_logic;
	signal wrerror 			: std_logic;
	signal rderror 			: std_logic;
	signal error_fifo 		: std_logic;

begin

	error <= error_adm or error_fifo;

	COMPARISON_CONFIGURATOR : entity work.rtl_arbitrator_comp_config
	port map
	(
		config_ac => config,
		config_comparison => config_comp,
		value1 => value1,
		value2 => value2
	);

	FULL_COMPARISON : entity work.rtl_full_comparison_operation
	generic map
	(
		DATA_WIDTH => 64,
		VALUE_HIGH => TO_SSD, -- do not change
		INVERT_WHEN_RANGE => true
	)
	port map
	(
		config => config_comp,
		value1 => value1,
		value2 => value2,
		data => field,
		oput => oput
	);

	COMPARISON_EVALUATION : entity work.rtl_assertion_comp_eval
	generic map
	(
		DEFAULT_DIRECTION => TO_SSD
	)
	port map
	(
		config_ac => config,
		opcode => opcode,
		direction => oput,
		eval_direction => din
	);

	ADMINISTRATOR : entity work.rtl_assertion_adm
	generic map
	(
		CLOCK_EDGE => CLOCK_EDGE
	)
	port map (
		wren => wren,
		full => full,
		direction => '0',
		direction_store => open,
		evaluate => evaluate,
		rst => rst,
		error => error_adm,
		clk => clk
	);

	wrmeta <= (0 => din, others => '0');
	direction <= rdmeta(0);
	error_fifo <= wrerror or rderror;
	new_available <= not empty;
	TINY_FIFO : entity work.inst_FIFO
	generic map
	(
		FIRST_FALL_THROUGH => false,
		INDEPENDENT_CLOCK => false,
		DATA_WIDTH => 16,
		SHORT => true,
		PROG_EMPTY_THRESH => 10, -- don't care
		PROG_FULL_THRESH => 10 -- don't care
	)
	port map
	(
		wrdata => cmd_id,
		wrmeta => wrmeta,
		wren => wren,
		wrcount => open,
		wrrstbusy => open,
		wrrst => rst,
		wrerror => wrerror,
		wrclk => clk,
		rddata => cmd_id_adm,
		rdmeta => rdmeta,
		rden => next_direction,
		rdcount => open,
		rdrstbusy => open,
		rderror => rderror,
		rdclk => clk,
		empty => empty,
		almostempty => open,
		full => full,
		almostfull => open
	);

end rtl;

