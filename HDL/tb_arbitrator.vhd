--------------------------------------------------------------------------------
--
-- Module Name: tb_arbitrator
-- Create Date: 09/30/2019 (MM/DD/YYYY)
--
-- Company: ittChip - Unisinos
-- Engineer: Vinícius Gabriel LINDEN
-- 
-- Description: Testbench for arbitrator
-- 
--------------------------------------------------------------------------------
-- Recommended span of 300 ns for period of 10 ns
-- TODO:


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library UNISIM;
use UNISIM.VComponents.all;

library STD;
use STD.TEXTIO.ALL;

library work;
use work.common_pkg.all;
use work.kratus_pkg.all;
use work.NVMe_pkg.all;
use work.std_cmds_pkg.all;

entity tb_arbitrator is
end tb_arbitrator;


architecture Behavioral of tb_arbitrator is

	-- DO NOT DELETE
	constant period 		: time := 5 ns;
	constant hp 			: time := period/2; -- half period

	signal index 			: natural := 1; -- index for redability
	signal clk 				: std_logic := '1'; -- system base clock

	procedure wait_clk(constant T : in natural) is
	begin
		wait for t*period;
	end procedure;
	-- DELETION RESTRICTION ENDED

	-- TESTBENCH FILE
	constant WIDTH_INPUT 	: integer := 8; -- number of input characters in a line
	file file_input 		: text;
	signal read_input 		: std_logic_vector(WIDTH_INPUT-1 downto 0);
	signal enable_input 	: std_logic := '0'; -- control signal to enable input read
	signal stream_ended 	: std_logic := '1'; -- indicates if NVMe stream has ended
	-- END OF TESTBENCH FILE

	-- INPUT FILE RELATED
	constant F_READ1 		: string(1 to 16) := "read1_______.txt";
	constant F_READ2 		: string(1 to 16) := "read2_______.txt";
	constant F_FLUSH 		: string(1 to 16) := "flush_______.txt";
	constant F_WRITE 		: string(1 to 16) := "write_______.txt";
	constant F_PROP1 		: string(1 to 16) := "NVMe_SQ_B_01.txt";
	constant F_PROP2 		: string(1 to 16) := "NVMe_SQ_B_02.txt";
	signal file_title 		: string(1 to 16) := F_PROP1; -- file currently being process
	-- END OF INPUT FILE RELATED

	-- UUT
	constant CLEAR_ON_BOTH 	: boolean := false;
	constant CLOCK_EDGE 	: std_logic := '1';
	signal config 			: std_logic_vector(7 downto 0) := (others => '0');
	signal config_updated 	: std_logic := '0';
	signal stream 			: std_logic_vector(7 downto 0) := (others => '0');
	signal valid 			: std_logic := '0';
	signal ready 			: std_logic;
	signal full 			: std_logic := '0'; -- not used
	signal empty 			: std_logic := '0'; -- not used
	signal unload 			: std_logic;
	signal unloaded 		: std_logic := '0';
	signal processing 		: std_logic := '0';
	signal number_pkgs 		: std_logic_vector(7 downto 0) := (others => '0'); -- not used
	signal cmd_id_filter 	: std_logic_vector(15 downto 0);
	signal submit 			: std_logic;
	signal response 		: std_logic := '0';
	signal generic_ask 		: std_logic := '0';
	signal generic_lock 	: std_logic;
	signal generic_flow 	: std_logic := TO_SSD;
	signal host2ssd 		: std_logic;
	signal error 			: std_logic;
	signal rst 				: std_logic := '1';
	-- END OF UUT

	-- SIMULATION
	signal test_case 		: string(1 to 20) := "waiting             ";
	signal read_file 		: std_logic := '0'; -- on/off control for input file and handshake control
	signal config_edge 		: std_logic_vector(7 downto 0) := (others => '0'); -- edge detection
	signal test 			: std_logic := '0';
	-- END OF SIMULATION

begin

	UUT : entity work.rtl_arbitrator
	generic map
	(
		CLEAR_ON_BOTH => CLEAR_ON_BOTH,
		CLOCK_EDGE => CLOCK_EDGE
	)
	port map
	(
		config => config,
		config_updated => config_updated,
		stream => stream,
		valid => valid,
		ready => ready,
		full => full,
		empty => empty,
		unload => unload,
		unloaded => unloaded,
		processing => processing,
		number_pkgs => number_pkgs,
		cmd_id_filter => cmd_id_filter,
		submit => submit,
		response => response,
		generic_ask => generic_ask,
		generic_lock => generic_lock,
		generic_flow => generic_flow,
		host2ssd => host2ssd,
		error => error,
		rst => rst,
		clk => clk
	);

	-- DO NOT DELETE
	clk <= not clk after hp;
	index <= index + 1 after period;
	-- DELETION RESTRICTION ENDED

	-- TESTBENCH FILE
	READ_PROCESS : process -- read
		variable line_input               : line;
		variable bit_input                : bit_vector(WIDTH_INPUT-1 downto 0);
	begin
		stream_ended <= '1';
		wait until enable_input'event and enable_input = '1';
		stream_ended <= '0';
		file_open(file_input,file_title,read_mode);
		while not endfile(file_input) loop
			if enable_input = '1' then
				readline(file_input,line_input);
				read(line_input,bit_input);
				read_input <= to_stdlogicvector(bit_input);
			end if;
			wait for period;
		end loop;
		file_close(file_input);
	end process READ_PROCESS;
	-- END OF TESTBENCH FILE

	-- INPUT DATA CONTROL
	stream <= read_input;
	-- END OF INPUT DATA CONTROL

	-- READ FILE AUTOMATIC CONTROL
	process
	begin
		enable_input <= '0';
		wait until read_file'event;
		enable_input <= '1';
		wait_clk(64);
	end process;
	-- END OF READ FILE AUTOMATIC CONTROL

	-- VALID CYCLE CONTROL
	process
	begin
		valid <= '0';
		wait until read_file'event;
		valid <= '1';
		wait_clk(1);
	end process;
	-- END OF VALID CYCLE CONTROL

	-- CONFIG_UPDATED AUTOMATIC CONTROL
	config_edge <= config after period;
	config_updated <= '1' when config_edge /= config else '0';
	-- END OF CONFIG_UPDATED AUTOMATIC CONTROL

	response <= submit;
	unloaded <= not processing;

	process
	begin
		report "simulation began";
		wait_clk(2);
		rst <= '0';
		wait_clk(2);

		test_case <= "clear               ";
		wait_clk(10);

		test_case <= "idled (SSD)         ";
		wait for 1 ns;
		report test_case;
		config <= AC_IDLE;
		file_title <= F_PROP2;
		wait_clk(5);
		toggle(read_file);
		wait until unload = '1';
		wait_clk(1); -- wait for host2ssd stabilize
		assert cmd_id_filter = STD_CMD_PROP2_ID report "cmd id" severity warning;
		assert host2ssd = TO_SSD report "host2ssd" severity warning;
		assert error = '0' report "error" severity warning;
		processing <= '1';
		wait_clk(1);
		processing <= '0';
		wait_clk(1);
		assert unload = '0' report "unload" severity warning;
		assert error = '0' report "error" severity warning;

		test_case <= "clear               ";
		wait_clk(10);

		test_case <= "fixed to SSD        ";
		wait for 1 ns;
		report test_case;
		config <= AC_SEND_SSD;
		file_title <= F_PROP1;
		wait_clk(5);
		toggle(read_file);
		wait until unload = '1';
		wait_clk(1); -- wait for host2ssd stabilize
		assert cmd_id_filter = STD_CMD_PROP1_ID report "cmd id" severity warning;
		assert host2ssd = TO_SSD report "host2ssd" severity warning;
		assert error = '0' report "error" severity warning;
		processing <= '1';
		wait_clk(1);
		processing <= '0';
		wait_clk(1);
		assert unload = '0' report "unload" severity warning;
		assert error = '0' report "error" severity warning;

		test_case <= "clear               ";
		wait_clk(10);

		test_case <= "fixed to generic SSD";
		wait for 1 ns;
		report test_case;
		config <= AC_SEND_GENERIC;
		file_title <= F_PROP2;
		wait_clk(5);
		toggle(read_file);
		wait until unload = '1';
		wait_clk(1); -- wait for host2ssd stabilize
		assert cmd_id_filter = STD_CMD_PROP2_ID report "cmd id" severity warning;
		assert host2ssd = TO_GENERIC report "host2ssd" severity warning;
		assert error = '0' report "error" severity warning;
		processing <= '1';
		wait_clk(1);
		processing <= '0';
		wait_clk(1);
		assert unload = '0' report "unload" severity warning;
		assert error = '0' report "error" severity warning;

		test_case <= "clear               ";
		wait_clk(10);

		test_case <= "multiple entries    ";
		wait for 1 ns;
		report test_case;
		config <= AC_SEND_GENERIC;
		file_title <= F_WRITE;
		toggle(read_file);
		wait until unload = '1';
		wait_clk(1); -- wait for host2ssd stabilize
		assert cmd_id_filter = STD_CMD_WRITE_ID report "cmd id" severity warning;
		assert host2ssd = TO_GENERIC report "host2ssd" severity warning;
		assert error = '0' report "error" severity warning;
		report "Loading one more command";
		file_title <= F_FLUSH;
		toggle(read_file);
		wait until stream_ended = '1';
		processing <= '1';
		wait_clk(1);
		processing <= '0';
		report "unload last event: " & time'image(unload'last_event);
		wait until unload = '1';
		wait_clk(2);
		processing <= '1';
		wait_clk(1);
		processing <= '0';
		wait_clk(2);
		assert cmd_id_filter = STD_CMD_FLUSH_ID report "cmd id" severity warning;
		assert host2ssd = TO_GENERIC report "host2ssd" severity warning;
		assert error = '0' report "error" severity warning;

		test_case <= "clear               ";
		wait_clk(10);

		test_case <= "test configuration  ";
		wait for 1 ns;
		report test_case;
		config <= AC_TEST;
		file_title <= F_READ1; -- TO_SSD
		wait_clk(1);
		toggle(read_file);
		wait until unload = '1';
		wait_clk(1); -- wait for host2ssd stabilize
		assert cmd_id_filter = STD_CMD_READ1_ID report "cmd id" severity warning;
		assert host2ssd = TO_SSD report "host2ssd" severity warning;
		assert error = '0' report "error" severity warning;
		processing <= '1';
		wait_clk(1);
		processing <= '0';
		wait_clk(1);
		assert unload = '0' report "unload" severity warning;
		assert error = '0' report "error" severity warning;
		report "second file";
		file_title <= F_READ2; -- TO_SSD
		wait_clk(1);
		toggle(read_file);
		wait until unload = '1';
		wait_clk(1); -- wait for host2ssd stabilize
		assert cmd_id_filter = STD_CMD_READ2_ID report "cmd id" severity warning;
		assert host2ssd = TO_GENERIC report "host2ssd" severity warning;
		assert error = '0' report "error" severity warning;
		processing <= '1';
		wait_clk(1);
		processing <= '0';
		wait_clk(1);
		assert unload = '0' report "unload" severity warning;
		assert error = '0' report "error" severity warning;

		test_case <= "clear               ";
		wait_clk(10);

		test_case <= "simulation ended    ";
		report "simulation ended";
		wait;
	end process;

end Behavioral;


