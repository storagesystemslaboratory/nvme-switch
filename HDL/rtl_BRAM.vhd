--------------------------------------------------------------------------------
--
-- Module Name: rtl_BRAM
-- Create Date: 09/02/2019 (MM/DD/YYYY)
-- Revision: 0
--
-- Company: ittChip - Unisinos
-- Engineer: Vinícius Gabriel LINDEN
-- 
-- Description: Instantiation of BRAM for any data width, with
--              True Dual Port (TDP) for parallel access (page 33)
--              The extra bits in the ECC will be used for metadata
-- 
--------------------------------------------------------------------------------
-- TODO:
--  Implement a NxBRAM

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

library UNISIM;
use UNISIM.VComponents.all;

library work;
use work.memory_pkg.all;

entity rtl_BRAM is
	generic (
				RD_CTRL         : boolean := false; -- true: disables read in port B when A is writing in the same address
				SHORT           : boolean := false; -- true: 18Kb; false: 36Kb (for width greathen than 32, this option is useless)
				DATA_WIDTH      : integer := 8 -- BRAM port width (8, 16, 32, 64)
			);
	port (
		-- PORT A
			 addra           : in std_logic_vector(BRAM_width(DATA_WIDTH,SHORT)-1 downto 0); -- address
			 wrdataa         : in std_logic_vector(DATA_WIDTH-1 downto 0); -- write data
			 wrmetaa         : in std_logic_vector(datawidth2paritywidth(DATA_WIDTH)-1 downto 0); -- write metadata
			 rddataa         : out std_logic_vector(DATA_WIDTH-1 downto 0); -- read data
			 rdmetaa         : out std_logic_vector(datawidth2paritywidth(DATA_WIDTH)-1 downto 0); -- read metadata
			 wrena           : in std_logic_vector(datawidth2paritywidth(DATA_WIDTH)-1 downto 0); -- write per byte
			 rdena           : in std_logic; -- read enable
			 clka            : in std_logic; -- clock
			 rsta            : in std_logic; -- reset

		-- PORT B
			 addrb           : in std_logic_vector(BRAM_width(DATA_WIDTH,SHORT)-1 downto 0); -- address
			 wrdatab         : in std_logic_vector(DATA_WIDTH-1 downto 0); -- write data
			 wrmetab         : in std_logic_vector(datawidth2paritywidth(DATA_WIDTH)-1 downto 0); -- write metadata
			 rddatab         : out std_logic_vector(DATA_WIDTH-1 downto 0); -- read data
			 rdmetab         : out std_logic_vector(datawidth2paritywidth(DATA_WIDTH)-1 downto 0); -- read metadata
			 wrenb           : in std_logic_vector(datawidth2paritywidth(DATA_WIDTH)-1 downto 0); -- write per byte
			 rdenb           : in std_logic; -- read enable
			 enableb         : out std_logic; -- read is enabled
			 clkb            : in std_logic; -- clock
			 rstb            : in std_logic -- reset
		 );
end rtl_BRAM;

architecture rtl of rtl_BRAM is

	constant DATA_WIDTH_SAT     : integer := saturate_at_32(DATA_WIDTH);
	constant PARITY_WIDTH_SAT   : integer := datawidth2paritywidth(DATA_WIDTH_SAT);
	constant PARITY_WIDTH       : integer := datawidth2paritywidth(DATA_WIDTH);
	signal wrena_red            : std_logic;
	signal rdenb_i              : std_logic;

begin

	RD_DISABLE_CTRL : if RD_CTRL generate

		OR_RED : entity work.rtl_reduce_operations
		generic map (
						DATA_WIDTH => PARITY_WIDTH,
						OPERATION => 0
					)
		port map (
					 data => wrena,
					 result => wrena_red
				 );

		rdenb_i <= '0' when addra = addrb and wrena_red = '1' else rdenb;
		enableb <= rdenb_i;

	end generate RD_DISABLE_CTRL;

	RD_ENABLED : if not RD_CTRL generate
		rdenb_i <= rdenb;
		enableb <= rdenb_i;
	end generate RD_ENABLED;

	IS_64W : if DATA_WIDTH = 64 generate
		BRAM_LEFT : entity work.inst_BRAM
		generic map (
						SHORT => SHORT,
						DATA_WIDTH => DATA_WIDTH_SAT
					)
		port map (
					 addra => addra,
					 wrdataa => wrdataa(DATA_WIDTH-1 downto DATA_WIDTH_SAT),
					 wrmetaa => wrmetaa(PARITY_WIDTH-1 downto PARITY_WIDTH_SAT),
					 rddataa => rddataa(DATA_WIDTH-1 downto DATA_WIDTH_SAT),
					 rdmetaa => rdmetaa(PARITY_WIDTH-1 downto PARITY_WIDTH_SAT),
					 wrena => wrena(PARITY_WIDTH-1 downto PARITY_WIDTH_SAT),
					 rdena => rdena,
					 clka => clka,
					 rsta => rsta,
					 addrb => addrb,
					 wrdatab => wrdatab(DATA_WIDTH-1 downto DATA_WIDTH_SAT),
					 wrmetab => wrmetab(PARITY_WIDTH-1 downto PARITY_WIDTH_SAT),
					 rddatab => rddatab(DATA_WIDTH-1 downto DATA_WIDTH_SAT),
					 rdmetab => rdmetab(PARITY_WIDTH-1 downto PARITY_WIDTH_SAT),
					 wrenb => wrenb(PARITY_WIDTH-1 downto PARITY_WIDTH_SAT),
					 rdenb => rdenb_i,
					 clkb => clkb,
					 rstb => rstb
				 );
	end generate IS_64W;

	BRAM_RIGHT : entity work.inst_BRAM
	generic map (
					SHORT => SHORT,
					DATA_WIDTH => DATA_WIDTH_SAT
				)
	port map (
				 addra => addra,
				 wrdataa => wrdataa(DATA_WIDTH_SAT-1 downto 0),
				 wrmetaa => wrmetaa(PARITY_WIDTH_SAT-1 downto 0),
				 rddataa => rddataa(DATA_WIDTH_SAT-1 downto 0),
				 rdmetaa => rdmetaa(PARITY_WIDTH_SAT-1 downto 0),
				 wrena => wrena(PARITY_WIDTH_SAT-1 downto 0),
				 rdena => rdena,
				 clka => clka,
				 rsta => rsta,
				 addrb => addrb,
				 wrdatab => wrdatab(DATA_WIDTH_SAT-1 downto 0),
				 wrmetab => wrmetab(PARITY_WIDTH_SAT-1 downto 0),
				 rddatab => rddatab(DATA_WIDTH_SAT-1 downto 0),
				 rdmetab => rdmetab(PARITY_WIDTH_SAT-1 downto 0),
				 wrenb => wrenb(PARITY_WIDTH_SAT-1 downto 0),
				 rdenb => rdenb_i,
				 clkb => clkb,
				 rstb => rstb
			 );

end rtl;


