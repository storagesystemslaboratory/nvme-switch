--------------------------------------------------------------------------------
--
-- Module Name: rtl_statistics_framework
-- Create Date: 09/05/2019 (MM/DD/YYYY)
-- Revision: 1
--
-- Company: ittChip - Unisinos
-- Engineer: Vinícius Gabriel LINDEN
-- 
-- Description: Real-time logic for statistics framework
-- 
--------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
--use IEEE.NUMERIC_STD.ALL;
--use IEEE.STD_LOGIC_MISC.ALL;

library UNISIM;
use UNISIM.VComponents.all;

library work;
use work.NVMe_pkg.all;

entity rtl_statistics_framework is
	generic
	(
		SUBMISSION_CMD		: boolean := true; -- true: submission command; false: completion command
		CLOCK_EDGE			: std_logic := '1' -- '1' for rising, '0' for falling
	);
	port
	(
		-- NVMe PACKAGE
		stream				: in std_logic_vector(7 downto 0); -- byte data size
		valid				: in std_logic; -- sending new data stream
		ready				: out std_logic; -- do not send new stream, module is busy

		-- FRAMEWORK
		stream_delayed		: out std_logic_vector(7 downto 0); -- delayed stream (framework)
		opcode				: out std_logic_vector(7 downto 0); -- NVMe command
		count				: out std_logic_vector(5 downto 0); -- counter
		cmd_id				: out std_logic_vector(15 downto 0); -- command identifier
		last				: out std_logic; -- last byte of command
		last_delayed		: out std_logic; -- delayed last
		evaluate			: out std_logic; -- evaluate result from opcode counter

		-- GENERAL PURPOSE
		error				: out std_logic;
		rst					: in std_logic;
		clk					: in std_logic
	);
end rtl_statistics_framework;

architecture rtl of rtl_statistics_framework is

	constant PACKAGE_LENGTH : integer := nvme_cmd_size(SUBMISSION_CMD);
	signal wren				: std_logic; -- writing the package
	signal saturated		: std_logic; -- counter has saturated
	signal last_delayed_i	: std_logic; -- counter has saturated
	signal count_i			: std_logic_vector(5 downto 0); -- internal count
	signal cmd_id_reg		: std_logic_vector(15 downto 0); -- command id register

begin

	last_delayed <= last_delayed_i;
	count <= count_i;
	last <= saturated;
	error <= '0';

	STREAM_RECEIVER : process(rst,clk)
	begin
		if rst = '1' then
			ready <= '0';
			wren <= '0';
		elsif clk'event and clk = '1' then
			if wren = '1' then -- writing a package
				if saturated = '1' then
					ready <= '1';
					wren <= '0';
				end if;
			elsif valid = '1' then -- receiving package
				opcode <= stream;
				ready <= '0';
				wren <= '1';
			else
				ready <= '1';
			end if;
		end if;
	end process STREAM_RECEIVER;

	CMD_ID_CAPTURE : process(rst,clk)
	begin
		if rst = '1' then
			elsif clk'event and clk = CLOCK_EDGE then
				case count_i is
					when "000001" => -- byte 2
						cmd_id_reg(7 downto 0) <= stream; -- first part
					when "000010" => -- byte 3
						cmd_id_reg(15 downto 8) <= stream; -- second part
					when "000011" =>
						cmd_id <= cmd_id_reg; -- free register
					when others =>
				end case;
			end if;
		end process CMD_ID_CAPTURE;

	COUNTER : entity work.rtl_counter
	generic map
	(
		CNT_RANGE => PACKAGE_LENGTH-1,
		CNT_RESET => 0,
		CNT_SATURATE_EN => false
	)
	port map
	(
		rst => rst,
		clk => clk,
		clk_en => '1',
		cnt_en => wren,
		cnt => count_i,
		sat => saturated
	);

	DELAY_STREAM : entity work.rtl_delay
	generic map
	(
		CLOCK_EDGE => CLOCK_EDGE,
		DELAY => 4,
		DATA_WIDTH => 8,
		REG_DELAY => false,
		INVERT_OUTPUT => '0'
	)
	port map
	(
		clk => clk,
		clk_en => '1',
		d => stream,
		q => stream_delayed
	);

	DELAY_LAST : entity work.rtl_delay
	generic map
	(
		CLOCK_EDGE => CLOCK_EDGE,
		DELAY => 1,
		DATA_WIDTH => 1,
		REG_DELAY => false,
		INVERT_OUTPUT => '0'
	)
	port map
	(
		clk => clk,
		clk_en => '1',
		d(0) => saturated, -- = last
		q(0) => last_delayed_i
	);

	EVALUATE_OC : entity work.rtl_delay
	generic map
	(
		CLOCK_EDGE => CLOCK_EDGE,
		DELAY => 1,
		DATA_WIDTH => 1,
		REG_DELAY => false,
		INVERT_OUTPUT => '0'
	)
	port map
	(
		clk => clk,
		clk_en => '1',
		d(0) => last_delayed_i,
		q(0) => evaluate
	);

end rtl;


