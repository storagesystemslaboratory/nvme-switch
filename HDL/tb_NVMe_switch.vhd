--------------------------------------------------------------------------------
--
-- Module Name: tb_NVMe_switch
-- Create Date: 10/04/2019 (MM/DD/YYYY)
--
-- Company: ittChip - Unisinos
-- Engineer: Vinícius Gabriel LINDEN
-- 
-- Description: Testbench for the intelligent switch assembly
-- 
--------------------------------------------------------------------------------
-- Recommended span of 300 ns for period of 10 ns
-- TODO:


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library UNISIM;
use UNISIM.VComponents.all;

library STD;
use STD.TEXTIO.ALL;

library work;
use work.common_pkg.all;
use work.kratus_pkg.all;
use work.NVMe_pkg.all;

entity tb_NVMe_switch is
end tb_NVMe_switch;


architecture Behavioral of tb_NVMe_switch is

	-- DO NOT DELETE
	constant period                 : time := 5 ns;
	constant hp                     : time := period/2; -- half period

	signal index                    : natural := 1; -- index for redability
	signal clk                      : std_logic := '1'; -- system base clock

	procedure wait_clk(constant T : in natural) is
	begin
		wait for t*period;
	end procedure;
	-- DELETION RESTRICTION ENDED

	-- TESTBENCH FILE
	constant WIDTH_INPUT            : integer := 8; -- number of input characters in a line
	file file_input                 : text;
	signal read_input               : std_logic_vector(WIDTH_INPUT-1 downto 0);
	signal enable_input             : std_logic := '0'; -- control signal to enable input read
	signal stream_ended             : std_logic := '1'; -- indicates if NVMe stream has ended
	-- END OF TESTBENCH FILE

	-- INPUT FILE RELATED
	constant F_READ1    		: string(1 to 16) := "read1_______.txt";
	constant F_READ2    		: string(1 to 16) := "read2_______.txt";
	constant F_FLUSH    		: string(1 to 16) := "flush_______.txt";
	constant F_WRITE    		: string(1 to 16) := "write_______.txt";
	constant F_PROP1    		: string(1 to 16) := "NVMe_SQ_B_01.txt";
	constant F_PROP2    		: string(1 to 16) := "NVMe_SQ_B_02.txt";
	signal file_title               : string(1 to 16) := F_PROP1; -- file currently being process
	-- END OF INPUT FILE RELATED

	-- UUT
	constant CLEAR_ON_BOTH          : boolean := false;
	constant CLOCK_EDGE             : std_logic := '1';
	signal stream_hostSC		: std_logic_vector(7 downto 0) := (others => '0');
	signal valid_hostSC             : std_logic := '0';
	signal ready_hostSC             : std_logic;
	signal stream_ssdSC        	: std_logic_vector(7 downto 0);
	signal valid_ssdSC              : std_logic;
	signal ready_ssdSC              : std_logic := '0';
	signal config_ac                : std_logic_vector(7 downto 0) := (others => '0');
	signal config_updated_ac        : std_logic := '0';
	signal error                    : std_logic;
	signal rst                      : std_logic := '1';
	-- END OF UUT

	-- SIMULATION
	signal test_case                : string(1 to 20) := "waiting             ";
	signal read_file                : std_logic := '0'; -- on/off control for input file and handshake control
	signal config_edge_ac           : std_logic_vector(7 downto 0) := (others => '0'); -- edge detection
	-- END OF SIMULATION

begin

	UUT : entity work.rtl_NVMe_switch
	generic map
	(
		CLEAR_ON_BOTH => CLEAR_ON_BOTH,
		CLOCK_EDGE => CLOCK_EDGE
	)
	port map
	(
		stream_hostSC => stream_hostSC,
		valid_hostSC => valid_hostSC,
		ready_hostSC => ready_hostSC,
		stream_ssdSC => stream_ssdSC,
		valid_ssdSC => valid_ssdSC,
		ready_ssdSC => ready_ssdSC,
		config_ac => config_ac,
		config_updated_ac => config_updated_ac,
		error => error,
		rst => rst,
		clk => clk
	);

	-- DO NOT DELETE
	clk <= not clk after hp;
	index <= index + 1 after period;
	-- DELETION RESTRICTION ENDED

	-- TESTBENCH FILE
	READ_PROCESS : process -- read
		variable line_input               : line;
		variable bit_input                : bit_vector(WIDTH_INPUT-1 downto 0);
	begin
		stream_ended <= '1';
		wait until enable_input'event and enable_input = '1'; -- and ready
		stream_ended <= '0';
		file_open(file_input,file_title,read_mode);
		while not endfile(file_input) loop
			if enable_input = '1' then
				readline(file_input,line_input);
				read(line_input,bit_input);
				read_input <= to_stdlogicvector(bit_input);
			end if;
			wait for period;
		end loop;
		file_close(file_input);
	end process READ_PROCESS;
	-- END OF TESTBENCH FILE

	-- INPUT DATA CONTROL
	stream_hostSC <= read_input;
	-- END OF INPUT DATA CONTROL


	-- READ FILE AUTOMATIC CONTROL
	process
	begin
		enable_input <= '0';
		wait until read_file'event;
		enable_input <= '1';
		wait_clk(64);
	end process;
	-- END OF READ FILE AUTOMATIC CONTROL

	-- VALID CYCLE CONTROL
	process
	begin
		valid_hostSC <= '0';
		wait until read_file'event;
		valid_hostSC <= '1';
		wait_clk(1);
	end process;
	-- END OF VALID CYCLE CONTROL

	-- CONFIG_UPDATED AUTOMATIC CONTROL
	config_edge_ac <= config_ac after period;
	config_updated_ac <= '1' when config_edge_ac /= config_ac else '0';
	-- END OF CONFIG_UPDATED AUTOMATIC CONTROL

	process
	begin
		wait_clk(2);
		rst <= '0';
		wait_clk(2);

		test_case <= "                    ";
		wait_clk(10);

		test_case <= "idle (fixed SSD)    ";
		config_ac <= AC_IDLE;
		file_title <= F_READ2;
		wait_clk(5);
		toggle(read_file);
		wait_clk(80);
		ready_ssdSC <= '1';
		wait_clk(1);
		ready_ssdSC <= '0';
		wait_clk(70);

		test_case <= "                    ";
		wait_clk(10);

		test_case <= "fixed generic SSD   ";
		config_ac <= AC_SEND_GENERIC;
		file_title <= F_FLUSH;
		wait_clk(5);
		toggle(read_file);
		wait_clk(80);
		ready_ssdSC <= '1';
		wait_clk(1);
		ready_ssdSC <= '0';
		wait_clk(70);

		test_case <= "                    ";
		wait_clk(10);

		test_case <= "fixed SSD           ";
		config_ac <= AC_SEND_SSD;
		file_title <= F_FLUSH;
		wait_clk(5);
		toggle(read_file);
		wait_clk(80);
		ready_ssdSC <= '1';
		wait_clk(1);
		ready_ssdSC <= '0';
		wait_clk(70);

		test_case <= "                    ";
		wait_clk(10);

		test_case <= "AC_TEST mult. entry ";
		config_ac <= AC_TEST;
		-- loading
		file_title <= F_READ1; -- first (to SSD, 0xB296)
		wait_clk(5);
		toggle(read_file);
		wait_clk(80);
		file_title <= F_READ2; -- second (to generic, 0xB397)
		wait_clk(5);
		toggle(read_file);
		wait_clk(80);
		file_title <= F_WRITE; -- third (to SSD, 0xB690)
		wait_clk(5);
		toggle(read_file);
		wait_clk(80);
		-- unloading
		ready_ssdSC <= '1'; -- first
		wait_clk(1);
		ready_ssdSC <= '0';
		wait_clk(80);
		ready_ssdSC <= '1'; -- second
		wait_clk(1);
		ready_ssdSC <= '0';
		wait_clk(80);
		ready_ssdSC <= '1'; -- third
		wait_clk(1);
		ready_ssdSC <= '0';
		wait_clk(80);
		wait_clk(5);

		test_case <= "                    ";
		wait_clk(10);

		test_case <= "                    ";
		wait;
	end process;

end Behavioral;


