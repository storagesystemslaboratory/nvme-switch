--------------------------------------------------------------------------------
--
-- Module Name: rtl_compare_operation
-- Create Date: 09/11/2019 (MM/DD/YYYY)
-- Revision: 1
--
-- Company: ittChip - Unisinos
-- Engineer: Vinícius Gabriel LINDEN
-- 
-- Description: Real-time logic for comparing two vectors numerically
--              Values are interpreted as unsigned
-- 
--------------------------------------------------------------------------------
-- TODO:
--  Implement HAND_MADE inequalities

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library UNISIM;
use UNISIM.VComponents.all;

entity rtl_compare_operation is
	generic (
				HAND_MADE           : boolean := false; -- true: user made, no choice left to synthesizer; false: synthesizer chooses
				DATA_WIDTH          : integer := 8 -- width of data [bit]
			);
	port (

		-- INPUTS
			 value1              : in std_logic_vector(DATA_WIDTH-1 downto 0);
			 value2              : in std_logic_vector(DATA_WIDTH-1 downto 0);

		-- INEQUALITY COMPARISON
			 greater             : out std_logic; -- value1 > value2
			 greater_equal       : out std_logic; -- value1 >= value2
			 less                : out std_logic; -- value1 < value2
			 less_equal          : out std_logic; -- value1 <= value2

		-- EQUALITY COMPARISON
			 equal               : out std_logic;
			 different           : out std_logic
		 );
end rtl_compare_operation;

architecture rtl of rtl_compare_operation is

	-- NOT HAND MADE SIGNALS
	signal unsig1           : unsigned(DATA_WIDTH-1 downto 0);
	signal unsig2           : unsigned(DATA_WIDTH-1 downto 0);
	-- END OF NOT HAND MADE SIGNALS

	-- HAND MADE SIGNALS
	signal value_xor        : std_logic_vector(DATA_WIDTH-1 downto 0);
	signal different_i      : std_logic;
-- END OF HAND MADE SIGNALS

begin

	IS_HAND_MADE : if HAND_MADE generate

		-- ASYNCHRONOUS LOGIC
		value_xor <= value1 xor value2;

		-- INEQUALITIES
		unsig1 <= unsigned(value1);
		unsig2 <= unsigned(value2);

		greater <= '1' when unsig1 > unsig2 else '0';
		greater_equal <= '1' when unsig1 >= unsig2 else '0';
		less <= '1' when unsig1 < unsig2 else '0';
		less_equal <= '1' when unsig1 <= unsig2 else '0';

		-- EQUALITIES
		OR_RED : entity work.rtl_reduce_operations
		generic map (
						DATA_WIDTH => DATA_WIDTH,
						OPERATION => 0
					)
		port map (
					 data => value_xor,
					 result => different_i
				 );
		different <= different_i;
		equal <= not different_i;

	end generate IS_HAND_MADE;

	NOT_HAND_MADE : if not HAND_MADE generate

		unsig1 <= unsigned(value1);
		unsig2 <= unsigned(value2);

		greater <= '1' when unsig1 > unsig2 else '0';
		greater_equal <= '1' when unsig1 >= unsig2 else '0';
		less <= '1' when unsig1 < unsig2 else '0';
		less_equal <= '1' when unsig1 <= unsig2 else '0';

		equal <= '1' when value1 = value2 else '0';
		different <= '1' when value1 /= value2 else '0';

	end generate NOT_HAND_MADE;

end rtl;


