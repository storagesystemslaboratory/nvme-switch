--------------------------------------------------------------------------------
--
-- Module Name: operations_pkg
-- Create Date: 09/19/2019 (MM/DD/YYYY)
--
-- Company: ittChip - Unisinos
-- Engineer: Vinícius Gabriel LINDEN
-- 
-- Description: Constants for full_comparison_operation module and things that
--              connects to it.
-- 
--------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
--use IEEE.NUMERIC_STD.ALL;


package operations_pkg is

	constant MAKE_LOW                       : std_logic_vector := x"00"; -- force low
	constant MAKE_HIGH                      : std_logic_vector := x"01"; -- force high
	constant ASSERT_EQUAL                   : std_logic_vector := x"02"; -- data = value1
	constant ASSERT_GREATER                 : std_logic_vector := x"03"; -- data > value1
	constant ASSERT_GREATER_EQUAL           : std_logic_vector := x"04"; -- data >= value1
	constant ASSERT_LESS                    : std_logic_vector := x"05"; -- data < value1
	constant ASSERT_LESS_EQUAL              : std_logic_vector := x"06"; -- data <= value1
	constant ASSERT_GREATER_LESS            : std_logic_vector := x"07"; -- value1 < data < value2
	constant ASSERT_GREATER_EQUAL_LESS      : std_logic_vector := x"08"; -- value1 <= data < value2
	constant ASSERT_GREATER_LESS_EQUAL      : std_logic_vector := x"09"; -- value1 < data <= value2
	constant ASSERT_GREATER_EQUAL_LESS_EQ   : std_logic_vector := x"0A"; -- value1 <= data <= value2

end operations_pkg;


package body operations_pkg is

	end operations_pkg;
