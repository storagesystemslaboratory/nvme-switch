--------------------------------------------------------------------------------
--
-- Module Name: NVMe_pkg
-- Create Date: 05/31/2018 (MM/DD/YYYY)
--
-- Company: ittChip - Unisinos
-- Engineer: Vinícius Gabriel LINDEN
--
-- Description: Common information for NVMe processing
--
--------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

package NVMe_pkg is

	-- NVMe commands
	constant NVME_FLUSH					: std_logic_vector := x"00";
	constant NVME_WRITE					: std_logic_vector := x"01";
	constant NVME_READ					: std_logic_vector := x"02";
	constant NVME_WRITE_UNCORRECTABLE	: std_logic_vector := x"04";
	constant NVME_COMPARE				: std_logic_vector := x"05";
	constant NVME_WRITE_ZEROS			: std_logic_vector := x"08";
	constant NVME_DATASET_MANAGEMENT	: std_logic_vector := x"09";
	constant NVME_RESERVATION_REGISTER	: std_logic_vector := x"0D";
	constant NVME_RESERVATION_REPORT	: std_logic_vector := x"0E";
	constant NVME_RESERVATION_ACQUIRE	: std_logic_vector := x"11";
	constant NVME_RESERVATION_RELEASE	: std_logic_vector := x"15";
	constant NVME_RANDOM				: std_logic_vector := x"94"; -- this may have to change in the future

	-- General purpose
	constant TO_SSD						: std_logic := '1';
	constant TO_GENERIC					: std_logic := not TO_SSD;

	-- Field capture configurations
	constant FC_IDLE					: std_logic_vector := x"00";
	constant FC_SLBA					: std_logic_vector := x"01";
	constant FC_PRP1					: std_logic_vector := x"02";
	constant FC_PRP2					: std_logic_vector := x"03";
	constant FC_FUA						: std_logic_vector := x"04";
	constant FC_DSM						: std_logic_vector := x"05";
	constant FC_TEST					: std_logic_vector := x"FE";

	-- Opcode counter configuration
	constant OC_IDLE					: std_logic_vector := x"00";
	constant OC_WRITE					: std_logic_vector := x"01";
	constant OC_READ					: std_logic_vector := x"02";
	constant OC_PROPRIETARY				: std_logic_vector := x"03"; -- proprietary command
	constant OC_RD_RANGE				: std_logic_vector := x"04"; -- within a range in read
	constant OC_RW_RANGE				: std_logic_vector := x"05"; -- within a range in both read & write

	-- Arbitrator configuration
	constant AC_IDLE					: std_logic_vector := x"00";
	constant AC_SEND_GENERIC			: std_logic_vector := x"01"; -- force send to generic SSD
	constant AC_SEND_SSD				: std_logic_vector := x"02"; -- force send to SSD
	constant AC_TEST					: std_logic_vector := x"FE"; -- arbitrary test configuration
	-- Arbitrator configuration ranges
	constant AC_TEST_MIN				: std_logic_vector := x"07060504030200FE"; -- AC_TEST
	constant AC_TEST_MAX				: std_logic_vector := x"0706050403020102"; -- AC_TEST

	-- FUNCTION: nvme_cmd_size
	-- Returns the number of bytes for a submission command (true) or
	-- completion command (false)
	function nvme_cmd_size(SQ : boolean) return integer;

	-- FUNCTION ssd_zeroth_place
	-- Returns: true if TO_SSD = '0'; false if TO_SSD = '1'
	function ssd_zeroth_place return boolean;

end NVMe_pkg;

package body NVMe_pkg is

	-- FUNCTION: nvme_cmd_size
	function nvme_cmd_size(SQ : boolean) return integer is
	begin
		if SQ then
			return 64;
		else
			return 16;
		end if;
	end function nvme_cmd_size;

	-- FUNCTION ssd_zeroth_place
	function ssd_zeroth_place return boolean is
	begin
		if TO_SSD = '0' then
			return true;
		else
			return false;
		end if;
	end function ssd_zeroth_place;

end NVMe_pkg;
