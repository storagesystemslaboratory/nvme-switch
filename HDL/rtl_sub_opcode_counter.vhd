--------------------------------------------------------------------------------
--
-- Module Name: rtl_sub_opcode_counter
-- Create Date: 09/18/2019 (MM/DD/YYYY)
-- Revision: 0
--
-- Company: ittChip - Unisinos
-- Engineer: Vinícius Gabriel LINDEN
-- 
-- Description: Real-time logic opcode counter.
-- 
--------------------------------------------------------------------------------
-- TODO:

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

library UNISIM;
use UNISIM.VComponents.all;

library work;
use work.NVMe_pkg.all;

entity rtl_sub_opcode_counter is
	generic
	(
		CLOCK_EDGE			: std_logic := '1' -- '1' for rising, '0' for falling
	);
	port
	(
		-- CONFIGURATION REGISTER
		config				: in std_logic_vector(7 downto 0); -- config for opcode counter
		config_updated		: in std_logic; -- '1' if new configuration has been sent

		-- FRAMEWORK
		stream_delayed		: in std_logic_vector(7 downto 0);
		count				: in std_logic_vector(5 downto 0);
		opcode				: in std_logic_vector(7 downto 0);
		last_delayed		: in std_logic;
		evaluate			: in std_logic; -- evaluate result

		-- EXTERNAL INTERFACE
		result				: out std_logic_vector(7 downto 0);

		-- OTHER
		error				: out std_logic;
		rst					: in std_logic;
		clk					: in std_logic
	);
end rtl_sub_opcode_counter;

architecture rtl of rtl_sub_opcode_counter is


	signal op_match			: std_logic; -- opcode mask matches the current opcode
	signal rg_match			: std_logic; -- range matches
	signal use_op			: std_logic; -- use op_match instead of rg_match

	signal cnt_rst			: std_logic; -- reset count
	signal cnt_en			: std_logic;
	signal saturated		: std_logic;
	signal error_saturated	: std_logic; -- tried to count when count is saturated (maximum value)
	signal error_rg			: std_logic; -- tried to count when count is saturated (maximum value)

begin

	cnt_rst <= rst or config_updated;
	error <= error_saturated or error_rg;

	-- YOU MAY CHANGE BELOW THIS LINE

	OPCODE_MASK : entity work.rtl_opcode_mask -- you may change here too
	port map
	(
		config => config,
		opcode => opcode,
		op_match => op_match,
		use_op => use_op
	);

	RANGE_TESTER : entity work.rtl_opcode_cnt_range_tester
	generic map
	(
		CLOCK_EDGE => CLOCK_EDGE
	)
	port map
	(
		config => config,
		config_updated => config_updated,
		stream_delayed => stream_delayed,
		count => count,
		opcode => opcode,
		last_delayed => last_delayed,
		rg_match => rg_match,
		error => error_rg,
		rst => rst,
		clk => clk
	);

	-- DO NOT CHANGE BELOW THIS LINE

	process(rst,clk)
	begin
		if rst = '1' then
			error_saturated <= '0';
		elsif clk'event and clk = '1' then
			if evaluate = '1' then -- evaluate

				if use_op = '1' then
					cnt_en <= op_match;
				else
					cnt_en <= rg_match;
				end if;

				if saturated = '1' then
					error_saturated <= '1';
				end if;

			else
				cnt_en <= '0';
			end if;
		end if;
	end process;

	RESULT_COUNTER : entity work.rtl_counter
	generic map
	(
		CNT_RANGE => 255, -- 8 bits
		CNT_RESET => 0,
		CNT_SATURATE_EN => true
	)
	port map
	(
		rst => cnt_rst,
		clk => clk,
		clk_en => '1',
		cnt_en => cnt_en,
		cnt => result,
		sat => saturated
	);

end rtl;


