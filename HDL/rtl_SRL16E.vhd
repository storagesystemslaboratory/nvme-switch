--------------------------------------------------------------------------------
--
-- Module Name: rtl_SRL16E.vhd
-- Create Date: XX/XX/XXXX (MM/DD/YYYY)
-- Revision: X
--
-- Company: ittChip-Unisinos
-- Engineer: Rodrigo Marques de Figueiredo
--
-- Description: SRL16E shift-register, based on XST User Guide.
--				References: WP271 and XAPP465.
--
--------------------------------------------------------------------------------
-- TODO:

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity rtl_SRL16E is
	generic
	(
		MODE		: string := "STATIC"; -- mode: "STATIC" or "DYNAMIC"
		ADDR_SEL	: integer := 3; -- "STATIC" read address
		ADDR_WIDTH	: integer := 3; -- address width
		CLOCK_EDGE	: std_logic := '1' -- '1' for rising, '0' for falling
	);
	port (
			d		: in std_logic; -- input
			q		: out std_logic; -- output
			a		: in std_logic_vector(ADDR_WIDTH-1 downto 0); -- address
			clk_en	: in std_logic; -- clock enable
			clk		: in std_logic
		);
end rtl_SRL16E;

architecture rtl of rtl_SRL16E is

	signal data		: std_logic_vector((2**ADDR_WIDTH)-1 downto 0);

	attribute shreg_extract : string;
	attribute shreg_extract of data: signal is "yes";

begin

	q <= data(ADDR_SEL) when MODE = "STATIC" else data(conv_integer(a));

	process(clk)
	begin
		if clk'event and clk = CLOCK_EDGE then
			if clk_en = '1' then
				for n in 0 to (2**ADDR_WIDTH)-2 loop
					data(n+1) <= data(n);
				end loop;
				data(0) <= d;
			end if;
		end if;
	end process;

end rtl;
