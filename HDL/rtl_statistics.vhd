--------------------------------------------------------------------------------
--
-- Module Name: rtl_statistics
-- Create Date: 21/01/2020 (MM/DD/YYYY)
-- Revision: 0
--
-- Company: ittChip - Unisinos
-- Engineer: Vinícius Gabriel LINDEN
-- 
-- Description: Real-time logic for statistics module
-- 
--------------------------------------------------------------------------------
-- TODO:
--	SUBMISSION_CMD = true

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
--use IEEE.NUMERIC_STD.ALL;

library UNISIM;
use UNISIM.VComponents.all;

library work;
use work.common_pkg.all;

entity rtl_statistics is
	generic
	(
		FC_NUMBER			: integer := 1; -- number of field captures to capture
		OC_NUMBER			: integer := 1; -- number of opcode_cnt to count
		CLEAR_ON_BOTH		: boolean := false; -- true: clear is activated on both edge; false: only the CLOCK_EDGE is active
		SUBMISSION_CMD		: boolean := true; -- true: submission command; false: completion command
		CLOCK_EDGE			: std_logic := '1' -- '1' for rising, '0' for falling
	);
	port
	(

		-- CONFIGURATION REGISTER
		config_fc			: in std_logic_vector_8(FC_NUMBER-1 downto 0); -- field capture
		config_updated_fc	: in std_logic_vector(FC_NUMBER-1 downto 0); -- field capture
		config_oc			: in std_logic_vector_8(OC_NUMBER-1 downto 0); -- opcode counter
		config_updated_oc	: in std_logic_vector(OC_NUMBER-1 downto 0); -- opcode counter

		-- FRAMEWORK
		stream				: in std_logic_vector(7 downto 0); -- byte data size
		valid				: in std_logic; -- sending new data stream
		ready				: out std_logic; -- do not send new stream, module is busy
		opcode				: out std_logic_vector(7 downto 0); -- NVMe command
		count				: out std_logic_vector(5 downto 0); -- byte count
		cmd_id				: out std_logic_vector(15 downto 0); -- command identifier
		last				: out std_logic; -- last byte of command

		-- STATISTICS
		new_data			: out std_logic_vector(FC_NUMBER-1 downto 0); -- new data to store
		clear				: in std_logic_vector(FC_NUMBER-1 downto 0); -- send new data (clear the registers)
		field				: out std_logic_vector_64(FC_NUMBER-1 downto 0); -- captured data output for storage
		opcode_cnt				: out std_logic_vector_8(OC_NUMBER-1 downto 0); -- number of counted opcode_cnt

		-- OTHER
		error				: out std_logic;
		rst					: in std_logic;
		clk					: in std_logic
	);
end rtl_statistics;

architecture rtl of rtl_statistics is

	signal stream_delayed	: std_logic_vector(7 downto 0);
	signal count_i			: std_logic_vector(5 downto 0);
	signal opcode_i			: std_logic_vector(7 downto 0);
	signal last_delayed		: std_logic;
	signal evaluate			: std_logic;
	signal error_fw			: std_logic;
	signal error_fc			: std_logic;
	signal error_oc			: std_logic;

begin

	error <= error_fw or error_fc or error_oc;
	count <= count_i;
	opcode <= opcode_i;

	FRAMEWORK : entity work.rtl_statistics_framework 
	generic map
	(
		SUBMISSION_CMD => SUBMISSION_CMD,
		CLOCK_EDGE => CLOCK_EDGE
	)
	port map
	(
		stream => stream,
		valid => valid,
		ready => ready,
		stream_delayed => stream_delayed,
		opcode => opcode_i,
		count => count_i,
		cmd_id => cmd_id,
		last => last,
		last_delayed => last_delayed,
		evaluate => evaluate,
		error => error_fw,
		rst => rst,
		clk => clk
	);

	HAS_FC : if FC_NUMBER > 0 generate
		FIELD_CAPTURE : entity work.rtl_field_capture
		generic map
		(
			CONFIG_NUMBER => FC_NUMBER,
			CLEAR_ON_BOTH => CLEAR_ON_BOTH,
			CLOCK_EDGE => CLOCK_EDGE
		)
		port map
		(
			config => config_fc,
			config_updated => config_updated_fc,
			stream_delayed => stream_delayed,
			opcode => opcode_i,
			count => count_i,
			last_delayed => last_delayed,
			new_data => new_data,
			clear => clear,
			field => field,
			error => error_fc,
			rst => rst,
			clk => clk
		);
	end generate HAS_FC;

	HAS_OC : if OC_NUMBER > 0 generate
		OPCODE_COUNTER : entity work.rtl_opcode_counter
		generic map
		(
			COUNTER_NUMBER => OC_NUMBER
		)
		port map
		(
			config => config_oc,
			config_updated => config_updated_oc,
			stream_delayed => stream_delayed,
			count => count_i,
			opcode => opcode_i,
			last_delayed => last_delayed,
			evaluate => evaluate,
			result => opcode_cnt,
			error => error_oc,
			rst => rst,
			clk => clk
		);
	end generate HAS_OC;

	NO_FC : if FC_NUMBER = 0 generate
		error_fc <= '0';
	end generate NO_FC;

	NO_OC : if OC_NUMBER = 0 generate
		error_oc <= '0';
	end generate NO_OC;

end rtl;


