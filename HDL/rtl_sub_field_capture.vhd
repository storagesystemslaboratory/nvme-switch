--------------------------------------------------------------------------------
--
-- Module Name: rtl_sub_field_capture
-- Create Date: 08/30/2019 (MM/DD/YYYY)
-- Revision: 2
--
-- Company: ittChip - Unisinos
-- Engineer: Vinícius Gabriel LINDEN
-- 
-- Description: Real-time logic for NVMe field capturing
-- 
--------------------------------------------------------------------------------
-- TODO:
--	Implement USE_CONFIG = 3

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
--use IEEE.NUMERIC_STD.ALL;

library UNISIM;
use UNISIM.VComponents.all;

library work;
--use work.common_pkg.all;
use work.NVMe_pkg.all;


entity rtl_sub_field_capture is
	generic (
		USE_CONFIG			: natural := 1; -- 1 = FC; 2 = OC; 3 = AC(1); 4 = AC(2), ...
		EN_CLEAR			: boolean := true; -- enables the new_data & clear mechanism
		CLEAR_ON_BOTH		: boolean := true; -- true, clear is activated on both edge, if false only the CLOCK_EDGE is active
		CLOCK_EDGE			: std_logic := '1' -- '1' for rising, '0' for falling
	);
	port (
		-- CONFIGURATION REGISTER
		config				: in std_logic_vector(7 downto 0); -- configuration for field capture
		config_updated		: in std_logic; -- '1' if new configuration has been sent

		-- FRAMEWORK
		stream_delayed		: in std_logic_vector(7 downto 0);
		count				: in std_logic_vector(5 downto 0); -- byte count
		opcode				: in std_logic_vector(7 downto 0); -- NVMe command
		last_delayed		: in std_logic;

		-- TEMPORARY STORAGE
		opcode_match 		: out std_logic; -- opcode matches the configuration
		new_data			: out std_logic; -- new data to store
		clear				: in std_logic; -- send new data (clear the registers)
		field				: out std_logic_vector(63 downto 0); -- captured data output for storage
		-- GENERAL PURPOSE
		rst					: in std_logic; -- async reset
		error				: out std_logic; -- error
		clk					: in std_logic -- clock
	);
end rtl_sub_field_capture;


architecture rtl of rtl_sub_field_capture is

	signal trigger_capture	: std_logic;
	signal field_raw		: std_logic_vector(63 downto 0);
	signal field_mask		: std_logic_vector(63 downto 0);
	signal error_storage	: std_logic;

begin

	field <= field_raw and field_mask;
	error <= error_storage;

	TEMPORATY_STORAGE : entity work.rtl_temporary_storage
	generic map
	(
		EN_CLEAR => EN_CLEAR,
		CLEAR_ON_BOTH => CLEAR_ON_BOTH,
		CLOCK_EDGE => CLOCK_EDGE
	)
	port map
	(
		config_updated => config_updated,
		stream_delayed => stream_delayed,
		trigger_capture => trigger_capture,
		count => count,
		last_delayed => last_delayed,
		clear => clear,
		field_raw => field_raw,
		new_data => new_data,
		rst => rst,
		error => error_storage,
		clk => clk
	);

	ACTIVATE_CAPTURE : entity work.rtl_activate_capture
	generic map
	(
		USE_CONFIG => USE_CONFIG
	)
	port map
	(
		config => config,
		opcode => opcode,
		count => count,
		opcode_match => opcode_match,
		trigger_capture => trigger_capture,
		field_mask => field_mask
	);

end rtl;


