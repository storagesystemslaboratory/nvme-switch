--------------------------------------------------------------------------------
--
-- Module Name: tb_field_capture
-- Create Date: 05/22/2019 (MM/DD/YYYY)
--
-- Company: ittChip - Unisinos
-- Engineer: Vinícius Gabriel LINDEN
-- 
-- Description: Testbench for the field capture module
-- 
--------------------------------------------------------------------------------
-- Recommended span of 300 ns for period of 10 ns

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library UNISIM;
use UNISIM.VComponents.all;

library STD;
use STD.TEXTIO.ALL;

library work;
use work.common_pkg.all;
use work.kratus_pkg.all;
use work.NVMe_pkg.all;
use work.std_cmds_pkg.all;

entity tb_field_capture is
end tb_field_capture;


architecture Behavioral of tb_field_capture is

	-- DO NOT DELETE
	constant period                 : time := 5 ns;
	constant hp                     : time := period/2; -- half period

	signal index                    : natural := 1; -- index for redability
	signal clk                      : std_logic := '1'; -- system base clock

	procedure wait_clk(constant T : in natural) is
	begin
		wait for t*period;
	end procedure;
	-- DELETION RESTRICTION ENDED

	-- TESTBENCH FILE
	constant WIDTH_INPUT		: integer := 8; -- number of input characters in a line
	file file_input				: text;
	signal read_input			: std_logic_vector(WIDTH_INPUT-1 downto 0);
	signal enable_input			: std_logic := '0'; -- control signal to enable input read
	-- END OF TESTBENCH FILE

	-- INPUT FILE RELATED
	constant F_READ1			: string(1 to 16) := "read1_______.txt";
	constant F_READ2			: string(1 to 16) := "read2_______.txt";
	constant F_FLUSH			: string(1 to 16) := "flush_______.txt";
	constant F_WRITE			: string(1 to 16) := "write_______.txt";
	constant F_PROP1			: string(1 to 16) := "NVMe_SQ_B_01.txt";
	constant F_PROP2			: string(1 to 16) := "NVMe_SQ_B_02.txt";
	signal file_title			: string(1 to 16) := F_READ1; -- file currently being process
	-- END OF INPUT FILE RELATED

	-- UUT
	constant EN_CLEAR		: boolean := true;
	constant CONFIG_NUMBER	: natural := 2;
	constant CLEAR_ON_BOTH	: boolean := true;
	constant CLOCK_EDGE		: std_logic := '1';
	signal config			: std_logic_vector_8(CONFIG_NUMBER-1 downto 0) := (others => (others => '0'));
	signal config_updated	: std_logic_vector(CONFIG_NUMBER-1 downto 0) := (others => '0');
	signal opcode			: std_logic_vector(7 downto 0);
	signal stream_delayed	: std_logic_vector(7 downto 0);
	signal count			: std_logic_vector(5 downto 0);
	signal last_delayed		: std_logic;
	signal new_data			: std_logic_vector(CONFIG_NUMBER-1 downto 0);
	signal clear			: std_logic_vector(CONFIG_NUMBER-1 downto 0) := (others => '0');
	signal field			: std_logic_vector_64(CONFIG_NUMBER-1 downto 0);
	signal rst				: std_logic := '1';
	signal error			: std_logic;
	-- END OF UUT

	-- FRAMEWORK
	signal stream			: std_logic_vector(7 downto 0);
	signal valid			: std_logic := '0';
	signal ready			: std_logic;
	signal error_fw			: std_logic;
	-- END OF FRAMEWORK

	-- SIMULATION
	signal field_i			: integer;
	signal test_case		: string(1 to 20) := "waiting             ";
	signal read_file		: std_logic := '0'; -- on/off control for input file and handshake control
	signal config_edge		: std_logic_vector_8(CONFIG_NUMBER-1 downto 0) := (others => (others => '0')); -- edge detection
	-- END OF SIMULATION

begin

	UUT : entity work.rtl_field_capture
	generic map
	(
		EN_CLEAR => EN_CLEAR,
		CONFIG_NUMBER => CONFIG_NUMBER,
		CLEAR_ON_BOTH => CLEAR_ON_BOTH,
		CLOCK_EDGE => CLOCK_EDGE
	)
	port map
	(
		config => config,
		config_updated => config_updated,
		stream_delayed => stream_delayed,
		opcode => opcode,
		count => count,
		last_delayed => last_delayed,
		new_data => new_data,
		clear => clear,
		field => field,
		rst => rst,
		error => error,
		clk => clk
	);

	FRAMEWORK : entity work.rtl_statistics_framework 
	generic map
	(
		SUBMISSION_CMD => true,
		CLOCK_EDGE => CLOCK_EDGE
	)
	port map
	(
		stream => stream,
		valid => valid,
		ready => ready,
		stream_delayed => stream_delayed,
		opcode => opcode,
		count => count,
		cmd_id => open,
		last => open,
		last_delayed => last_delayed,
		evaluate => open,
		error => error_fw,
		rst => rst,
		clk => clk
	);

	-- DO NOT DELETE
	clk <= not clk after hp;
	index <= index + 1 after period;
	-- DELETION RESTRICTION ENDED

	-- TESTBENCH FILE
	READ_PROCESS : process -- read
		variable line_input               : line;
		variable bit_input                : bit_vector(WIDTH_INPUT-1 downto 0);
	begin
		wait until enable_input'event and enable_input = '1';
		file_open(file_input,file_title,read_mode);
		while not endfile(file_input) loop
			if enable_input = '1' then
				readline(file_input,line_input);
				read(line_input,bit_input);
				read_input <= to_stdlogicvector(bit_input);
			end if;
			wait for period;
		end loop;
		file_close(file_input);
	end process READ_PROCESS;
	-- END OF TESTBENCH FILE

	-- INPUT DATA CONTROL
	stream <= read_input;
	-- END OF INPUT DATA CONTROL

	-- READ FILE AUTOMATIC CONTROL
	process
	begin
		enable_input <= '0';
		wait until read_file'event;
		enable_input <= '1';
		wait_clk(64);
	end process;
	-- END OF READ FILE AUTOMATIC CONTROL

	-- RECEIVING CYCLE CONTROL
	process
	begin
		valid <= '0';
		wait until read_file'event;
		valid <= '1';
		wait_clk(1);
	end process;
	-- END OF RECEIVING CYCLE CONTROL

	-- CONFIG_UPDATED AUTOMATIC CONTROL
	process
	begin
		wait for period;
		config_edge <= config;
	end process;
	CONFIG_EACH_FC : for i in config'range generate
		config_updated(i) <= '1' when config_edge(i) /= config(i) else '0';
	end generate CONFIG_EACH_FC;
	-- END OF CONFIG_UPDATED AUTOMATIC CONTROL

	process
	begin
		report "simulation started";
		wait_clk(1);
		rst <= '0';
		wait_clk(1);

		test_case <= "clearing            ";
		wait_clk(10);

		test_case <= "standard test 1     ";
		wait for 1 ns;
		report test_case;
		file_title <= F_READ1;
		config(0) <= FC_SLBA;
		config(1) <= FC_DSM;
		wait_clk(5);
		toggle(read_file);
		wait until ready = '1';
		wait_clk(1);
		toggle(clear);
		wait_clk(5);
		assert field(0) = STD_CMD_READ1_SLBA report "field(0)" severity warning;
		assert field(1) = STD_CMD_READ1_DSM report "field(1)" severity warning;
		assert error = '0' report "error" severity warning;

		test_case <= "clearing            ";
		wait_clk(10);

		test_case <= "standard test 2     ";
		wait for 1 ns;
		report test_case;
		file_title <= F_WRITE;
		config(0) <= FC_PRP1;
		config(1) <= FC_SLBA;
		wait_clk(5);
		toggle(read_file);
		wait until ready = '1';
		wait_clk(1);
		assert field(0) = NULL_FIELD report "field(0)" severity warning;
		assert field(1) = NULL_FIELD report "field(1)" severity warning;
		assert error = '0' report "error" severity warning;

		test_case <= "clearing            ";
		wait_clk(10);

		test_case <= "simulation ended    ";
		report "simulation ended";
		wait;
	end process;

end Behavioral;


