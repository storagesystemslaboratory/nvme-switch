--------------------------------------------------------------------------------
--
-- Module Name: rtl_arbitrator_comp_config
-- Create Date: 09/19/2019 (MM/DD/YYYY)
-- Revision: 1
--
-- Company: ittChip - Unisinos
-- Engineer: Vinícius Gabriel LINDEN
-- 
-- Description: Real-time logic for translating between arbitrator configuration
--              and full comparison operation configuration. It also deduces the
--              correct values for value1 and value2 according to the
--              configuration & configuring the field capture module.
--
-- Observation: Comparison has a VALUE_HIGH generic, for when comparison is true,
--              output VALUE_HIGH. This module also has this generic, so it keeps
--              true to the configuration when anything changes.
-- 
--------------------------------------------------------------------------------
-- TODO:
--  Check if the "include" ranges (=>, <=) are correct [lower boundary seems ok, upper does not]

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
--use IEEE.NUMERIC_STD.ALL;

library UNISIM;
use UNISIM.VComponents.all;

library work;
use work.operations_pkg.all;
use work.NVMe_pkg.all;

entity rtl_arbitrator_comp_config is
	port
	(
		-- CONFIGURATIONS
		config_ac 			: in std_logic_vector(7 downto 0); -- input AC_CONFIG
		config_comparison 	: out std_logic_vector(7 downto 0); -- output comp config

		-- VALUES
		value1 				: out std_logic_vector(63 downto 0);
		value2 				: out std_logic_vector(63 downto 0)
	);
end rtl_arbitrator_comp_config;

architecture rtl of rtl_arbitrator_comp_config is

	function translate_ssd return std_logic_vector is
	begin
		if TO_SSD = '1' then
			return MAKE_HIGH;
		else
			return MAKE_LOW;
		end if;
	end function translate_ssd;

	function translate_generic return std_logic_vector is
	begin
		if TO_SSD = '1' then
			return MAKE_LOW; -- SSD: MAKE_HIGH
		else
			return MAKE_HIGH; -- SSD: MAKE_LOW
		end if;
	end function translate_generic;

	constant FIX_SSD 		: std_logic_vector := translate_ssd;
	constant FIX_GENERIC 	: std_logic_vector := translate_generic;

begin

	with config_ac select value1 <=
		AC_TEST_MIN when AC_TEST,
		(others => '0') when others;

	with config_ac select value2 <=
		AC_TEST_MAX when AC_TEST,
		(others => '0') when others;

	with config_ac select config_comparison <=
		FIX_SSD 						when AC_IDLE,
		FIX_GENERIC 					when AC_SEND_GENERIC,
		FIX_SSD 						when AC_SEND_SSD,
		ASSERT_GREATER_EQUAL_LESS_EQ 	when AC_TEST,
		FIX_SSD 						when others;

end rtl;


