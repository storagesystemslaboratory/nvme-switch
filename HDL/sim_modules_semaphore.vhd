--------------------------------------------------------------------------------
--
-- Module Name: sim_modules_semaphore
-- Create Date: 09/21/2019 (MM/DD/YYYY)
--
-- Company: ittChip - Unisinos
-- Engineer: Vinícius Gabriel LINDEN
-- 
-- Description: Stimulae for the modules semaphone module
-- 
--------------------------------------------------------------------------------
-- Recommended span of 300 ns for period of 10 ns
-- TODO:


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
--use IEEE.NUMERIC_STD.ALL;

library UNISIM;
use UNISIM.VComponents.all;

--library STD;
--use STD.TEXTIO.ALL;

--library work;
--use work.kratus_pkg.all;

entity sim_modules_semaphore is
	end sim_modules_semaphore;


architecture Behavioral of sim_modules_semaphore is

	-- DO NOT DELETE
	constant period                 : time := 5 ns;
	constant hp                     : time := period/2; -- half period

	signal index                    : natural := 1; -- index for redability
	signal clk                      : std_logic := '1'; -- system base clock

	procedure wait_clk(constant T : in natural) is
	begin
		wait for t*period;
	end procedure;
	-- DELETION RESTRICTION ENDED

	-- UUT
	signal prioritary_ask           : std_logic := '0';
	signal prioritary_lock          : std_logic;
	signal secondary_ask            : std_logic := '0';
	signal secondary_lock           : std_logic;
	signal ctrl_sel                 : std_logic;
	signal rst                      : std_logic := '1';
	-- END OF UUT

	-- SIMULATION
	constant D                      : integer := 5;
	signal test_case                : string(1 to 20) := "waiting             ";
-- END OF SIMULATION

begin

	UUT : entity work.rtl_modules_semaphore
	port map (
				 prioritary_ask => prioritary_ask,
				 prioritary_lock => prioritary_lock,
				 secondary_ask => secondary_ask,
				 secondary_lock => secondary_lock,
				 ctrl_sel => ctrl_sel,
				 rst => rst,
				 clk => clk
			 );

	-- DO NOT DELETE
	clk <= not clk after hp;
	index <= index + 1 after period;
	-- DELETION RESTRICTION ENDED

	process
	begin
		wait_clk(2);
		rst <= '0';

		test_case <= "clear               ";
		wait_clk(10);

		test_case <= "prioritary alone    ";
		prioritary_ask <= '1';
		wait_clk(D);
		prioritary_ask <= '0';
		wait_clk(D);

		test_case <= "secondary alone     ";
		secondary_ask <= '1';
		wait_clk(D);
		secondary_ask <= '0';
		wait_clk(D);

		test_case <= "prioritary than seco";
		prioritary_ask <= '1';
		wait_clk(D);
		secondary_ask <= '1';
		wait_clk(D);
		prioritary_ask <= '0';
		wait_clk(D);
		secondary_ask <= '0';
		wait_clk(D);

		test_case <= "secondary than prior";
		secondary_ask <= '1';
		wait_clk(D);
		prioritary_ask <= '1';
		wait_clk(D);
		secondary_ask <= '0';
		wait_clk(D);
		prioritary_ask <= '0';
		wait_clk(D);

		test_case <= "prioritary & seconda";
		prioritary_ask <= '1';
		secondary_ask <= '1';
		wait_clk(D);
		prioritary_ask <= '0';
		secondary_ask <= '0';
		wait_clk(D);

		test_case <= "clear               ";
		wait_clk(10);

		test_case <= "simulation ended    ";
		wait;
	end process;

end Behavioral;


