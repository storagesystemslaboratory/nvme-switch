--------------------------------------------------------------------------------
--
-- Module Name: sim_mux_NVMe
-- Create Date: 10/03/2019 (MM/DD/YYYY)
--
-- Company: ittChip - Unisinos
-- Engineer: Vinícius Gabriel LINDEN
-- 
-- Description: Stimulae mux_NVMe
-- 
--------------------------------------------------------------------------------
-- Recommended span of 300 ns for period of 10 ns
-- TODO:


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library UNISIM;
use UNISIM.VComponents.all;

--library work;
--use work.common_pkg.all;
--use work.kratus_pkg.all;

entity sim_mux_NVMe is
	end sim_mux_NVMe;


architecture Behavioral of sim_mux_NVMe is

	-- DO NOT DELETE
	constant period                 : time := 5 ns;
	constant hp                     : time := period/2; -- half period

	signal index                    : natural := 1; -- index for redability
	signal clk                      : std_logic := '1'; -- system base clock

	procedure wait_clk(constant T : in natural) is
	begin
		wait for t*period;
	end procedure;
	-- DELETION RESTRICTION ENDED

	-- UUT
	signal stream_source1  : std_logic_vector(7 downto 0) := (others => '0');
	signal valid_source1        : std_logic := '0';
	signal ready_source1        : std_logic;
	signal stream_source2  : std_logic_vector(7 downto 0) := (others => '0');
	signal valid_source2        : std_logic := '0';
	signal ready_source2        : std_logic;
	signal stream_destin   : std_logic_vector(7 downto 0);
	signal valid_destin         : std_logic;
	signal ready_destin         : std_logic := '0';
	signal sel                  : std_logic := '0';
-- END OF UUT

-- SIMULATION
-- END OF SIMULATION

begin

	UUT : entity work.rtl_mux_NVMe
	port map (
				 stream_source1 => stream_source1,
				 valid_source1 => valid_source1,
				 ready_source1 => ready_source1,
				 stream_source2 => stream_source2,
				 valid_source2 => valid_source2,
				 ready_source2 => ready_source2,
				 stream_destin => stream_destin,
				 valid_destin => valid_destin,
				 ready_destin => ready_destin,
				 sel => sel
			 );

	-- DO NOT DELETE
	clk <= not clk after hp;
	index <= index + 1 after period;
	-- DELETION RESTRICTION ENDED

	process
	begin
		wait for 1*period;
		sel <= not sel;
	end process;

	process
	begin
		wait for 2*period;
		valid_source1 <= not valid_source1;
	end process;

	valid_source2 <= not valid_source1;

	process
	begin
		wait for 3*period;
		ready_destin <= not ready_destin;
	end process;

	process
	begin
		for i in 0 to 255 loop
			stream_source1 <= std_logic_vector(to_unsigned(i,8));
			stream_source2 <= std_logic_vector(to_unsigned(i+128,8));
			wait for 1*period;
		end loop;
	end process;

end Behavioral;


