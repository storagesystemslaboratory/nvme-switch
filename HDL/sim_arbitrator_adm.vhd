--------------------------------------------------------------------------------
--
-- Module Name: sim_arbitrator_adm
-- Create Date: 09/06/2019 (MM/DD/YYYY)
--
-- Company: ittChip - Unisinos
-- Engineer: Vinícius Gabriel LINDEN
-- 
-- Description: Stimulae arbitrator adm submodule
-- 
--------------------------------------------------------------------------------
-- Recommended span of 300 ns for period of 10 ns
-- TODO:


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library UNISIM;
use UNISIM.VComponents.all;

--library STD;
--use STD.TEXTIO.ALL;

library work;

entity sim_arbitrator_adm is
end sim_arbitrator_adm;


architecture Behavioral of sim_arbitrator_adm is

	-- DO NOT DELETE
	constant period					: time := 5 ns;
	constant hp						: time := period/2; -- half period

	signal index					: natural := 1; -- index for redability
	signal clk						: std_logic := '1'; -- system base clock

	procedure wait_clk(constant T : in natural) is
	begin
		wait for t*period;
	end procedure;
	-- DELETION RESTRICTION ENDED

	-- UUT
	constant CLOCK_EDGE 			: std_logic := '1';
	signal direction 				: std_logic := '0';
	signal new_available 			: std_logic := '0';
	signal next_direction 			: std_logic;
	signal adm_ask 					: std_logic;
	signal adm_lock 				: std_logic := '0';
	signal adm_flow 				: std_logic;
	signal clear 					: std_logic;
	signal full 					: std_logic := '0';
	signal empty 					: std_logic := '0';
	signal unload 					: std_logic;
	signal unloaded 				: std_logic := '0';
	signal processing 				: std_logic := '0';
	signal number_pkgs 				: std_logic_vector(7 downto 0) := (others => '0');
	signal submit 					: std_logic;
	signal response 				: std_logic := '0';
	signal error 					: std_logic;
	signal rst 						: std_logic := '1';
	-- END OF UUT

	-- SIMULATION
	signal test_case				: string(1 to 20) := "waiting             ";
-- END OF SIMULATION

begin

	UUT : entity work.rtl_arbitrator_adm
	generic
	generic map
	(
		CLOCK_EDGE => CLOCK_EDGE
	)
	port map
	(
		direction => direction,
		new_available => new_available,
		next_direction => next_direction,
		adm_ask => adm_ask,
		adm_lock => adm_lock,
		adm_flow => adm_flow,
		full => full,
		empty => empty,
		unload => unload,
		unloaded => unloaded,
		processing => processing,
		number_pkgs => number_pkgs,
		submit => submit,
		response => response,
		error => error,
		rst => rst,
		clk => clk
	);

	-- DO NOT DELETE
	clk <= not clk after hp;
	index <= index + 1 after period;
	-- DELETION RESTRICTION ENDED

	process
	begin
		wait for 5*period;
		direction <= not direction;
	end process;

	process
	begin
		wait_clk(2);
		rst <= '0';
		wait_clk(10);

		test_case <= "clear               ";
		wait_clk(10);

		test_case <= "through wait state  ";
		new_available <= '1';
		adm_lock <= '0';
		response <= '0';
		processing <= '0';
		unloaded <= '0';
		wait_clk(1); -- just one cmd has been processed
		new_available <= '0';
		wait_clk(5); -- adm waits for lock
		adm_lock <= '1';
		wait_clk(5); -- adm wait for filter response
		response <= '1';
		wait_clk(1);
		response <= '0';
		wait_clk(5); -- wait for FIFO NVMe to process request
		processing <= '1';
		wait_clk(1);
		processing <= '0';
		unloaded <= '1';
		wait_clk(5);

		test_case <= "clear               ";
		wait_clk(10);

		test_case <= "directly to release ";
		new_available <= '1';
		adm_lock <= '1';
		wait_clk(1); -- just one cmd has been processed
		new_available <= '0';
		wait_clk(1);
		unloaded <= '0';
		processing <= '1'; -- FIFO NVMe immeadiatly processes the request
		response <= '1'; -- filter response is immeadiate
		wait_clk(2);
		response <= '0';
		wait_clk(4);
		processing <= '0';
		unloaded <= '1';
		wait_clk(5);

		test_case <= "clear               ";
		wait_clk(10);

		test_case <= "simulation ended    ";
		wait;
	end process;

end Behavioral;


