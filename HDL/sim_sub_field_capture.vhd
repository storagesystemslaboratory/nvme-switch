--------------------------------------------------------------------------------
--
-- Module Name: sim_sub_field_capture
-- Create Date: 09/11/2019 (MM/DD/YYYY)
--
-- Company: ittChip - Unisinos
-- Engineer: Vinícius Gabriel LINDEN
-- 
-- Description: Stimulae sub_field_capture
-- 
--------------------------------------------------------------------------------
-- Recommended span of 300 ns for period of 10 ns
-- TODO:


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library UNISIM;
use UNISIM.VComponents.all;

--library STD;
--use STD.TEXTIO.ALL;

library work;
--use work.common_pkg.all;
use work.kratus_pkg.all;
use work.NVMe_pkg.all;

entity sim_sub_field_capture is
	end sim_sub_field_capture;


architecture Behavioral of sim_sub_field_capture is

	-- DO NOT DELETE
	constant period                 : time := 5 ns;
	constant hp                     : time := period/2; -- half period

	signal index                    : natural := 1; -- index for redability
	signal clk                      : std_logic := '1'; -- system base clock

	procedure wait_clk(constant T : in natural) is
	begin
		wait for t*period;
	end procedure;
	-- DELETION RESTRICTION ENDED

	-- UUT
	constant DATA_WIDTH         : natural := 64;
	constant CLEAR_ON_BOTH      : boolean := true;
	constant CLOCK_EDGE         : std_logic := '1';
	signal config               : std_logic_vector(7 downto 0) := (others => '0');
	signal config_updated       : std_logic := '0';
	signal stream          : std_logic_vector(7 downto 0) := (others => '0');
	signal count                : std_logic_vector(5 downto 0) := (others => '0');
	signal opcode               : std_logic_vector(7 downto 0) := (others => '0');
	signal new_data             : std_logic;
	signal clear                : std_logic := '0';
	signal field                : std_logic_vector(DATA_WIDTH-1 downto 0);
	signal rst                  : std_logic := '1';
	signal error                : std_logic;
	-- END OF UUT

	-- SIMULATION
	signal test_case                : string(1 to 20) := "waiting             ";
	signal data                     : unsigned(7 downto 0) := x"ac";
-- END OF SIMULATION

begin

	UUT : entity work.rtl_sub_field_capture
	generic map (
					CLEAR_ON_BOTH => CLEAR_ON_BOTH,
					CLOCK_EDGE => CLOCK_EDGE
				)
	port map (
				 config => config,
				 config_updated => config_updated,
				 stream => stream,
				 count => count,
				 opcode => opcode,
				 new_data => new_data,
				 clear => clear,
				 field => field,
				 rst => rst,
				 error => error,
				 clk => clk
			 );

	-- DO NOT DELETE
	clk <= not clk after hp;
	index <= index + 1 after period;
	-- DELETION RESTRICTION ENDED

	-- SEQUENTIAL DATA GENERATOR
	process
	begin
		wait for period;
		IncrementWrapFull(data);
	end process;
	stream <= std_logic_vector(data);
	-- END OF SEQUENTIAL DATA GENERATOR

	process
	begin
		wait_clk(2);
		rst <= '0';
		wait_clk(2);

		test_case <= "standard operation  ";
		config <= FC_SLBA;
		opcode <= NVME_READ;
		config_updated <= '1';
		wait_clk(1);
		config_updated <= '0';
		wait_clk(1);
		count <= "100111";
		wait_clk(1);
		count <= "101000"; -- activates
		wait_clk(1);
		count <= "101001";
		wait_clk(9);
		toggle(clear);

		test_case <= "clearing            ";
		wait_clk(10);

		test_case <= "test operation      ";
		config <= FC_TEST;
		opcode <= NVME_READ; -- to trigger the FC_TEST
		config_updated <= '1';
		wait_clk(1);
		config_updated <= '0';
		wait_clk(1);
		count <= "111101";
		wait_clk(1);
		count <= "111110"; -- activates
		wait_clk(1);
		count <= "111111";
		wait_clk(9);
		toggle(clear);

		test_case <= "clearing            ";
		wait_clk(10);

		test_case <= "simulation ended    ";
		wait;
	end process;

end Behavioral;


