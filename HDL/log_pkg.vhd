--------------------------------------------------------------------------------
--
-- Module Name: log_pkg.vhd
-- Create Date: XX/XX/XXXX (MM/DD/YYYY)
--
-- Company: ittChip - Unisinos
-- Engineer: Rodrigo Marques de Figueiredo
-- 
-- Description: Logarithmic functions
-- 
--------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.MATH_REAL.ALL;

package log_pkg is
	
	-- FUNCTION: log2
	-- Returns the base 2 logarithm of x
	function log2(x: integer) return integer;

	-- FUNCTION: logn (integer)
	-- Returns the base n logarithm of x
	function logn(x, n: integer) return integer;

	-- FUNCTION: logn (real)
	-- Returns the base n logarithm of x
	function logn(x, n: real) return real;

	-- FUNCTION: numbits
	-- Returns the number of bits necessary to represent an amount of values 
	function numbits(x: integer) return integer;

	-- FUNCTION: base2
	-- Returns true when tested variable is in 2 base otherwise returns false
	function base2(x: integer) return boolean;

end log_pkg;

package body log_pkg is

	-- FUNCTION: log2
	function log2(x: integer) return integer is
	begin
		return integer(ceil(log2(real(x))));
	end log2;

	-- FUNCTION: logn (integer)
	function logn(x, n: integer) return integer is
	begin
		if x = 0 OR n = 0 OR (x = 1 AND n = 1) then
			return 0;
		else
			return integer(ceil(log2(real(x))/log2(real(n))));			
		end if;
	end logn; 

	-- FUNCTION: logn (real)
	function logn(x, n: real) return real is
	begin
		return log2(x)/log2(n);
	end logn; 

	-- FUNCTION: numbits
	function numbits (x: integer) return integer is
		variable y : integer;
	begin
		y := x + 1;
		if y = 0 OR y = 1 then
			return 1;
		else
			return log2(y);
		end if;
	end numbits;

	-- FUNCTION: base2
	function base2 (x: integer) return boolean is
	begin
		if x - 2**log2(x) = 0 then
			return true;
		else
			return false;
		end if;
	end base2;

end log_pkg;
