--------------------------------------------------------------------------------
--
-- Module Name: rtl_edge_detection
-- Create Date: 09/02/2019 (MM/DD/YYYY)
-- Revision: 1
--
-- Company: ittChip - Unisinos
-- Engineer: Vinícius Gabriel LINDEN
-- 
-- Description: Real-time logic for edge detection
-- 
--------------------------------------------------------------------------------
-- TODO:

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

library UNISIM;
use UNISIM.VComponents.all;

--library work;


entity rtl_edge_detection is
	generic
	(
		SIGNALS 		: natural := 4; -- number of signals
		ACTIVE_LOW 		: boolean := false; -- true: output signal is active-low; false: output signal is active high
		BOTH_EDGES 		: boolean := true; -- true: activated on both edge; false: only the DETECTION_EDGE is active
		DETECTION_EDGE 	: std_logic := '1'; -- signal edge (only if BOTH_EDGES = false): '1' for rising, '0' for falling
		CLOCK_EDGE 		: std_logic := '1' -- change detection clock board: '1' for rising, '0' for falling
	);
	port
	(
		sense 			: in std_logic_vector(SIGNALS-1 downto 0); -- sense input
		updated 		: out std_logic_vector(SIGNALS-1 downto 0); -- configuration After Register (ar)
		clk 			: in std_logic -- clock
	);
end rtl_edge_detection;


architecture rtl of rtl_edge_detection is

	function HIGH_VALUE_ASSERTION (bool : boolean) return std_logic is
	begin
		if bool then
			return '0';
		else
			return '1';
		end if;
	end function HIGH_VALUE_ASSERTION;

	constant HIGH_VALUE 	: std_logic := HIGH_VALUE_ASSERTION(ACTIVE_LOW);
	constant LOW_VALUE 		: std_logic := not HIGH_VALUE;
	signal sense_edge 		: std_logic_vector(SIGNALS-1 downto 0); -- edge detection

begin

	process(clk)
	begin
		if clk'event and clk = CLOCK_EDGE then
			sense_edge <= sense;
		end if;
	end process;


	EACH_BIT : for i in 0 to SIGNALS-1 generate

		COMMIT_BOTH_EDGES : if BOTH_EDGES generate
			updated(i) <= HIGH_VALUE when
						  sense_edge(i) /= sense(i)
						   else LOW_VALUE;
		end generate COMMIT_BOTH_EDGES;

		COMMIT_ONE_EDGE : if not BOTH_EDGES generate
			updated(i) <= HIGH_VALUE when
						  sense_edge(i) /= sense(i) and sense(i) = DETECTION_EDGE
						   else LOW_VALUE;
		end generate COMMIT_ONE_EDGE;

	end generate EACH_BIT;

end rtl;


