--------------------------------------------------------------------------------
--
-- Module Name: sim_BRAM
-- Create Date: 06/03/2019 (MM/DD/YYYY)
--
-- Company: ittChip - Unisinos
-- Engineer: Vinícius Gabriel LINDEN
-- 
-- Description: Stimulae for BRAM instantiation
-- 
--------------------------------------------------------------------------------
-- Recommended span of 300 ns for period of 10 ns
-- TODO:


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
--use IEEE.NUMERIC_STD.ALL;

library UNISIM;
use UNISIM.VComponents.all;

--library STD;
--use STD.TEXTIO.ALL;

library work;
--use work.common_pkg.all;
use work.kratus_pkg.all;

entity sim_BRAM is
	end sim_BRAM;


architecture Behavioral of sim_BRAM is

	-- DO NOT DELETE
	constant period                 : time := 5 ns;
	constant hp                     : time := period/2; -- half period

	signal index                    : integer := 1; -- index for redability
	signal clk                      : std_logic := '1'; -- system base clock

	procedure wait_clk(constant T : in natural) is
	begin
		wait for T*period;
	end procedure;
	-- DELETION RESTRICTION ENDED

	-- SIMULATION
	constant space                  : integer := 5;
	signal test_case                : string(1 to 20) := "waiting             ";
	-- END OF SIMULATION

	-- INSTANTIATION PORTS
	constant RD_CTRL                : boolean := true;
	constant SHORT                  : boolean := false;
	constant DATA_WIDTH             : integer := 64;
	signal addra                    : std_logic_vector(BRAM_width(DATA_WIDTH,SHORT)-1 downto 0); -- address
	signal wrdataa                  : std_logic_vector(DATA_WIDTH-1 downto 0); -- write data
	signal wrmetaa                  : std_logic_vector(datawidth2paritywidth(DATA_WIDTH)-1 downto 0); -- write metadata
	signal rddataa                  : std_logic_vector(DATA_WIDTH-1 downto 0); -- read data
	signal rdmetaa                  : std_logic_vector(datawidth2paritywidth(DATA_WIDTH)-1 downto 0); -- read metadata
	signal wrena                    : std_logic_vector(datawidth2paritywidth(DATA_WIDTH)-1 downto 0); -- write
	signal rdena                    : std_logic;
	signal clka                     : std_logic; -- clock
	signal rsta                     : std_logic; -- reset
	signal addrb                    : std_logic_vector(BRAM_width(DATA_WIDTH,SHORT)-1 downto 0); -- address
	signal wrdatab                  : std_logic_vector(DATA_WIDTH-1 downto 0); -- write data
	signal wrmetab                  : std_logic_vector(datawidth2paritywidth(DATA_WIDTH)-1 downto 0); -- write metadata
	signal rddatab                  : std_logic_vector(DATA_WIDTH-1 downto 0); -- read data
	signal rdmetab                  : std_logic_vector(datawidth2paritywidth(DATA_WIDTH)-1 downto 0); -- read metadata
	signal wrenb                    : std_logic_vector(datawidth2paritywidth(DATA_WIDTH)-1 downto 0); -- write
	signal rdenb                    : std_logic;
	signal enableb                  : std_logic;
	signal clkb                     : std_logic; -- clock
	signal rstb                     : std_logic; -- reset
												 -- END OF INSTANTIATION PORTS

begin

	UUT : entity work.rtl_BRAM
	generic map (
					RD_CTRL => RD_CTRL,
					SHORT => SHORT,
					DATA_WIDTH => DATA_WIDTH
				)
	port map (
				 addra => addra,
				 wrdataa => wrdataa,
				 wrmetaa => wrmetaa,
				 rddataa => rddataa,
				 rdmetaa => rdmetaa,
				 wrena => wrena,
				 rdena => rdena,
				 clka => clka,
				 rsta => rsta,
				 addrb => addrb,
				 wrdatab => wrdatab,
				 wrmetab => wrmetab,
				 rddatab => rddatab,
				 rdmetab => rdmetab,
				 wrenb => wrenb,
				 rdenb => rdenb,
				 enableb => enableb,
				 clkb => clkb,
				 rstb => rstb
			 );

	-- DO NOT DELETE
	clk <= not clk after hp;
	index <= index + 1 after period;
	-- DELETION RESTRICTION ENDED

	clka <= clk;
	--clkb <= not clk;
	clkb <= clk;

	process
	begin
		addra <= (others => '0');
		wrdataa <= (others => '0');
		wrmetaa <= (others => '0');
		rsta <= '1';
		wrena <= (others => '0');
		rdena <= '1';
		addrb <= (others => '0');
		wrdatab <= (others => '0');
		wrmetab <= (others => '0');
		rstb <= '1';
		wrenb <= (others => '0');
		rdenb <= '1';
		wait_clk(space);
		test_case <= "reset               ";
		rsta <= '0';
		wait_clk(2);
		rstb <= '0';
		wait_clk(space);
		test_case <= "addr range          ";
		wrdataa(7 downto 0) <= "00000001";
		wrmetaa <= not wrmetaa;
		for i in addra'range loop
			if i /= addra'left then
				addra(i+1) <= '0';
			end if;
			addra(i) <= '1';
			wait for hp/4;
			addrb <= addra;
			wait_clk(5);
			wrena <= (others => '1');
			wait_clk(1);
			wrena <= (others => '0');
			wait_clk(2);
		end loop;
		addra <= (others => '0');
		wait_clk(space);
		test_case <= "reset capability    "; -- check if reset clears the memory
		for i in addra'range loop -- see what the memory contains
			if i /= addra'left then
				addra(i+1) <= '0';
			end if;
			addra(i) <= '1';
			wait_clk(5);
		end loop;
		addra <= (others => '0'); -- back to address 0 (no value inside)
		wait_clk(5);
		rsta <= '1'; -- test to see what happens across the swept range
		wait_clk(2);
		rsta <= '0';
		wait_clk(3);
		for i in addra'range loop
			if i /= addra'left then
				addra(i+1) <= '0';
			end if;
			addra(i) <= '1';
			wait_clk(2);
		end loop;
		addra <= (others => '0');
		wait_clk(5);
		wrdataa(7 downto 0) <= "00000010";
		wrmetaa <= not wrmetaa;
		wrena <= (others => '1'); -- store a value inside address 0
		wait_clk(1);
		wrena <= (others => '0');
		wait_clk(5);
		rsta <= '1'; -- reset to see if the value has been erased
		wait_clk(2);
		rsta <= '0';
		wait_clk(3);
		wait_clk(space);
		test_case <= "write enable cap.   ";
		addra(3 downto 0) <= "1010";
		wait_clk(1);
		addrb <= addra;
		wait_clk(5);
		wrdataa(7 downto 0) <= "11111000";
		wrmetaa <= not wrmetaa;
		wrena(0) <= '1'; -- rightmost byte
		wait_clk(1);
		wrena <= (others => '0');
		wait_clk(2);
		wrdataa(7 downto 0) <= "11011001";
		wrmetaa <= not wrmetaa;
		wait_clk(2);
		wrena(wrena'left) <= '1'; -- leftmost byte
		wait_clk(1);
		wrena <= (others => '0');
		wait_clk(space);
		test_case <= "write               ";
		wrdataa(7 downto 0) <= "10101010";
		wrmetaa <= not wrmetaa;
		wait_clk(space);
		wrena <= (others => '1');
		wait_clk(1);
		wrena <= (others => '0');
		test_case <= "address change      ";
		wrdataa(7 downto 0) <= "10101111";
		wrmetaa <= not wrmetaa;
		wait_clk(space);
		addra(addra'left downto 1) <= (others => '0');
		addra(0) <= '1';
		wait_clk(5);
		wrena <= (others => '1');
		wait_clk(1);
		wrena <= (others => '0');
		wait_clk(space);
		test_case <= "leave wren en & chan";
		wrdataa(7 downto 0) <= "00001111";
		wrmetaa <= not wrmetaa;
		wrena <= (others => '1');
		wait_clk(5);
		wrdataa(7 downto 0) <= "00111111";
		wrmetaa <= not wrmetaa;
		wait_clk(5);
		wrdataa(7 downto 0) <= "11111111";
		wrmetaa <= not wrmetaa;
		wait_clk(100); -- 
		wrmetaa <= not wrmetaa;
		wait;
	end process;

end Behavioral;



