--------------------------------------------------------------------------------
--
-- Module Name: rtl_NVMe_switch
-- Create Date: 10/31/2019 (MM/DD/YYYY)
-- Revision: 0
--
-- Company: ittChip - Unisinos
-- Engineer: Vinícius Gabriel LINDEN
-- 
-- Description: Assembly for the IOSR-JEEE
--		"Enabling Fast Decision Criteria in NVMe SSDs through
--		NVMe-based Switch" article, published in March 2020.
-- 
--------------------------------------------------------------------------------
-- TODO:

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

library UNISIM;
use UNISIM.VComponents.all;

library work;
use work.common_pkg.all;
use work.NVMe_pkg.all;

entity rtl_NVMe_switch is
	generic
	(
		CLEAR_ON_BOTH 		: boolean := true; -- true: clear is activated on both edge; false: only the CLOCK_EDGE is active
		CLOCK_EDGE 			: std_logic := '1' -- '1' for rising, '0' for falling
	);
	port
	(
		-- HOST DECAPSULATOR (SC)
		stream_hostSC 		: in std_logic_vector(7 downto 0);
		valid_hostSC 		: in std_logic;
		ready_hostSC 		: out std_logic;

		-- SSD ENCAPSULATOR (SC)
		stream_ssdSC 		: out std_logic_vector(7 downto 0);
		valid_ssdSC 		: out std_logic;
		ready_ssdSC 		: in std_logic;

		-- ARBITRATOR CONFIGURATION
		config_ac 			: in std_logic_vector(7 downto 0);
		config_updated_ac 	: in std_logic;

		-- GENERAL PURPOSE
		error 				: out std_logic;
		rst 				: in std_logic;
		clk 				: in std_logic
	);
end rtl_NVMe_switch;

architecture rtl of rtl_NVMe_switch is

	signal valid_hostSC_split 		: std_logic_vector(1 downto 0);
	signal ready_hostSC_split 		: std_logic_vector(1 downto 0);
	signal full 					: std_logic;
	signal empty 					: std_logic;
	signal unload 					: std_logic;
	signal unloaded 				: std_logic;
	signal processing 				: std_logic;
	signal number_pkgs 				: std_logic_vector(7 downto 0);
	signal submit 					: std_logic;
	signal response 				: std_logic;
	signal host2ssd 				: std_logic;
	signal cmd_id 					: std_logic_vector(15 downto 0);
	signal stream_demux 			: std_logic_vector(7 downto 0);
	signal valid_demux 				: std_logic;
	signal ready_demux 				: std_logic;
	signal stream_generic 			: std_logic_vector(7 downto 0);
	signal valid_generic			: std_logic;
	signal ready_generic			: std_logic;
	signal error_arbitrator 		: std_logic;
	signal error_FIFO_NVMe 			: std_logic;

begin

	error <= error_arbitrator or error_FIFO_NVMe;

	BUS_SPLITTER : entity work.rtl_bus_splitter
	generic map
	(
		RAMIFICATIONS => 2
	)
	port map
	(
		valid_source => valid_hostSC,
		ready_source => ready_hostSC,
		valid_destination => valid_hostSC_split,
		ready_destination => ready_hostSC_split,
		halt => '0'
	);

	FIFO_NVME : entity work.rtl_FIFO_NVMe
	generic map
	(
		SUBMISSION_CMD => true,
		SHORT => true
	)
	port map
	(
		stream_wr => stream_hostSC,
		valid_wr => valid_hostSC_split(0),
		ready_wr => ready_hostSC_split(0),
		stream_rd => stream_demux,
		valid_rd => valid_demux,
		ready_rd => ready_demux,
		full => full,
		empty => empty,
		block_pkgs => '0',
		unload => unload,
		unloaded => unloaded,
		processing => processing,
		number_pkgs => number_pkgs,
		error => error_FIFO_NVMe,
		rst => rst,
		clk => clk
	);

	ARBITRATOR : entity work.rtl_arbitrator
	generic map
	(
		CLEAR_ON_BOTH => CLEAR_ON_BOTH,
		CLOCK_EDGE => CLOCK_EDGE
	)
	port map
	(
		config => config_ac,
		config_updated => config_updated_ac,
		stream => stream_hostSC,
		valid => valid_hostSC_split(1),
		ready => ready_hostSC_split(1),
		full => full,
		empty => empty,
		unload => unload,
		unloaded => unloaded,
		processing => processing,
		number_pkgs => number_pkgs,
		cmd_id_filter => cmd_id,
		submit => submit,
		response => response,
		generic_ask => '0',
		generic_lock => open,
		generic_flow => '1',
		host2ssd => host2ssd,
		error => error_arbitrator,
		rst => rst,
		clk => clk
	);

	DEMUX_NVME : entity work.rtl_demux_NVMe
	port map
	(
		stream_source => stream_demux,
		valid_source => valid_demux,
		ready_source => ready_demux,
		stream_destin1 => stream_generic,
		valid_destin1 => valid_generic,
		ready_destin1 => ready_generic,
		stream_destin2 => stream_ssdSC,
		valid_destin2 => valid_ssdSC,
		ready_destin2 => ready_ssdSC,
		--ready_destin2 => '1',
		sel => host2ssd
	);

	response <= submit;
	ready_generic <= '1';

end rtl;

