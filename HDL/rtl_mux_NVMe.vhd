--------------------------------------------------------------------------------
--
-- Module Name: rtl_mux_NVMe
-- Create Date: 10/03/2019 (MM/DD/YYYY)
-- Revision: 0
--
-- Company: ittChip - Unisinos
-- Engineer: Vinícius Gabriel LINDEN
-- 
-- Description: Real-time logic multiplexer for the NVMe stream convention.
--              Multiplexer means here that it has two NVMe stream sources & only
--              one destination.
--              "source1" is selected when sel = '0'; "source2", sel = '1'.
-- 
--------------------------------------------------------------------------------
-- TODO:

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

library UNISIM;
use UNISIM.VComponents.all;

--library work;
--use work.common_pkg.all;

entity rtl_mux_NVMe is
	port (
			 stream_source1 : in std_logic_vector(7 downto 0);
			 valid_source1       : in std_logic;
			 ready_source1       : out std_logic;

			 stream_source2 : in std_logic_vector(7 downto 0);
			 valid_source2       : in std_logic;
			 ready_source2       : out std_logic;

			 stream_destin  : out std_logic_vector(7 downto 0);
			 valid_destin        : out std_logic;
			 ready_destin        : in std_logic;

			 sel                 : in std_logic
		 );
end rtl_mux_NVMe;

architecture rtl of rtl_mux_NVMe is

begin

	MUX_NVME_STREAM : entity work.rtl_mux_2
	generic map(
				   DATA_WIDTH => 8
			   )
	port map (
				 sel => sel,
				 iput1 => stream_source1,
				 iput2 => stream_source2,
				 oput => stream_destin
			 );

	MUX_VALID : entity work.rtl_mux_2
	generic map(
				   DATA_WIDTH => 1
			   )
	port map (
				 sel => sel,
				 iput1(0) => valid_source1,
				 iput2(0) => valid_source2,
				 oput(0) => valid_destin
			 );

	DEMUX_READY : entity work.rtl_demux_2
	generic map(
				   DATA_WIDTH => 1,
				   NO_IDLE_VALUE => false,
				   UNSELECTED_VALUE_1 => "0",
				   UNSELECTED_VALUE_2 => "0"
			   )
	port map (
				 sel => sel,
				 iput(0) => ready_destin,
				 oput1(0) => ready_source1,
				 oput2(0) => ready_source2
			 );

end rtl;


