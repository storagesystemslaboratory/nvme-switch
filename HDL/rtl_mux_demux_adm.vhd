--------------------------------------------------------------------------------
--
-- Module Name: rtl_mux_demux_adm
-- Create Date: 09/06/2019 (MM/DD/YYYY)
-- Revision: 3
--
-- Company: ittChip - Unisinos
-- Engineer: Vinícius Gabriel LINDEN
-- 
-- Description: Real-time logic for the mux demux adm FSM
-- 
--------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VComponents.all;

entity rtl_mux_demux_adm is
	generic
	(
		GENERIC_PRIORITARY 	: boolean := false; -- true: generic is prioritary; false: arbitrator is prioritary
		CLOCK_EDGE 			: std_logic := '1' -- '1' for rising, '0' for falling
	);
	port
	(
		arb_ask 			: in std_logic; -- arbitrator asks for lock
		arb_lock 			: out std_logic; -- arbitrator is locked
		arb_flow 			: in std_logic; -- arbitrator is controlling the flow

		generic_ask 		: in std_logic; -- generic SSD asks for lock
		generic_lock 		: out std_logic; -- generic SSD is locked
		generic_flow 		: in std_logic; -- generic SSD is flow control

		-- control
		host2ssd 			: out std_logic; -- controlls the flow host->SSD

		rst 				: in std_logic;
		clk 				: in std_logic
	);
end rtl_mux_demux_adm;

architecture rtl of rtl_mux_demux_adm is

	-- SEMAPHORE MODULE
	signal prioritary_ask 	: std_logic;
	signal prioritary_lock 	: std_logic;
	signal secondary_ask 	: std_logic;
	signal secondary_lock 	: std_logic;
	signal ctrl_sel 		: std_logic;
	-- SEMAPHORE MODULE
	
	-- MUX-DEMUX SIGNALS
	signal prioritary_flow 	: std_logic;
	signal secondary_flow 	: std_logic;
	-- END OF MUX-DEMUX SIGNALS

begin

	FROM_HOST_TO_SSD : entity work.rtl_modules_semaphore
	generic map
	(
		CLOCK_EDGE => CLOCK_EDGE
	)
	port map
	(
		prioritary_ask => prioritary_ask,
		prioritary_lock => prioritary_lock,
		secondary_ask => secondary_ask,
		secondary_lock => secondary_lock,
		ctrl_sel => ctrl_sel,
		rst => rst,
		clk => clk
	);

	MUX : entity work.rtl_mux_2
	generic map
	(
		DATA_WIDTH => 1
	)
	port map
	(
		sel => ctrl_sel,
		iput1(0) => prioritary_flow,
		iput2(0) => secondary_flow,
		oput(0) => host2ssd
	);

	ARBITRATOR_FIRST : if not GENERIC_PRIORITARY generate
		prioritary_ask <= arb_ask;
		arb_lock <= prioritary_lock;
		prioritary_flow <= arb_flow;
		secondary_ask <= generic_ask;
		generic_lock <= secondary_lock;
		secondary_flow <= generic_flow;
	end generate ARBITRATOR_FIRST;

	GENERIC_FIRST : if GENERIC_PRIORITARY generate
		prioritary_ask <= generic_ask;
		generic_lock <= prioritary_lock;
		prioritary_flow <= generic_flow;
		secondary_ask <= arb_ask;
		arb_lock <= secondary_lock;
		secondary_flow <= arb_flow;
	end generate GENERIC_FIRST;

end rtl;


