--------------------------------------------------------------------------------
--
-- Module Name: rtl_assertion_adm
-- Create Date: 09/19/2019 (MM/DD/YYYY)
-- Revision: 1
--
-- Company: ittChip - Unisinos
-- Engineer: Vinícius Gabriel LINDEN
-- 
-- Description: Real time logic for the finite state machine inside of
--              assertion module
-- 
--------------------------------------------------------------------------------
-- TODO:

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

library UNISIM;
use UNISIM.VComponents.all;

--library work;
--use work.NVMe_pkg.all;

entity rtl_assertion_adm is
	generic
	(
		CLOCK_EDGE			: std_logic := '1' -- '1' for rising, '0' for falling
	);
	port
	(
		-- CONTROL
		wren 				: out std_logic; -- do write data
		full 				: in std_logic;
		direction 			: in std_logic;
		direction_store 	: out std_logic; -- latched direction

		-- FIELD CAPTURE
		evaluate			: in std_logic;

		-- GENERAL PURPOSE
		rst					: in std_logic; -- async reset
		error				: out std_logic; -- error
		clk					: in std_logic -- clock
	);
end rtl_assertion_adm;

architecture rtl of rtl_assertion_adm is

	signal wren_i 			: std_logic; -- enables counter and BRAM write

	type fsm_state is (IDLE, -- nothing to do
					   REQUESTED, -- send request to field capture module
					   WAIT_REPLY, -- wait for field
					   STORING, -- storing the direction
					   BLOCKED -- FIFO is full
					   );
	signal state_fsm 		: fsm_state;
	signal error_blocked_evaluate : std_logic; -- FIFO is full, but arbitrator is receiving requests

begin

	wren <= wren_i;
	wren_i <= '1' when state_fsm = STORING else '0';
	error <= error_blocked_evaluate;

	process(rst,clk)
	begin
		if rst = '1' then
			direction_store <= '0'; -- doesn't matter
			state_fsm <= IDLE;
		elsif clk'event and clk = CLOCK_EDGE then
			case state_fsm is
				when IDLE =>
					if evaluate = '1' and full = '0' then
						state_fsm <= REQUESTED;
					end if;
					if full = '1' then
						state_fsm <= BLOCKED;
					end if;
				when REQUESTED =>
					state_fsm <= WAIT_REPLY;
				when WAIT_REPLY =>
					direction_store <= direction;
					state_fsm <= STORING;
				when STORING =>
					state_fsm <= IDLE;
				when BLOCKED =>
					if full = '0' then
						state_fsm <= IDLE;
					end if;
				when others =>
					direction_store <= '0'; -- doesn't matter
					state_fsm <= IDLE;
			end case;
		end if;
	end process;

	error_blocked_evaluate <= '1' when state_fsm = BLOCKED and evaluate = '1' else '0';

end rtl;


