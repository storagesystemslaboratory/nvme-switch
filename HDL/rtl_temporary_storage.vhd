--------------------------------------------------------------------------------
--
-- Module Name: rtl_temporary_storage
-- Create Date: 08/30/2019 (MM/DD/YYYY)
-- Revision: 1
--
-- Company: ittChip - Unisinos
-- Engineer: Vinícius Gabriel LINDEN
-- 
-- Description: Real-time logic for temporary storage sub-module
-- 
--------------------------------------------------------------------------------
-- TODO:
--  Implement block_byte
--  (maybe) implement a generic DATA_WIDTH
--	fix EN_CLEAR = false, new_data = '0' at start of new packet

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
--use IEEE.NUMERIC_STD.ALL;

library UNISIM;
use UNISIM.VComponents.all;

--library work;
--use work.common_pkg.all;

entity rtl_temporary_storage is
	generic
	(
		EN_CLEAR			: boolean := true; -- makes the new_data & clear mechanism optional
		CLEAR_ON_BOTH 		: boolean := true; -- true, clear is activated on both edge, if false only the CLOCK_EDGE is active
		CLOCK_EDGE 			: std_logic := '1' -- clock edge, '1' for rising, '0' for falling
	);
	port (
		config_updated 		: in std_logic; -- '1' if new configuration has been sent
		stream_delayed		: in std_logic_vector(7 downto 0);
		trigger_capture		: in std_logic; -- capture has been triggered
		count 				: in std_logic_vector(5 downto 0); -- byte count
		last_delayed		: in std_logic;

		new_data 			: out std_logic;
		clear 				: in std_logic; -- send the new data (clear the storage)
		field_raw 			: out std_logic_vector(63 downto 0); -- captured field before mask

		-- GENERAL PURPOSE
		rst 				: in std_logic;
		error 				: out std_logic;
		clk 				: in std_logic

	);
end rtl_temporary_storage;

architecture rtl of rtl_temporary_storage is

	signal reset 			: std_logic; -- DO NOT put it on the sensitivity list

	signal new_data_i 		: std_logic;
	signal stored_data 		: std_logic_vector(63 downto 0); -- internal register
	signal count_store 		: std_logic_vector(2 downto 0); -- byte count

	signal wren 			: std_logic; -- process has been triggered (counting)
	signal saturated 		: std_logic; -- counter is saturated
	signal error_missed 	: std_logic; -- missed a captured field
	--signal block_byte       : std_logic; -- block storage (if count_store surpasses # of bytes)

	signal out_stored_data 	: std_logic; -- clear signal to output data
	signal error_clear 		: std_logic; -- clear activated before new field is available
	signal error_trigger 	: std_logic; -- trigger activated before field storage process ended
-- MAYBE DO THIS WITH counter_step

begin

	reset <= rst or config_updated;
	new_data <= new_data_i;

	CLEAR_MECHANISM_EN : if EN_CLEAR generate
		error <= error_missed or error_clear or error_trigger;
	end generate CLEAR_MECHANISM_EN;

	CLEAR_MECHANISM_DIS : if not EN_CLEAR generate
		error <= error_trigger;
	end generate CLEAR_MECHANISM_DIS;

	BYTE_CTRL : process(rst,clk)
	begin
		if reset = '1' then
			wren <= '0';
			new_data_i <= '0';
			error_missed <= '0';
			error_clear <= '0';
			error_trigger <= '0';
			field_raw <= (others => '0');
		elsif clk'event and clk = CLOCK_EDGE then

			if wren = '1' then -- writing a package
				case count_store is -- storing the package into internal register
					when "000" =>
						stored_data(7 downto 0) <= stream_delayed;
					when "001" =>
						stored_data(15 downto 8) <= stream_delayed;
					when "010" =>
						stored_data(23 downto 16) <= stream_delayed;
					when "011" =>
						stored_data(31 downto 24) <= stream_delayed;
					when "100" =>
						stored_data(39 downto 32) <= stream_delayed;
					when "101" =>
						stored_data(47 downto 40) <= stream_delayed;
					when "110" =>
						stored_data(55 downto 48) <= stream_delayed;
					when "111" =>
						stored_data(63 downto 56) <= stream_delayed;
					when others =>
				end case;

				if trigger_capture = '1' then
					error_trigger <= '1'; -- storage process is triggered when its already running
				end if;

				if saturated = '1' then -- returns to idle state
					wren <= '0';
					new_data_i <= '1';
				end if;

			elsif trigger_capture = '1' then -- process has been triggered
				wren <= '1';
				error_missed <= new_data_i; -- if new_data is 1, field will be missed
				if not EN_CLEAR then -- if clear mechanism is optional
					new_data_i <= '0'; -- resets new_data indication at start of new packet
				end if;
			end if;

			if out_stored_data = '1' then -- clear is activated
				if new_data_i = '1' then
					field_raw <= stored_data;
					new_data_i <= '0';
				else
					error_clear <= '1'; -- user request the newly stored field, but there is none
				end if;
			end if;

			if not EN_CLEAR and last_delayed = '1' then -- if clear mechanism is disabled
				field_raw <= stored_data; -- output stored field
			end if;

		end if;
	end process BYTE_CTRL;

	COUNTER : entity work.rtl_counter
	generic map
	(
		CNT_RANGE => 7,
		CNT_RESET => 0,
		CNT_SATURATE_EN => false
	)
	port map
	(
		rst => reset,
		clk => clk,
		clk_en => '1',
		cnt_en => wren,
		cnt => count_store,
		sat => saturated
	);

	CLEAR_DETECTION : entity work.rtl_edge_detection
	generic map
	(
		SIGNALS => 1,
		BOTH_EDGES => CLEAR_ON_BOTH,
		CLOCK_EDGE => CLOCK_EDGE
	)
	port map
	(
		sense(0) => clear,
		updated(0) => out_stored_data,
		clk => clk
	);



end rtl;


