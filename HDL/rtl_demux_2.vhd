--------------------------------------------------------------------------------
--
-- Module Name: rtl_demux_2
-- Create Date: 08/26/2019 (MM/DD/YYYY)
-- Revision: 0
--
-- Company: ittChip - Unisinos
-- Engineer: Vinícius Gabriel LINDEN
-- 
-- Description: Real-time logic for 2 output with W bit width demultiplexer
-- 
--------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
--use IEEE.NUMERIC_STD.ALL;

library UNISIM;
use UNISIM.VComponents.all;

--library STD;
--use STD.TEXTIO.ALL;

--library work;
--use work.common_pkg.all;

entity rtl_demux_2 is
	generic (
				DATA_WIDTH          : natural := 8; -- I/O width [bit]
				NO_IDLE_VALUE		: boolean := true; -- leave the output alone when it is not selected
				UNSELECTED_VALUE_1	: std_logic_vector := (others => '0'); -- NO_IDLE_VALUE = false, the value of the oput1 when not selected
				UNSELECTED_VALUE_2	: std_logic_vector := (others => '0') -- NO_IDLE_VALUE = false, the value of the out2 when not selected
			);
	port (
			 sel         : in std_logic; -- input selection, '0' for 1st, '1' for 2nd
			 iput        : in std_logic_vector(DATA_WIDTH-1 downto 0); -- input
			 oput1       : out std_logic_vector(DATA_WIDTH-1 downto 0); -- first output (sel = '0')
			 oput2       : out std_logic_vector(DATA_WIDTH-1 downto 0) -- second output (sel = '1')
		 );
end rtl_demux_2;


architecture rtl of rtl_demux_2 is

begin

	-- implementation with AND gate would have implied NO_IDLE_VALUE = false and UNSELECTED_VALUE = '0'

	DO_NOTHING : if NO_IDLE_VALUE generate
		oput1 <= iput when sel = '0';
		oput2 <= iput when sel = '1';
	end generate;

	IDLE_VALUE : if not NO_IDLE_VALUE generate
		oput1 <= iput when sel = '0' else UNSELECTED_VALUE_1;
		oput2 <= iput when sel = '1' else UNSELECTED_VALUE_2;
	end generate;

end rtl;


