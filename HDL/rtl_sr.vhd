--------------------------------------------------------------------------------
--
-- Module Name: rtl_sr
-- Create Date: XX/XX/XXXX (MM/DD/YYYY)
-- Revision: X
--
-- Company: ittChip - Unisinos
-- Engineer: Rodrigo Marques de Figueiredo
--
-- Description: Shift register with variable data width.
--				Divisor must be even.
--
--------------------------------------------------------------------------------
-- TODO:

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
--use IEEE.NUMERIC_STD.ALL;

library UNISIM;
use UNISIM.VComponents.all;

--library work;
--use work.common_pkg.all;

entity rtl_sr is
	generic
	(
		MODE		: string	:= "STATIC";	-- Modo de fucionamento do shift-register ("STATIC"/"DYNAMIC")
		OUTPUT_TYPE	: string	:= "SYNC";		-- Habilita registrador de saida ("SYNC"/"ASYNC")
		ADDR_SEL	: integer	:= 3;			-- Endere�o de leitura do shift-register(modo est�tico)
		DATA_WIDTH	: integer	:= 3;			-- Largura barramento dados
		ADDR_WIDTH	: integer	:= 3;			-- Largura barramento enderecos
		CLOCK_EDGE	: std_logic := '1' -- '1' for rising, '0' for falling
	);
	port
	(
		d			: in std_logic_vector(DATA_WIDTH-1 downto 0); -- input
		q			: out std_logic_vector(DATA_WIDTH-1 downto 0); -- output
		a			: in std_logic_vector(ADDR_WIDTH-1 downto 0); -- address
		clk_en		: in std_logic; -- clock enable
		clk			: in std_logic
	);

end rtl_sr;

architecture rtl of rtl_sr is

	signal q_sr		: std_logic_vector(DATA_WIDTH-1 downto 0); -- shift register data
	signal q_reg	: std_logic_vector(DATA_WIDTH-1 downto 0); -- register data

begin

	q <= q_reg when OUTPUT_TYPE = "SYNC" else q_sr;

	CLOCK_ENABLE : process(clk)
	begin
		if clk'event and clk = CLOCK_EDGE then
			if clk_en = '1' then
				q_reg <= q_sr;
			end if;
		end if;
	end process CLOCK_ENABLE;

	SHIFT_REGISTER_COUNT_DOWN : for n in 0 to DATA_WIDTH-1 generate
		SHIFT_REGISTER : entity work.rtl_srl16e
		generic map
		(
			MODE => MODE,
			ADDR_SEL => ADDR_SEL,
			ADDR_WIDTH => ADDR_WIDTH,
			CLOCK_EDGE => CLOCK_EDGE
		)
		port map
		(
			d => d(n),
			q => q_sr(n),
			a => a,
			clk_en => clk_en,
			clk => clk
		);
	end generate SHIFT_REGISTER_COUNT_DOWN;

end rtl;
