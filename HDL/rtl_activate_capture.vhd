--------------------------------------------------------------------------------
--
-- Module Name: rtl_activate_capture
-- Create Date: 09/10/2019 (MM/DD/YYYY)
-- Revision: 2
--
-- Company: ittChip - Unisinos
-- Engineer: Vinícius Gabriel LINDEN
-- 
-- Description: Real-time logic for triggering the NVMe capture process.
--				Use in field capture only.
--
-- Procedure: To add a new configuration, create a constant in the NVMe_pkg file,
--            add the corresponding opcode, add the corresponding count value &
--            enable with "valid"
-- 
--------------------------------------------------------------------------------
-- TODO:

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
--use IEEE.NUMERIC_STD.ALL;

library UNISIM;
use UNISIM.VComponents.all;

library work;
use work.NVMe_pkg.all;

entity rtl_activate_capture is
	generic
	(
		USE_CONFIG			: natural := 1 -- 1 = FC; 2 = OC; 3 = AC(1); 4 = AC(2), ...
	);
	port
	(
		config				: in std_logic_vector(7 downto 0); -- configuration
		opcode				: in std_logic_vector(7 downto 0); -- cmd opcode
		count				: in std_logic_vector(5 downto 0); -- byte count

		opcode_match 		: out std_logic; -- opcode matches the configuration
		trigger_capture		: out std_logic;  -- trigger capture
		field_mask			: out std_logic_vector(63 downto 0) -- masking-out unused bits
	);
end rtl_activate_capture;

architecture rtl of rtl_activate_capture is

	-- FIELD CONSTANTS
	constant BYTE_NULL		: std_logic_vector := "000000"; -- doesn't matter
	constant BYTE_SLBA		: std_logic_vector := "101000"; -- 40
	constant BYTE_PRP1		: std_logic_vector := "011000"; -- 24
	constant BYTE_PRP2		: std_logic_vector := "100000"; -- 32
	constant BYTE_FUA		: std_logic_vector := "110011"; -- 51 (not the first bit! becareful)
	constant BYTE_DSM		: std_logic_vector := "110100"; -- 52
	constant BYTE_TEST		: std_logic_vector := "111110"; -- 62
	constant MASK_NULL		: std_logic_vector := x"0000000000000000";  -- doesn't matter
	constant MASK_SLBA		: std_logic_vector := x"FFFFFFFFFFFFFFFF";
	constant MASK_PRP1		: std_logic_vector := x"FFFFFFFFFFFFFFFF";
	constant MASK_PRP2		: std_logic_vector := x"FFFFFFFFFFFFFFFF";
	constant MASK_FUA		: std_logic_vector := x"000000000000000F"; -- not the 1st bit becareful
	constant MASK_DSM		: std_logic_vector := x"00000000000000FF";
	constant MASK_TEST		: std_logic_vector := x"00000000FFFFFFFF";
	-- END OF FIELD CONSTANTS

	signal count_mask		: std_logic_vector(5 downto 0); -- count value for the configuration
	signal count_match		: std_logic; -- count = count_mask

	signal valid_config		: std_logic; -- 1: config is valid; 0: config is invalid
	signal valid_config2	: std_logic;
	signal valid_config_reduced: std_logic;

	signal opcode_mask		: std_logic_vector(7 downto 0); -- opcode for the configuration
	signal opcode_mask2		: std_logic_vector(7 downto 0);
	signal op_match_raw		: std_logic; -- opcode = opcode_mask
	signal op_match_raw2	: std_logic; -- opcode = opcode_mask2
	signal op_match_raw_reduced: std_logic; -- opcode = opcode_mask

begin

	---------------------------------
	-- YOU MAY CHANGE BELOW THIS LINE
	---------------------------------

	-- !! FIELD CAPTURE CONFIGURATION

	FIELD_CAPTURE_CONFIGURATION : if USE_CONFIG = 1 generate

		-- VALID CONFIGURATION
		with config select valid_config <=
			'1' when FC_SLBA,
			'1' when FC_PRP1,
			'1' when FC_PRP2,
			'1' when FC_FUA,
			'1' when FC_DSM,
			'1' when FC_TEST,
			'0' when others;

		with config select valid_config2 <= -- ADDITIONAL opcode mask
			'0' when others;
		-- END OF VALID CONFIGURATION

		-- SPECIFIC OPCODE TO TRIGGER
		with config select opcode_mask <=
			NVME_READ	when FC_SLBA,
			NVME_READ	when FC_PRP1,
			NVME_READ	when FC_PRP2,
			NVME_READ	when FC_FUA,
			NVME_READ	when FC_DSM,
			NVME_READ	when FC_TEST,
			NVME_RANDOM	when others;

		with config select opcode_mask2 <=
			NVME_RANDOM	when others;
		-- END OF SPECIFIC OPCODE TO TRIGGER

		-- SPECIFIC BYTE TO TRIGGER
		with config select count_mask <=
			BYTE_SLBA 	when FC_SLBA,
			BYTE_PRP1 	when FC_PRP1,
			BYTE_PRP2 	when FC_PRP2,
			BYTE_FUA 	when FC_FUA,
			BYTE_DSM 	when FC_DSM,
			BYTE_TEST 	when FC_TEST,
			BYTE_NULL 	when others;
		-- END OF SPECIFIC BYTE TO TRIGGER

		-- MASKING FILTER
		with config select field_mask <=
			MASK_SLBA 	when FC_SLBA,
			MASK_PRP1 	when FC_PRP1,
			MASK_PRP2 	when FC_PRP2,
			MASK_FUA 	when FC_FUA,
			MASK_DSM 	when FC_DSM,
			MASK_TEST 	when FC_TEST,
			MASK_NULL 	when others;
		-- END OF MASKING FILTER

	end generate FIELD_CAPTURE_CONFIGURATION;

	-- !! OPCODE COUNTER CONFIGURATION

	-- use valid_config and opcode_mask only if configuration is ranged

	OPCODE_COUNTER_CONFIGURATION : if USE_CONFIG = 2 generate

		-- VALID CONFIGURATION
		with config select valid_config <=
			'1' when OC_RD_RANGE,
			'1' when OC_RW_RANGE,
			'0' when others;

		with config select valid_config2 <= -- ADDITIONAL opcode mask
			'1' when OC_RW_RANGE,
			'0' when others;
		-- END OF VALID CONFIGURATION

		-- SPECIFIC OPCODE TO TRIGGER
		with config select opcode_mask <=
			NVME_READ		when OC_RD_RANGE,
			NVME_READ		when OC_RW_RANGE,
			NVME_RANDOM		when others;

		with config select opcode_mask2 <= -- ADDITIONAL opcode mask
			NVME_WRITE		when OC_RW_RANGE,
			NVME_RANDOM		when others;
		-- END OF SPECIFIC OPCODE TO TRIGGER

		-- SPECIFIC BYTE TO TRIGGER
		with config select count_mask <=
			BYTE_SLBA 	when OC_RD_RANGE,
			BYTE_SLBA 	when OC_RW_RANGE,
			BYTE_NULL 	when others;
		-- END OF SPECIFIC BYTE TO TRIGGER

		-- MASKING FILTER
		with config select field_mask <=
			MASK_SLBA 	when OC_RW_RANGE,
			MASK_SLBA 	when OC_RD_RANGE,
			MASK_NULL 	when others;
		-- END OF MASKING FILTER

	end generate OPCODE_COUNTER_CONFIGURATION;

	-- !! 1ST ARBITRATOR CONFIGURATION

	-- use valid_config and opcode_mask only if configuration is ranged

	FIST_ARBITRATOR_CONFIGURATION : if USE_CONFIG = 3 generate

		-- VALID CONFIGURATION
		with config select valid_config <=
			'1' when AC_TEST,
			'0' when others;

		with config select valid_config2 <= -- ADDITIONAL opcode mask
			'0' when others;
		-- END OF VALID CONFIGURATION

		-- SPECIFIC OPCODE TO TRIGGER
		with config select opcode_mask <=
			NVME_READ	when AC_TEST,
			NVME_RANDOM	when others;

		with config select opcode_mask2 <=
			NVME_RANDOM	when others;
		-- END OF SPECIFIC OPCODE TO TRIGGER

		-- SPECIFIC OPCODE TO TRIGGER
		with config select opcode_mask <=
			NVME_READ	when AC_TEST,
			NVME_RANDOM	when others;

		with config select opcode_mask2 <=
			NVME_RANDOM	when others;
		-- END OF SPECIFIC OPCODE TO TRIGGER

		-- SPECIFIC BYTE TO TRIGGER
		with config select count_mask <=
			BYTE_SLBA 	when AC_TEST,
			BYTE_NULL 	when others;
		-- END OF SPECIFIC BYTE TO TRIGGER

		-- MASKING FILTER
		with config select field_mask <=
			MASK_SLBA 	when FC_TEST,
			MASK_NULL 	when others;
		-- END OF MASKING FILTER

	end generate FIST_ARBITRATOR_CONFIGURATION;

	--------------------------------
	-- DO NOT CHANGE BELOW THIS LINE
	--------------------------------

	COMPARE_COUNT : entity work.rtl_compare_operation
	generic map
	(
		HAND_MADE => false,
		DATA_WIDTH => 6
	)
	port map
	(
		value1 => count,
		value2 => count_mask,
		greater => open,
		greater_equal => open,
		less => open,
		less_equal => open,
		equal => count_match,
		different => open
	);

	-- FIRST MATCH
	COMPARE_OPCODE : entity work.rtl_compare_operation
	generic map
	(
		HAND_MADE => false,
		DATA_WIDTH => 8
	)
	port map
	(
		value1 => opcode,
		value2 => opcode_mask,
		greater => open,
		greater_equal => open,
		less => open,
		less_equal => open,
		equal => op_match_raw,
		different => open
	);
	-- END OF FIRST MATCH

	-- SECOND MATCH
	COMPARE_OPCODE2 : entity work.rtl_compare_operation
	generic map
	(
		HAND_MADE => false,
		DATA_WIDTH => 8
	)
	port map
	(
		value1 => opcode,
		value2 => opcode_mask2,
		greater => open,
		greater_equal => open,
		less => open,
		less_equal => open,
		equal => op_match_raw2,
		different => open
	);
	-- END OF SECOND MATCH

	op_match_raw_reduced <= op_match_raw or op_match_raw2;
	valid_config_reduced <= valid_config or valid_config2;

	trigger_capture <= op_match_raw_reduced and valid_config_reduced and count_match;
	opcode_match <= op_match_raw_reduced;

end rtl;


