--------------------------------------------------------------------------------
--
-- Module Name: sim_statistics_framework
-- Create Date: 09/05/2019 (MM/DD/YYYY)
--
-- Company: ittChip - Unisinos
-- Engineer: Vinícius Gabriel LINDEN
-- 
-- Description: Stimulae statistics_framework
-- 
--------------------------------------------------------------------------------
-- Recommended span of 300 ns for period of 10 ns
-- TODO:


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library UNISIM;
use UNISIM.VComponents.all;

--library STD;
--use STD.TEXTIO.ALL;

library work;
--use work.common_pkg.all;
use work.kratus_pkg.all;

entity sim_statistics_framework is
end sim_statistics_framework;


architecture Behavioral of sim_statistics_framework is

	-- DO NOT DELETE
	constant period			: time := 5 ns;
	constant hp				: time := period/2; -- half period

	signal index			: natural := 1; -- index for redability
	signal clk				: std_logic := '1'; -- system base clock

	procedure wait_clk(constant T : in natural) is
	begin
		wait for t*period;
	end procedure;
	-- DELETION RESTRICTION ENDED

	-- UUT
	constant SUBMISSION_CMD : boolean := true;
	signal stream			: std_logic_vector(7 downto 0);
	signal valid			: std_logic := '0';
	signal ready			: std_logic;
	signal stream_delayed	: std_logic_vector(7 downto 0);
	signal cmd_id 			: std_logic_vector(15 downto 0);
	signal opcode			: std_logic_vector(7 downto 0);
	signal count			: std_logic_vector(5 downto 0);
	signal last				: std_logic;
	signal rst				: std_logic := '1';
	signal error			: std_logic := '0';
	-- END OF UUT

	-- SIMULATION
	signal test_case		: string(1 to 20) := "waiting             ";
	signal data				: unsigned(7 downto 0) := (others => '0');
	-- END OF SIMULATION

begin

	UUT : entity work.rtl_statistics_framework
	generic map
	(
		SUBMISSION_CMD => SUBMISSION_CMD,
		CLOCK_EDGE => '1'
	)
	port map
	(
		stream => stream,
		valid => valid,
		ready => ready,
		stream_delayed => stream_delayed,
		opcode => opcode,
		cmd_id => cmd_id,
		count => count,
		last => last,
		error => error,
		rst => rst,
		clk => clk
	);

	-- DO NOT DELETE
	clk <= not clk after hp;
	index <= index + 1 after period;
	-- DELETION RESTRICTION ENDED

	-- SEQUENTIAL DATA GENERATOR
	data <= data + 1 after period;
	stream <= std_logic_vector(data);
	-- END OF SEQUENTIAL DATA GENERATOR

	process
	begin
		wait_clk(2);
		rst <= '0';
		wait_clk(2);

		test_case <= "clear               ";
		wait_clk(10);

		test_case <= "testing stream      ";
		for i in 0 to 5 loop
			valid <= '1';
			wait_clk(2);
			valid <= '0';
			wait_clk(65);
		end loop;

		test_case <= "clear               ";
		wait_clk(10);

		test_case <= "simulation ended    ";
		wait;
	end process;

end Behavioral;


