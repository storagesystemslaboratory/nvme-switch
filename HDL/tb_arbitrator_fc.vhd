--------------------------------------------------------------------------------
--
-- Module Name: tb_arbitrator_fc
-- Create Date: 01/07/2020 (MM/DD/YYYY)
--
-- Company: ittChip - Unisinos
-- Engineer: Vinícius Gabriel LINDEN
-- 
-- Description: Testbench arbitrator_fc
-- 
--------------------------------------------------------------------------------
-- Recommended span of 300 ns for period of 10 ns
-- TODO:


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library UNISIM;
use UNISIM.VComponents.all;

library STD;
use STD.TEXTIO.ALL;

library work;
use work.common_pkg.all;
use work.kratus_pkg.all;
use work.NVMe_pkg.all;
use work.std_cmds_pkg.all;

entity tb_arbitrator_fc is
end tb_arbitrator_fc;


architecture Behavioral of tb_arbitrator_fc is

	-- DO NOT DELETE
	constant period				: time := 5 ns;
	constant hp					: time := period/2; -- half period

	signal index				: natural := 1; -- index for redability
	signal clk					: std_logic := '1'; -- system base clock

	procedure wait_clk(constant T : in natural) is
	begin
		wait for t*period;
	end procedure;
	-- DELETION RESTRICTION ENDED

	-- TESTBENCH FILE
	constant WIDTH_INPUT		: integer := 8; -- number of input characters in a line
	file file_input				: text;
	signal read_input			: std_logic_vector(WIDTH_INPUT-1 downto 0);
	signal enable_input			: std_logic := '0'; -- control signal to enable input read
	-- END OF TESTBENCH FILE

	-- INPUT FILE RELATED
	constant F_READ1			: string(1 to 16) := "read1_______.txt";
	constant F_READ2			: string(1 to 16) := "read2_______.txt";
	constant F_FLUSH			: string(1 to 16) := "flush_______.txt";
	constant F_WRITE			: string(1 to 16) := "write_______.txt";
	constant F_PROP1			: string(1 to 16) := "NVMe_SQ_B_01.txt";
	constant F_PROP2			: string(1 to 16) := "NVMe_SQ_B_02.txt";
	signal file_title			: string(1 to 16) := F_READ1; -- file currently being process
	-- END OF INPUT FILE RELATED

	-- UUT
	constant AC_CONFIGS		: natural := 1;
	constant CLOCK_EDGE		: std_logic := '1';
	signal config			: std_logic_vector(7 downto 0) := (others => '0');
	signal config_updated	: std_logic := '0';
	signal stream			: std_logic_vector(7 downto 0) := (others => '0');
	signal valid			: std_logic := '0';
	signal ready			: std_logic;
	signal field			: std_logic_vector_64(AC_CONFIGS-1 downto 0);
	signal opcode			: std_logic_vector(7 downto 0);
	signal cmd_id			: std_logic_vector(15 downto 0);
	signal evaluate			: std_logic;
	signal rst				: std_logic := '1';
	signal error			: std_logic;
	-- END OF UUT

	-- SIMULATION
	signal test_case		: string(1 to 20) := "waiting             ";
	signal read_file		: std_logic := '0'; -- on/off control for input file and handshake control
	signal config_edge		: std_logic_vector(7 downto 0) := (others => '0'); -- edge detection
	-- END OF SIMULATION

begin

	UUT : entity work.rtl_arbitrator_fc
	generic map
	(
		AC_CONFIGS => AC_CONFIGS,
		CLOCK_EDGE => CLOCK_EDGE
	)
	port map
	(
		config => config,
		config_updated => config_updated,
		stream => stream,
		valid => valid,
		ready => ready,
		field => field,
		opcode => opcode,
		cmd_id => cmd_id,
		evaluate => evaluate,
		rst => rst,
		error => error,
		clk => clk
	);

	-- DO NOT DELETE
	clk <= not clk after hp;
	index <= index + 1 after period;
	-- DELETION RESTRICTION ENDED

	-- TESTBENCH FILE
	READ_PROCESS : process -- read
		variable line_input               : line;
		variable bit_input                : bit_vector(WIDTH_INPUT-1 downto 0);
	begin
		wait until enable_input'event and enable_input = '1';
		file_open(file_input,file_title,read_mode);
		while not endfile(file_input) loop
			if enable_input = '1' then
				readline(file_input,line_input);
				read(line_input,bit_input);
				read_input <= to_stdlogicvector(bit_input);
			end if;
			wait for period;
		end loop;
		file_close(file_input);
	end process READ_PROCESS;
	-- END OF TESTBENCH FILE

	-- INPUT DATA CONTROL
	stream <= read_input;
	-- END OF INPUT DATA CONTROL

	-- READ FILE AUTOMATIC CONTROL
	process
	begin
		enable_input <= '0';
		wait until read_file'event;
		enable_input <= '1';
		wait_clk(64);
	end process;
	-- END OF READ FILE AUTOMATIC CONTROL

	-- RECEIVING CYCLE CONTROL
	process
	begin
		valid <= '0';
		wait until read_file'event;
		valid <= '1';
		wait_clk(1);
	end process;
	-- END OF RECEIVING CYCLE CONTROL

	-- CONFIG_UPDATED AUTOMATIC CONTROL
	config_edge <= config after period;
	config_updated  <= '1' when config_edge /= config else '0';
	-- END OF CONFIG_UPDATED AUTOMATIC CONTROL

	process
	begin
		report "simulation started";
		wait_clk(2);
		rst <= '0';
		wait_clk(2);

		test_case <= "clear               ";
		wait_clk(10);

		test_case <= "ac_test read1       ";
		wait_clk(1);
		report test_case;
		file_title <= F_READ1;
		config <= AC_TEST;
		wait_clk(5);
		toggle(read_file);
		wait until ready = '1';
		wait_clk(2);
		assert field(0) = STD_CMD_READ1_SLBA report "field" severity warning;
		assert error = '0' report "error" severity warning;

		test_case <= "clear               ";
		wait_clk(10);

		test_case <= "simulation ended    ";
		report "simulation ended";
		wait;
	end process;

end Behavioral;


