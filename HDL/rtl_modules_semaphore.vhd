--------------------------------------------------------------------------------
--
-- Module Name: rtl_modules_semaphore
-- Create Date: 09/06/2019 (MM/DD/YYYY)
-- Revision: 1
--
-- Company: ittChip - Unisinos
-- Engineer: Vinícius Gabriel LINDEN
-- 
-- Description: Real-time logic for the mux/demux control
--              Each channel has a "ask" and "lock" signals
--              "lock" means that the module is selected
--              "ask" means that the module is asking to be selected
--              When ask = lock = 1, the module will not be unselected until
--              "ask" is released
--              When ask = 0, lock = 1, the module is selected but it may
--              change without notice
-- 
--------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

library UNISIM;
use UNISIM.VComponents.all;

entity rtl_modules_semaphore is
	generic
	(
		CLOCK_EDGE 			: std_logic := '1' -- '1' for rising, '0' for falling
	);
	port
	(
		prioritary_ask 		: in std_logic; -- prioritary asks for lock
		prioritary_lock 	: out std_logic; -- prioritary is locked

		secondary_ask 		: in std_logic; -- secondary asks for lock
		secondary_lock 		: out std_logic; -- secondary is locked

		ctrl_sel 			: out std_logic; -- control select output

		rst 				: in std_logic;
		clk 				: in std_logic
	);
end rtl_modules_semaphore;

architecture rtl of rtl_modules_semaphore is

	type lock_state is (IDLE, -- idle, no request
						FIRST_LOCKED, -- prioritary has been locked
						SECOND_LOCKED -- secondary has been locked
						);

	signal state_lock		: lock_state;

begin

	process(rst,clk)
	begin
		if rst = '1' then
			state_lock <= IDLE;
		elsif clk'event and clk = CLOCK_EDGE then
			case state_lock is
				when IDLE =>
					if prioritary_ask = '1' then -- prioritary comes before secondary
						state_lock <= FIRST_LOCKED;
					elsif secondary_ask = '1' then
						state_lock <= SECOND_LOCKED;
					end if;
				when FIRST_LOCKED =>
					if prioritary_ask = '0' then
						if secondary_ask = '1' then
							state_lock <= SECOND_LOCKED;
						else
							state_lock <= IDLE;
						end if;
					end if;
				when SECOND_LOCKED =>
					if secondary_ask = '0' then
						if prioritary_ask = '1' then
							state_lock <= FIRST_LOCKED;
						else
							state_lock <= IDLE;
						end if;
					end if;
				when others =>
					state_lock <= IDLE;
			end case;
		end if;
	end process;
	ctrl_sel <= '1' when state_lock = SECOND_LOCKED else '0';
	prioritary_lock <= '1' when state_lock /= SECOND_LOCKED else '0';
	secondary_lock <= '1' when state_lock = SECOND_LOCKED else '0';

end rtl;


