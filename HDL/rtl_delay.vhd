--------------------------------------------------------------------------------
--
-- Module Name: rtl_delay
-- Create Date: XX/XX/XXXX (MM/DD/YYYY)
-- Revision: X
--
-- Company: ittChip-Unisinos
-- Engineer: Rodrigo Marques de Figueiredo
--
-- Description: Real-time logic for controlled delay
--
--------------------------------------------------------------------------------
-- TODO:

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
--use IEEE.NUMERIC_STD.ALL;

library UNISIM;
use UNISIM.VComponents.all;

library work;
use work.log_pkg.all;

entity rtl_delay is
	generic
	(
		DELAY			: integer := 0; -- delay clock cycles
		DATA_WIDTH		: integer := 2; -- data width
		REG_DELAY		: boolean := false; -- use flipflops D for delaying
		INVERT_OUTPUT	: std_logic := '0'; -- invert output
		CLOCK_EDGE		: std_logic := '1' -- '1' for rising, '0' for falling
	);
	port
	(
		d				: in std_logic_vector(DATA_WIDTH-1 downto 0); -- input
		q				: out std_logic_vector(DATA_WIDTH-1 downto 0); -- output
		clk_en			: in std_logic; -- clock enable
		clk				: in std_logic
	);
end rtl_delay;

architecture rtl of rtl_delay is

	type data_type is array (DELAY downto 0) of std_logic_vector(DATA_WIDTH-1 downto 0);
	signal ff_dout		: data_type; -- FFD output
	signal d_i			: std_logic_vector(DATA_WIDTH-1 downto 0);
	signal dontcare		: std_logic_vector(numbits(DELAY)-1 downto 0); -- not used

begin

	d_i <= not d when INVERT_OUTPUT = '1' else d;

	ZERO_DELAY : if DELAY = 0 generate -- simplifies instantiation
		q <= d_i;
	end generate ZERO_DELAY;

	UNITARY_DELAY : if DELAY = 1 generate
		SINGLE_FFD : entity work.rtl_ffd
		generic map
		(
			DATA_WIDTH => DATA_WIDTH,
			CLOCK_EDGE => CLOCK_EDGE
		)
		port map
		(
			din => d_i,
			dout => q,
			aclr => '0',
			apre => '0',
			clk_en => clk_en,
			clk => clk
		);
	end generate UNITARY_DELAY;

	NON_UNITARY_DELAY : if DELAY > 1 generate
		DONT_USE_REGISTERS : if REG_DELAY = false generate
			sr: entity work.rtl_sr
			generic map
			(
				MODE => "STATIC",
				OUTPUT_TYPE => "SYNC",
				ADDR_SEL => DELAY-2,
				DATA_WIDTH => DATA_WIDTH,
				ADDR_WIDTH => numbits(DELAY-1),
				CLOCK_EDGE => CLOCK_EDGE
			)
			port map
			(
				q => q,
				d => d_i,
				a => dontcare(numbits(DELAY-1)-1 downto 0),
				clk_en => clk_en,
				clk => clk
			);
		end generate DONT_USE_REGISTERS;

		USE_REGISTERS : if REG_DELAY = true generate
			DELAY_COUNT : for i in 0 to DELAY-1 generate
				FIRST : if i = 0 generate
					FIRST_FF : entity work.rtl_ffd
					generic map
					(
						DATA_WIDTH => DATA_WIDTH,
						CLOCK_EDGE => CLOCK_EDGE
					)
					port map
					(
						din => d_i,
						dout => ff_dout(0),
						aclr => '0',
						apre => '0',
						clk_en => clk_en,
						clk => clk
					);
				end generate FIRST;

				MIDDLE : if i /= 0 and i /= DELAY-1 generate
					MIDDLE_FF : entity work.rtl_ffd
					generic map
					(
						DATA_WIDTH => DATA_WIDTH,
						CLOCK_EDGE => CLOCK_EDGE
					)
					port map
					(
						din => ff_dout(i-1),
						dout => ff_dout(i),
						aclr => '0',
						apre => '0',
						clk_en => clk_en,
						clk => clk
					);
				end generate MIDDLE;

				LAST : if i = DELAY-1 generate
					LAST_FF : entity work.rtl_ffd
					generic map
					(
						DATA_WIDTH => DATA_WIDTH,
						CLOCK_EDGE => CLOCK_EDGE
					)
					port map
					(
						din => ff_dout(i-1),
						dout => q,
						aclr => '0',
						apre => '0',
						clk_en => clk_en,
						clk => clk
					);
				end generate LAST;
			end generate DELAY_COUNT;
		end generate USE_REGISTERS;
	end generate NON_UNITARY_DELAY;

end rtl;
