--------------------------------------------------------------------------------
--
-- Module Name: rtl_reduce_operations
-- Create Date: 09/11/2019 (MM/DD/YYYY)
-- Revision: 0
--
-- Company: ittChip - Unisinos
-- Engineer: Vinícius Gabriel LINDEN
-- 
-- Description: Real-time logic for the bitwise reduce operations.
--              For gates like NAND, which has only one HIGH combination, just
--              use the "when" statement.
-- 
--------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

library UNISIM;
use UNISIM.VComponents.all;

entity rtl_reduce_operations is
	generic (
				DATA_WIDTH          : integer := 8; -- width of data [bit]
				OPERATION           : integer := 1 -- operation: 0 = or, 1 = and, 2 = xor
			);
	port (
			 data                : in std_logic_vector(DATA_WIDTH-1 downto 0);
			 result              : out std_logic
		 );
end rtl_reduce_operations;

architecture rtl of rtl_reduce_operations is

	signal operation_vec    : std_logic_vector(DATA_WIDTH-2 downto 0);

begin

	IS_VECTOR : if DATA_WIDTH /= 1 generate

		OR_OPERATION : if OPERATION = 0 generate
			operation_vec(0) <= data(0) or data(1);
			BITWISE : FOR i in 1 to operation_vec'left generate
				operation_vec(i) <= operation_vec(i-1) or data(i+1);
			end generate BITWISE;
		end generate OR_OPERATION;

		AND_OPERATION : if OPERATION = 1 generate
			operation_vec(0) <= data(0) and data(1);
			BITWISE : FOR i in 1 to operation_vec'left generate
				operation_vec(i) <= operation_vec(i-1) and data(i+1);
			end generate BITWISE;
		end generate AND_OPERATION;

		XOR_OPERATION : if OPERATION = 2 generate
			operation_vec(0) <= data(0) xor data(1);
			BITWISE : FOR i in 1 to operation_vec'left generate
				operation_vec(i) <= operation_vec(i-1) xor data(i+1);
			end generate BITWISE;
		end generate XOR_OPERATION;

		result <= operation_vec(DATA_WIDTH-2);

	end generate IS_VECTOR;

	IS_BIT : if DATA_WIDTH = 1 generate

		result <= data(0);

	end generate IS_BIT;

end rtl;


