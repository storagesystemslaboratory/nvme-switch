--------------------------------------------------------------------------------
--
-- Module Name: rtl_arbitrator
-- Create Date: 09/30/2019 (MM/DD/YYYY)
-- Revision: 0
--
-- Company: ittChip - Unisinos
-- Engineer: Vinícius Gabriel LINDEN
-- 
-- Description: Real-time logic arbitrator
-- 
--------------------------------------------------------------------------------
-- TODO:

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

library UNISIM;
use UNISIM.VComponents.all;

library work;
use work.common_pkg.all;
use work.NVMe_pkg.all;

entity rtl_arbitrator is
	generic
	(
		CLEAR_ON_BOTH		: boolean := true; -- true, clear is activated on both edge, if false only the CLOCK_EDGE is active
		CLOCK_EDGE			: std_logic := '1' -- '1' for rising, '0' for falling
	);
	port
	(
		-- CONFIGURATION REGISTER
		config				: in std_logic_vector(7 downto 0); -- configuration for arbitration
		config_updated		: in std_logic; -- '1' if new configuration has been sent

		-- NVMe FLOW
		stream				: in std_logic_vector(7 downto 0); -- byte data size
		valid				: in std_logic; -- sending new data stream
		ready				: out std_logic; -- do not send new stream, module is busy

		-- FIFO NVMe
		full				: in std_logic;
		empty				: in std_logic;
		unload				: out std_logic;
		unloaded			: in std_logic;
		processing			: in std_logic;
		number_pkgs			: in std_logic_vector(7 downto 0);

		-- FILTER
		cmd_id_filter		: out std_logic_vector(15 downto 0);
		submit				: out std_logic; -- submit to filter module
		response			: in std_logic; -- acknowledgement from filter module

		-- SELECTION FLOW
		generic_ask			: in std_logic; -- generic SSD asks for lock
		generic_lock		: out std_logic; -- generic SSD is locked
		generic_flow 		: in std_logic; -- generic SSD is flow control
		host2ssd			: out std_logic; -- controlls the flow host->SSD

		-- OTHER
		error				: out std_logic;
		rst					: in std_logic;
		clk					: in std_logic
	);
end rtl_arbitrator;

architecture rtl of rtl_arbitrator is

	signal opcode				: std_logic_vector(7 downto 0);
	signal field				: std_logic_vector(63 downto 0);
	signal cmd_id				: std_logic_vector(15 downto 0);
	signal evaluate				: std_logic;
	signal direction			: std_logic;
	signal new_available		: std_logic;
	signal next_direction		: std_logic;
	signal adm_ask				: std_logic;
	signal adm_lock				: std_logic;
	signal adm_flow				: std_logic;
	-- ERRORS
	signal error_fc				: std_logic;
	signal error_assertion		: std_logic;
	signal error_adm			: std_logic;

begin

	error <= error_assertion or error_adm or error_fc;


	ARBITRATOR_FC : entity work.rtl_arbitrator_fc
	generic map
	(
		AC_CONFIGS => 1,
		CLOCK_EDGE => CLOCK_EDGE
	)
	port map
	(
		config => config,
		config_updated => config_updated,
		stream => stream,
		valid => valid,
		ready => ready,
		field(0) => field,
		opcode => opcode,
		cmd_id => cmd_id,
		evaluate => evaluate,
		rst => rst,
		error => error_fc,
		clk => clk
	);

	ASSERTION : entity work.rtl_assertion
	generic map
	(
		CLOCK_EDGE => CLOCK_EDGE
	)
	port map
	(
		config => config,
		config_updated => config_updated,
		opcode => opcode,
		evaluate => evaluate,
		field => field,
		cmd_id => cmd_id,
		direction => direction,
		cmd_id_adm => cmd_id_filter,
		new_available => new_available,
		next_direction => next_direction,
		rst => rst,
		error => error_assertion,
		clk => clk
	);

	ADMINISTRATOR : entity work.rtl_arbitrator_adm
	generic map
	(
		CLOCK_EDGE => CLOCK_EDGE
	)
	port map
	(
		direction => direction,
		new_available => new_available,
		next_direction => next_direction,
		adm_ask => adm_ask,
		adm_lock => adm_lock,
		adm_flow => adm_flow,
		full => full,
		empty => empty,
		unload => unload,
		unloaded => unloaded,
		processing => processing,
		number_pkgs => number_pkgs,
		submit => submit,
		response => response,
		error => error_adm,
		rst => rst,
		clk => clk
	);

	MUX_DEMUX : entity work.rtl_mux_demux_adm
	generic map
	(
		GENERIC_PRIORITARY => false,
		CLOCK_EDGE => CLOCK_EDGE
	)
	port map
	(
		arb_ask => adm_ask,
		arb_lock => adm_lock,
		arb_flow => adm_flow,
		generic_ask => generic_ask,
		generic_lock => generic_lock,
		generic_flow => generic_flow,
		host2ssd => host2ssd,
		rst => rst,
		clk => clk
	);

end rtl;


