--------------------------------------------------------------------------------
--
-- Module Name: sim_reduce_operations
-- Create Date: 09/11/2019 (MM/DD/YYYY)
--
-- Company: ittChip - Unisinos
-- Engineer: Vinícius Gabriel LINDEN
-- 
-- Description: Stimulae for reduce operations
-- 
--------------------------------------------------------------------------------
-- Recommended span of 300 ns for period of 10 ns

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.STD_LOGIC_MISC.ALL;

library UNISIM;
use UNISIM.VComponents.all;

--library STD;
--use STD.TEXTIO.ALL;

library work;
--use work.common_pkg.all;
--use work.kratus_pkg.all;

entity sim_reduce_operations is
	end sim_reduce_operations;


architecture Behavioral of sim_reduce_operations is

	-- DO NOT DELETE
	constant period                 : time := 5 ns;
	constant hp                     : time := period/2; -- half period

	signal index                    : natural := 1; -- index for redability
	signal clk                      : std_logic := '1'; -- system base clock

	procedure wait_clk(constant T : in natural) is
	begin
		wait for t*period;
	end procedure;
	-- DELETION RESTRICTION ENDED

	-- UUT
	constant DATA_WIDTH             : integer := 8;
	signal data                     : std_logic_vector(DATA_WIDTH-1 downto 0);
	signal or_result                : std_logic;
	signal or_proof                 : std_logic;
	signal and_result               : std_logic;
	signal and_proof                : std_logic;
	signal xor_result               : std_logic;
	signal xor_proof                : std_logic;
-- END OF UUT

begin

	OR_RED : entity work.rtl_reduce_operations
	generic map (
					DATA_WIDTH => DATA_WIDTH,
					OPERATION => 0
				)
	port map (
				 data => data,
				 result => or_result
			 );
	or_proof <= or_reduce(data);

	AND_RED : entity work.rtl_reduce_operations
	generic map (
					DATA_WIDTH => DATA_WIDTH,
					OPERATION => 1
				)
	port map (
				 data => data,
				 result => and_result
			 );
	and_proof <= and_reduce(data);

	XOR_RED : entity work.rtl_reduce_operations
	generic map (
					DATA_WIDTH => DATA_WIDTH,
					OPERATION => 2
				)
	port map (
				 data => data,
				 result => xor_result
			 );
	xor_proof <= xor_reduce(data);

	-- DO NOT DELETE
	clk <= not clk after hp;
	index <= index + 1 after period;
	-- DELETION RESTRICTION ENDED

	process
	begin
		for i in 0 to 2**DATA_WIDTH-1 loop
			data <= std_logic_vector(to_unsigned(i,DATA_WIDTH));
			wait_clk(1);
		end loop;
		wait;
	end process;

end Behavioral;


