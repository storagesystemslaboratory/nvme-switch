--------------------------------------------------------------------------------
--
-- Module Name: sim_arbitrator
-- Create Date: 09/30/2019 (MM/DD/YYYY)
--
-- Company: ittChip - Unisinos
-- Engineer: Vinícius Gabriel LINDEN
-- 
-- Description: Stimulae arbitrator
-- 
--------------------------------------------------------------------------------
-- Recommended span of 300 ns for period of 10 ns
-- TODO:


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
--use IEEE.NUMERIC_STD.ALL;

library UNISIM;
use UNISIM.VComponents.all;

library work;
--use work.common_pkg.all;
--use work.kratus_pkg.all;

entity sim_arbitrator is
end sim_arbitrator;


architecture Behavioral of sim_arbitrator is

	-- DO NOT DELETE
	constant period                 : time := 5 ns;
	constant hp                     : time := period/2; -- half period

	signal index                    : natural := 1; -- index for redability
	signal clk                      : std_logic := '1'; -- system base clock

	procedure wait_clk(constant T : in natural) is
	begin
		wait for t*period;
	end procedure;
	-- DELETION RESTRICTION ENDED

	-- UUT
	constant CLEAR_ON_BOTH          : boolean := true;
	constant CLOCK_EDGE             : std_logic := '1';
	signal config                   : std_logic_vector(7 downto 0) := (others => '0');
	signal config_updated           : std_logic := '0';
	signal stream              : std_logic_vector(7 downto 0) := (others => '0');
	signal valid                    : std_logic := '0';
	signal ready                    : std_logic;
	signal error                    : std_logic;
	signal rst                      : std_logic := '1';
	-- END OF UUT

	-- SIMULATION
	signal test_case                : string(1 to 20) := "waiting             ";
-- END OF SIMULATION

begin

	UUT : entity work.rtl_arbitrator
	generic map (
					CLEAR_ON_BOTH => CLEAR_ON_BOTH,
					CLOCK_EDGE => CLOCK_EDGE
				)
	port map (
				 config => config,
				 config_updated => config_updated,
				 stream => stream,
				 valid => valid,
				 ready => ready,
				 error => error,
				 rst => rst,
				 clk => clk
			 );

	-- DO NOT DELETE
	clk <= not clk after hp;
	index <= index + 1 after period;
	-- DELETION RESTRICTION ENDED

	process
	begin
		wait_clk(2);
		rst <= '0';
		wait_clk(2);

		test_case <= "clear               ";
		wait_clk(10);

		test_case <= "simulation ended    ";
		wait;
	end process;

end Behavioral;


