--------------------------------------------------------------------------------
--
-- Module Name: tb_sub_field_capture
-- Create Date: 27/01/2020 (MM/DD/YYYY)
--
-- Company: ittChip - Unisinos
-- Engineer: Vinícius Gabriel LINDEN
-- 
-- Description: Testbench for the field capture module
--
-- Observation: There are N+1 sub field capture modules being tested (N = # config)
--				One of each configuration, plus one config_fc with different clear
--				option. Therefore, the label of fc is "_clr" & "_noclr".
-- 
--------------------------------------------------------------------------------
-- Recommended span of 300 ns for period of 10 ns

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library UNISIM;
use UNISIM.VComponents.all;

library STD;
use STD.TEXTIO.ALL;

library work;
use work.common_pkg.all;
use work.kratus_pkg.all;
use work.NVMe_pkg.all;

entity tb_sub_field_capture is
end tb_sub_field_capture;


architecture Behavioral of tb_sub_field_capture is

	-- DO NOT DELETE
	constant period                 : time := 5 ns;
	constant hp                     : time := period/2; -- half period

	signal index                    : natural := 1; -- index for redability
	signal clk                      : std_logic := '1'; -- system base clock

	procedure wait_clk(constant T : in natural) is
	begin
		wait for t*period;
	end procedure;
	-- DELETION RESTRICTION ENDED

	-- TESTBENCH FILE
	constant WIDTH_INPUT		: integer := 8; -- number of input characters in a line
	file file_input				: text;
	signal read_input			: std_logic_vector(WIDTH_INPUT-1 downto 0);
	signal enable_input			: std_logic := '0'; -- control signal to enable input read
	-- END OF TESTBENCH FILE

	-- INPUT FILE RELATED
	constant F_READ1			: string(1 to 16) := "read1_______.txt";
	constant F_READ2			: string(1 to 16) := "read2_______.txt";
	constant F_FLUSH			: string(1 to 16) := "flush_______.txt";
	constant F_WRITE			: string(1 to 16) := "write_______.txt";
	constant F_PROP1			: string(1 to 16) := "NVMe_SQ_B_01.txt";
	constant F_PROP2			: string(1 to 16) := "NVMe_SQ_B_02.txt";
	signal file_title			: string(1 to 16) := F_READ1; -- file currently being process
	-- END OF INPUT FILE RELATED

	-- UUT
	--constant USE_CONFIG		: natural := 1;
	--constant EN_CLEAR		: boolean := true;
	constant CLEAR_ON_BOTH	: boolean := true;
	constant CLOCK_EDGE		: std_logic := '1';
	signal config_fc		: std_logic_vector(7 downto 0) := (others => '0');
	signal config_oc		: std_logic_vector(7 downto 0) := (others => '0');
	signal config_updated_fc: std_logic := '0';
	signal config_updated_oc: std_logic := '0';
	signal stream_delayed	: std_logic_vector(7 downto 0);
	signal opcode			: std_logic_vector(7 downto 0);
	signal count			: std_logic_vector(5 downto 0);
	signal last_delayed		: std_logic;
	signal opcode_match_fc	: std_logic;
	signal opcode_match_oc	: std_logic;
	signal new_data_clr		: std_logic;
	signal new_data_noclr	: std_logic;
	signal new_data_oc		: std_logic;
	signal clear			: std_logic := '0';
	signal field_clr		: std_logic_vector(63 downto 0);
	signal field_noclr		: std_logic_vector(63 downto 0);
	signal field_oc			: std_logic_vector(63 downto 0);
	signal rst				: std_logic := '1';
	signal error_clr		: std_logic;
	signal error_noclr		: std_logic;
	signal error_oc			: std_logic;
	-- END OF UUT

	-- FRAMEWORK
	signal stream			: std_logic_vector(7 downto 0);
	signal valid			: std_logic := '0';
	signal ready			: std_logic;
	signal last				: std_logic;
	signal error_fw			: std_logic;
	-- END OF FRAMEWORK

	-- SIMULATION
	signal test_case		: string(1 to 20) := "waiting             ";
	signal read_file		: std_logic := '0'; -- on/off control for input file and handshake control
	signal config_fc_edge		: std_logic_vector(7 downto 0) := (others => '0'); -- edge detection
	signal config_oc_edge		: std_logic_vector(7 downto 0) := (others => '0'); -- edge detection
	-- END OF SIMULATION

begin

	UUT_CLR_EN : entity work.rtl_sub_field_capture
	generic map
	(
		USE_CONFIG => 1,
		EN_CLEAR => true,
		CLEAR_ON_BOTH => CLEAR_ON_BOTH,
		CLOCK_EDGE => CLOCK_EDGE
	)
	port map
	(
		config => config_fc,
		config_updated => config_updated_fc,
		stream_delayed => stream_delayed,
		opcode => opcode,
		count => count,
		last_delayed => last_delayed,
		opcode_match => opcode_match_fc,
		new_data => new_data_clr,
		clear => clear,
		field => field_clr,
		rst => rst,
		error => error_clr,
		clk => clk
	);

	UUT_CLR_DIS : entity work.rtl_sub_field_capture
	generic map
	(
		USE_CONFIG => 1,
		EN_CLEAR => false,
		CLEAR_ON_BOTH => CLEAR_ON_BOTH,
		CLOCK_EDGE => CLOCK_EDGE
	)
	port map
	(
		config => config_fc,
		config_updated => config_updated_fc,
		stream_delayed => stream_delayed,
		opcode => opcode,
		count => count,
		last_delayed => last_delayed,
		opcode_match => open,
		new_data => new_data_noclr,
		clear => clear,
		field => field_noclr,
		rst => rst,
		error => error_noclr,
		clk => clk
	);

	UUT_OC : entity work.rtl_sub_field_capture
	generic map
	(
		USE_CONFIG => 2,
		EN_CLEAR => false,
		CLEAR_ON_BOTH => CLEAR_ON_BOTH,
		CLOCK_EDGE => CLOCK_EDGE
	)
	port map
	(
		config => config_oc,
		config_updated => config_updated_oc,
		stream_delayed => stream_delayed,
		opcode => opcode,
		count => count,
		last_delayed => last_delayed,
		opcode_match => opcode_match_oc,
		new_data => new_data_oc,
		clear => clear,
		field => field_oc,
		rst => rst,
		error => error_oc,
		clk => clk
	);

	FRAMEWORK : entity work.rtl_statistics_framework 
	generic map
	(
		SUBMISSION_CMD => true,
		CLOCK_EDGE => CLOCK_EDGE
	)
	port map
	(
		stream => stream,
		valid => valid,
		ready => ready,
		stream_delayed => stream_delayed,
		opcode => opcode,
		count => count,
		last_delayed => last_delayed,
		cmd_id => open,
		last => last,
		error => error_fw,
		rst => rst,
		clk => clk
	);

	-- DO NOT DELETE
	clk <= not clk after hp;
	index <= index + 1 after period;
	-- DELETION RESTRICTION ENDED

	-- TESTBENCH FILE
	READ_PROCESS : process -- read
		variable line_input               : line;
		variable bit_input                : bit_vector(WIDTH_INPUT-1 downto 0);
	begin
		wait until enable_input'event and enable_input = '1';
		file_open(file_input,file_title,read_mode);
		while not endfile(file_input) loop
			if enable_input = '1' then
				readline(file_input,line_input);
				read(line_input,bit_input);
				read_input <= to_stdlogicvector(bit_input);
			end if;
			wait for period;
		end loop;
		file_close(file_input);
	end process READ_PROCESS;
	-- END OF TESTBENCH FILE

	-- INPUT DATA CONTROL
	stream <= read_input;
	-- END OF INPUT DATA CONTROL

	-- READ FILE AUTOMATIC CONTROL
	process
	begin
		enable_input <= '0';
		wait until read_file'event;
		enable_input <= '1';
		wait_clk(64);
	end process;
	-- END OF READ FILE AUTOMATIC CONTROL

	-- RECEIVING CYCLE CONTROL
	process
	begin
		valid <= '0';
		wait until read_file'event;
		valid <= '1';
		wait_clk(1);
	end process;
	-- END OF RECEIVING CYCLE CONTROL

	-- CONFIG_UPDATED AUTOMATIC CONTROL
	config_fc_edge <= config_fc after period;
	config_updated_fc  <= '1' when config_fc_edge /= config_fc else '0';

	config_oc_edge <= config_oc after period;
	config_updated_oc  <= '1' when config_oc_edge /= config_oc else '0';
	-- END OF CONFIG_UPDATED AUTOMATIC CONTROL

	process
	begin
		report "simulation started";
		wait_clk(1);
		rst <= '0';
		wait_clk(1);

		test_case <= "clearing            ";
		wait_clk(10);

		test_case <= "read1 f_lba o_wr    ";
		wait_clk(1);
		report test_case;
		file_title <= F_READ1;
		config_fc <= FC_SLBA;
		config_oc <= OC_WRITE;
		wait_clk(5);
		toggle(read_file);
		wait until ready = '1';
		wait_clk(1);
		assert error = '0' report "error" severity warning;
		toggle(clear);
		wait_clk(1);
		assert field_fc = STD_CMD_READ1_SLBA report "field_fc" severity warning;
		assert field_oc = STD_CMD_WRITE_SLBA report "field_oc" severity warning;
		assert error = '0' report "error" severity warning;

		test_case <= "clearing            ";
		wait_clk(10);

		test_case <= "2xrd2 f_dsm o_rd    ";
		wait_clk(1);
		report test_case;
		file_title <= F_READ2;
		config_fc <= FC_DSM;
		config_oc <= OC_READ;
		wait_clk(5);
		toggle(read_file);
		wait until ready = '1';
		wait_clk(1);
		toggle(clear);
		wait_clk(5);
		toggle(read_file);
		wait until ready = '1';
		wait_clk(1);
		toggle(clear);

		test_case <= "clearing            ";
		wait_clk(10);

		test_case <= "write f_dsm o_wr    ";
		wait_clk(1);
		report test_case;
		file_title <= F_WRITE;
		config_fc <= FC_DSM;
		config_oc <= OC_WRITE;
		wait_clk(5);
		toggle(read_file);
		wait until ready = '1';
		wait_clk(1);

		test_case <= "clearing            ";
		wait_clk(10);

		test_case <= "read1 slba no clr   ";
		wait_clk(1);
		report test_case;
		file_title <= F_READ1;
		config_fc <= FC_SLBA;
		wait_clk(5);
		toggle(read_file);
		wait until ready = '1';
		wait_clk(1);

		test_case <= "clearing            ";
		wait_clk(10);

		test_case <= "rd2 f_lba noclr o_wr";
		wait_clk(1);
		report test_case;
		file_title <= F_READ2;
		wait_clk(5);
		toggle(read_file);
		wait until ready = '1';
		wait_clk(1);

		test_case <= "clearing            ";
		wait_clk(10);

		test_case <= "reset               ";
		wait_clk(1);
		report test_case;
		rst <= '1';
		wait_clk(10);
		rst <= '0';

		test_case <= "clearing            ";
		wait_clk(10);

		test_case <= "rd1 f_test o_rd_rang";
		wait_clk(1);
		report test_case;
		file_title <= F_READ1;
		config_fc <= FC_TEST;
		config_oc <= OC_RD_RANGE;
		wait_clk(5);
		toggle(read_file);
		wait until ready = '1';
		wait_clk(10);
		toggle(clear);

		test_case <= "clearing            ";
		wait_clk(10);

		test_case <= "rd2 f_test o_rd_rang";
		wait_clk(1);
		report test_case;
		file_title <= F_READ2;
		wait_clk(5);
		toggle(read_file);
		wait until ready = '1';
		wait_clk(10);
		toggle(clear);

		test_case <= "clearing            ";
		wait_clk(10);

		test_case <= "wr f_test o_rd_rang ";
		wait_clk(1);
		report test_case;
		file_title <= F_WRITE;
		wait_clk(5);
		toggle(read_file);
		wait until ready = '1';
		wait_clk(10);
		toggle(clear);

		test_case <= "clearing            ";
		wait_clk(10);

		test_case <= "wr f_test o_rw_rang ";
		wait_clk(1);
		report test_case;
		file_title <= F_WRITE;
		config_oc <= OC_RW_RANGE;
		wait_clk(5);
		toggle(read_file);
		wait until ready = '1';
		wait_clk(10);
		toggle(clear);

		test_case <= "clearing            ";
		wait_clk(10);

		test_case <= "rd1 f_test o_rw_rang";
		wait_clk(1);
		report test_case;
		file_title <= F_READ1;
		wait_clk(5);
		toggle(read_file);
		wait until ready = '1';
		wait_clk(10);
		toggle(clear);

		test_case <= "clearing            ";
		wait_clk(10);

		wait_clk(64);
		test_case <= "simulation ended    ";
		report "simulation ended";
		wait;
	end process;

end Behavioral;


