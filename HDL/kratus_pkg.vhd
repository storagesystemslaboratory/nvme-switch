--------------------------------------------------------------------------------
--
-- Module Name: kratus_pkg
-- Create Date: 10/03/2018 (MM/DD/YYYY)
--
-- Company: TECAE - Unisinos
-- Engineer: Vinícius Gabriel LINDEN
-- 
-- Description: Commonly used functions for simulating Kratus project
-- 
--------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
--use IEEE.STD_LOGIC_unsigned.all;
--use IEEE.MATH_REAL.all;


package kratus_pkg is

	-- PROCEDURE: IncrementWrap
	-- Increment by one each call until WrapValue-1 value, then it goes back to 0
	-- Example: WrapValue = 3
	-- Counter: 0 1 2 0 1 2
	procedure IncrementWrap(signal   Counter    : inout integer;
							constant WrapValue  : in    integer);
	procedure IncrementWrap(signal Counter      : inout unsigned;
							constant WrapValue  : in integer);
	procedure IncrementWrap(signal Counter      : inout unsigned;
							constant WrapValue  : in unsigned);
	procedure IncrementWrap(signal Counter      : inout unsigned);
	--    procedure IncrementWrap(signal   Counter   : inout integer;
	--                             constant WrapValue : in    integer;
	--                             constant Enable    : in    boolean;
	--                             variable Wrapped   : out   boolean);

	-- PROCEDURE: IncrementWrapFull
	-- Increment by one each call until WrapValue value, then it goes back to 0
	procedure IncrementWrapFull(signal Counter     : inout integer;
								constant WrapValue : in integer);
	procedure IncrementWrapFull(signal Counter     : inout unsigned;
								constant WrapValue : in integer);
	procedure IncrementWrapFull(signal Counter     : inout unsigned;
								constant WrapValue : in unsigned);
	procedure IncrementWrapFull(signal Counter     : inout unsigned); -- from 0 to Counter'length

	-- PROCEDURE: IncrementSat
	-- Increment by one each call until SatValue-1, then it stays at SatValue-1
	-- Example: SatValue = 10
	-- Counter:   0 1 2 3 4 5 6 7 8 9 9
	-- Saturated: 0 0 0 0 0 0 0 0 0 1 1
	procedure IncrementSat(signal Counter       : inout integer;
						   signal Saturated     : out std_logic;
						   constant SatValue    : in    integer);
	procedure IncrementSat(signal Counter       : inout integer;
						   constant SatValue    : in    integer);
	procedure IncrementSat(signal Counter       : inout unsigned;
						   signal Saturated     : out std_logic;
						   constant SatValue    : in    integer);
	procedure IncrementSat(signal Counter       : inout unsigned;
						   constant SatValue    : in    integer);
	procedure IncrementSat(signal Counter       : inout unsigned;
						   constant SatValue    : in    unsigned);
	procedure IncrementSat(signal Counter       : inout unsigned);

	-- PROCEDURE: IncrementSatFull
	-- Increment by one each call until SatValue, then it stays at SatValue
	-- Example: SatValue = 9
	-- Counter:   0 1 2 3 4 5 6 7 8 9 9
	-- Saturated: 0 0 0 0 0 0 0 0 0 1 1
	procedure IncrementSatFull(signal Counter       : inout integer;
							   signal Saturated     : out std_logic;
							   constant SatValue    : in    integer);
	procedure IncrementSatFull(signal Counter       : inout integer;
							   constant SatValue    : in    integer);
	procedure IncrementSatFull(signal Counter       : inout unsigned;
							   signal Saturated     : out std_logic;
							   constant SatValue    : in    integer);
	procedure IncrementSatFull(signal Counter       : inout unsigned;
							   signal Saturated     : out std_logic;
							   constant SatValue    : in    unsigned);
	procedure IncrementSatFull(signal Counter       : inout unsigned;
							   constant SatValue    : in    integer);
	procedure IncrementSatFull(signal Counter       : inout unsigned;
							   constant SatValue    : in    unsigned);
	procedure IncrementSatFull(signal Counter       : inout unsigned); -- from 0 to Counter'length

	-- PROCEDURE: DecrementWrap
	-- Decrement by one each call until 0 value, then it goes back to WrapValue-1
	-- Example: WrapValue = 3
	-- Counter: 0 2 1 0 2 1 ...
	procedure DecrementWrap(signal   Counter    : inout integer;
							constant WrapValue  : in    integer);

	-- PROCEDURE: DecrementWrapFull
	-- Decrement by one each call until 0 value, then it goes back to WrapValue
	-- Example: WrapValue = 3
	-- Counter: 0 3 2 1 0 3 2 1 ...
	procedure DecrementWrapFull(signal   Counter    : inout integer;
								constant WrapValue  : in    integer);

	-- PROCEDURE: WrapMinMax
	-- Increment by one each call. The possible Counter range is Min to Max. When it reaches Max, it goes back to Min
	procedure WrapMinMax(signal Counter : inout integer;
						 constant Min   : in integer;
						 constant Max   : in integer);
	procedure WrapMinMax(signal Counter : inout unsigned;
						 constant Min   : in integer;
						 constant Max   : in integer);

	-- PROCEDURE: toggle
	procedure toggle(signal a : inout std_logic);
	procedure toggle(signal a : inout std_logic_vector);
	procedure toggle(signal a : inout boolean);

	-- FUNCTION: is_logic   
	function is_logic(signal slv : in std_logic_vector) return boolean;

	-- FUNCTION: min
	function min(signal a : in integer;
	signal b : in integer) return integer;

	-- FUNCTION: max
	function max(signal a : in integer;
	signal b : in integer) return integer;

	-- FUNCTION: number_of_zeros
	-- Counts the number of zero bits
	function number_of_zeros (x: std_logic_vector) return integer;

end kratus_pkg;


package body kratus_pkg is

	-- PROCEDURE: IncrementWrap
	procedure IncrementWrap(signal   Counter   : inout integer;
							constant WrapValue : in    integer) is
	begin
		if Counter = WrapValue - 1 then
			Counter <= 0;
		else
			Counter <= Counter + 1;
		end if;
	end procedure;

	procedure IncrementWrap(signal Counter      : inout unsigned;
							constant WrapValue  : in integer) is
		constant CNT_ZERO : unsigned(Counter'range) := (others => '0');
	begin
		if Counter = WrapValue-1 then
			Counter <= CNT_ZERO;
			--Counter <= (others => '0'); -- Runs on Vivado, but not in GHDL
		else
			Counter <= to_unsigned(to_integer(unsigned(Counter)) + 1, Counter'length);
		end if;
	end procedure;

	procedure IncrementWrap(signal Counter      : inout unsigned;
							constant WrapValue  : in unsigned) is
		constant CNT_ZERO : unsigned(Counter'range) := (others => '0');
	begin
		if Counter = WrapValue-1 then
			Counter <= CNT_ZERO;
			--Counter <= (others => '0'); -- Runs on Vivado, but not in GHDL
		else
			Counter <= Counter + 1;
		end if;
	end procedure;

	procedure IncrementWrap(signal Counter     : inout unsigned) is
							constant WrapValue : unsigned(Counter'range) := (others => '1');
		constant CNT_ZERO : unsigned(Counter'range) := (others => '0');
	begin
		if Counter = WrapValue-1 then
			counter <= Counter + 2;
		elsif Counter = WrapValue then
			Counter <= CNT_ZERO;
			--Counter <= (others => '0'); -- Runs on Vivado, but not in GHDL
		else
			Counter <= Counter + 1;
		end if;
	end procedure;

	--    procedure IncrementWrap(signal   Counter   : inout integer;
	--                            constant WrapValue : in    integer;
	--                            constant Enable    : in    boolean;
	--                            variable Wrapped   : out   boolean) is
	--    begin
	--        if Enable then
	--            if Counter = WrapValue - 1 then
	--                Wrapped := true;
	--                Counter <= 0;
	--            else
	--                Wrapped := false;
	--                Counter <= Counter + 1;
	--            end if;
	--        end if;
	--    end procedure;

	-- PROCEDURE: IncrementWrapFull
	procedure IncrementWrapFull(signal Counter     : inout integer;
								constant WrapValue : in integer) is
	begin
		if Counter = WrapValue then
			Counter <= 0;
		else
			Counter <= Counter + 1;
		end if;
	end procedure;

	procedure IncrementWrapFull(signal Counter     : inout unsigned;
								constant WrapValue : in integer) is
		constant CNT_ZERO : unsigned(Counter'range) := (others => '0');
	begin
		if Counter = WrapValue then
			Counter <= CNT_ZERO;
			--Counter <= (others => '0'); -- Runs on Vivado, but not in GHDL
		else
			Counter <= Counter + 1;
		end if;
	end procedure;

	procedure IncrementWrapFull(signal Counter     : inout unsigned;
								constant WrapValue : in unsigned) is
		constant CNT_ZERO : unsigned(Counter'range) := (others => '0');
	begin
		if Counter = WrapValue then
			Counter <= CNT_ZERO;
			--Counter <= (others => '0'); -- Runs on Vivado, but not in GHDL
		else
			Counter <= Counter + 1;
		end if;
	end procedure;

	procedure IncrementWrapFull(signal Counter     : inout unsigned) is
	begin
		Counter <= Counter + 1;
	end procedure;

	-- PROCEDURE: IncrementSat
	procedure IncrementSat(signal Counter       : inout integer;
						   signal Saturated     : out std_logic;
						   constant SatValue    : in integer) is
	begin
		if Counter = SatValue - 2 then
			Saturated <= '1';
			Counter <= Counter + 1;
		elsif Counter < SatValue - 1 then
			Saturated <= '0';
			Counter <= Counter + 1;
		else
			Saturated <= '1';
		end if;
	end procedure;

	procedure IncrementSat(signal Counter       : inout integer;
						   constant SatValue    : in integer) is
	begin
		if Counter < SatValue - 1 then
			Counter <= Counter + 1;
		end if;
	end procedure;

	procedure IncrementSat(signal Counter       : inout unsigned;
						   signal Saturated     : out std_logic;
						   constant SatValue    : in integer) is
	begin
		if Counter = SatValue - 2 then
			Saturated <= '1';
			Counter <= Counter + 1;
		elsif Counter < SatValue - 1 then
			Saturated <= '0';
			Counter <= Counter + 1;
		else
			Saturated <= '1';
		end if;
	end procedure;

	procedure IncrementSat(signal Counter       : inout unsigned;
						   constant SatValue    : in    integer) is
	begin
		if Counter < SatValue - 1 then
			Counter <= Counter + 1;
		end if;
	end procedure;

	procedure IncrementSat(signal Counter       : inout unsigned;
						   constant SatValue    : in    unsigned) is
	begin
		if Counter < SatValue - 1 then
			Counter <= Counter + 1;
		end if;
	end procedure;

	procedure IncrementSat(signal Counter       : inout unsigned) is
						   constant SatValue : unsigned(Counter'range) := (others => '1');
	begin
		if Counter < SatValue - 1 then
			Counter <= Counter + 1;
		end if;
	end procedure;

	-- PROCEDURE: IncrementSat
	procedure IncrementSatFull(signal Counter       : inout integer;
							   signal Saturated     : out std_logic;
							   constant SatValue    : in integer) is
	begin
		if Counter = SatValue - 1 then
			Saturated <= '1';
			Counter <= Counter + 1;
		elsif Counter < SatValue then
			Saturated <= '0';
			Counter <= Counter + 1;
		else
			Saturated <= '1';
		end if;
	end procedure;

	procedure IncrementSatFull(signal Counter       : inout integer;
							   constant SatValue    : in integer) is
	begin
		if Counter < SatValue then
			Counter <= Counter + 1;
		end if;
	end procedure;

	procedure IncrementSatFull(signal Counter       : inout unsigned;
							   signal Saturated     : out std_logic;
							   constant SatValue    : in integer) is
	begin
		if Counter = SatValue - 1 then
			Saturated <= '1';
			Counter <= Counter + 1;
		elsif Counter < SatValue then
			Saturated <= '0';
			Counter <= Counter + 1;
		else
			Saturated <= '1';
		end if;
	end procedure;

	procedure IncrementSatFull(signal Counter       : inout unsigned;
							   signal Saturated     : out std_logic;
							   constant SatValue    : in unsigned) is
	begin
		if Counter = SatValue - 1 then
			Saturated <= '1';
			Counter <= Counter + 1;
		elsif Counter < SatValue then
			Saturated <= '0';
			Counter <= Counter + 1;
		else
			Saturated <= '1';
		end if;
	end procedure;

	procedure IncrementSatFull(signal Counter       : inout unsigned;
							   constant SatValue    : in integer) is
	begin
		if Counter < SatValue then
			Counter <= Counter + 1;
		end if;
	end procedure;

	procedure IncrementSatFull(signal Counter       : inout unsigned;
							   constant SatValue    : in unsigned) is
	begin
		if Counter < SatValue then
			Counter <= Counter + 1;
		end if;
	end procedure;

	procedure IncrementSatFull(signal Counter       : inout unsigned) is
							   constant WrapValue : unsigned(Counter'range) := (others => '1');
	begin
		if Counter < WrapValue then
			Counter <= Counter + 1;
		end if;
	end procedure;

	-- PROCEDURE: DecrementWrap
	procedure DecrementWrap(signal   Counter   : inout integer;
							constant WrapValue : in    integer) is
	begin
		if Counter <= 0 then
			Counter <= WrapValue - 1;
		else
			Counter <= Counter - 1;
		end if;
	end procedure;

	-- PROCEDURE: DecrementWrapFull
	procedure DecrementWrapFull(signal   Counter   : inout integer;
								constant WrapValue : in    integer) is
	begin
		if Counter <= 0 then
			Counter <= WrapValue;
		else
			Counter <= Counter - 1;
		end if;
	end procedure;

	-- PROCEDURE: WrapMinMax
	procedure WrapMinMax(signal Counter : inout integer;
						 constant Min   : in integer;
						 constant Max   : in integer) is
	begin
		if Counter = Max then
			Counter <= Min;
		else
			Counter <= Counter + 1;
		end if;
	end procedure;

	procedure WrapMinMax(signal Counter : inout unsigned;
						 constant Min   : in integer;
						 constant Max   : in integer) is
	begin
		if Counter = Max then
			Counter <= to_unsigned(Min,Counter'length);
		else
			Counter <= to_unsigned(to_integer(unsigned(Counter)) + 1, Counter'length);
		end if;
	end procedure;


	-- PROCEDURE: toggle
	procedure toggle(signal a : inout std_logic) is
	begin
		a <= not a;
	end procedure;

	procedure toggle(signal a : inout std_logic_vector) is
	begin
		a <= not a;
	end procedure;

	procedure toggle(signal a : inout boolean) is
	begin
		a <= not a;
	end procedure;

	-- FUNCTION: is_logic
	function is_logic(signal slv : in std_logic_vector) return boolean is -- is unlogic if any bit is not 0 or 1
	variable res_slv : boolean := true;
	begin
		for i in slv'range loop
			res_slv := res_slv and ((slv(i) = '0') or (slv(i) = '1'));
		end loop;
		return res_slv;
	end is_logic;

	-- FUNCTION: divide
	--    function  divide(a : unsigned; -- fonte: https://vhdlguru.blogspot.com/2010/03/vhdl-function-for-division-two-signed.html
	--                     b : unsigned) return unsigned is
	--    variable a1 : unsigned(a'length-1 downto 0) := a;
	--    variable b1 : unsigned(b'length-1 downto 0) := b;
	--    variable p1 : unsigned(b'length downto 0) := (others => '0');
	--    variable i  : integer := 0;
	--    begin
	--        for i in 0 to b'length-1 loop
	--                p1(b'length-1 downto 1) := p1(b'length-2 downto 0);
	--                p1(0) := a1(a'length-1);
	--                a1(a'length-1 downto 1) := a1(a'length-2 downto 0);
	--                p1 := p1-b1;
	--            if(p1(b'length-1) ='1') then
	--                a1(0) :='0';
	--                p1 := p1+b1;
	--            else
	--                a1(0) :='1';
	--            end if;
	--        end loop;
	--        return a1;
	--    end divide;

	-- FUNCTION: min
	function min(signal a : in integer;
	signal b : in integer) return integer is
	variable res : integer := 0;
	begin
		if a < b then
			res := a;
		else
			res := b;
		end if;
		return res;
	end min;

	-- FUNCTION: max
	function max(signal a : in integer;
	signal b : in integer) return integer is
	variable res : integer := 0;
	begin
		if a > b then
			res := a;
		else
			res := b;
		end if;
		return res;
	end max;

	-- FUNCTION: number_of_zeros
	function number_of_zeros(x: std_logic_vector) return integer is
	variable zero   : integer := 0;
	begin
		-- Conta '1'
		for i in x'low to x'high loop
			if x(i) = '0' then
				zero := zero + 1;
			end if;
		end loop;

		return zero;

	end number_of_zeros;

end kratus_pkg;
