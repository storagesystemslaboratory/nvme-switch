--------------------------------------------------------------------------------
--
-- Module Name: rtl_ffd
--
-- Company: ittChip - Unisinos
-- Engineer: Rodrigo Marques de Figueiredo
--			 & Vinícius Gabriel Linden
-- 
-- Description: Flip-flip D implementation
-- 
--------------------------------------------------------------------------------
-- TODO:

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
--use IEEE.STD_LOGIC_UNSIGNED.ALL;

library UNISIM;
use UNISIM.VComponents.ALL;

library work;
use work.common_pkg.all;

entity rtl_ffd is
	generic
	(
		DATA_WIDTH		: integer := 4; -- width of data
		FORCE_IOB		: boolean := false; -- forces the positioning in IOB
		CLOCK_EDGE		: std_logic := '1' -- '1' for rising, '0' for falling
	);
	port
	(
		din				: in std_logic_vector(DATA_WIDTH-1 downto 0); -- input
		dout 			: out std_logic_vector(DATA_WIDTH-1 downto 0); -- output
		aclr 			: in std_logic; -- assynchrounous clear
		apre			: in std_logic; -- assynchronous preset
		clk_en 			: in std_logic; -- clock enable
		clk 			: in std_logic
	);
end rtl_ffd;

architecture rtl of rtl_ffd is

	attribute iob : string; -- Xilinx attribute

	impure function bool2str(bool : boolean) return string is
	begin
		if bool then
			return "true";
		else
			return "auto";
		end if;
	end bool2str;

	constant IOB_STR : string := bool2str(FORCE_IOB);

begin

	FFD_GEN : for n in 0 to DATA_WIDTH-1 generate

		RISING_GEN : if CLOCK_EDGE = '1' generate
			attribute iob of ffd_int : label is IOB_STR;
		begin
			FFD_INT : fdcpe
			generic map
			(
				INIT => '0'
			)
			port map (
				q => dout(n),
				c => clk,
				ce => clk_en,
				clr => aclr,
				d => din(n),
				pre => apre
			);
		end generate RISING_GEN;

		FALLING_GEN : if CLOCK_EDGE = '0' generate
			attribute iob of ffd_int : label is IOB_STR;
		begin
			FFD_INT : fdcpe_1
			generic map
			(
				INIT => '0'
			)
			port map
			(
				q => dout(n),
				c => clk,
				ce => clk_en,
				clr => aclr,
				d => din(n),
				pre => apre
			);
		end generate FALLING_GEN;

	end generate FFD_GEN;

end rtl;
