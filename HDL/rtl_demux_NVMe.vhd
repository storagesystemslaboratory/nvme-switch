--------------------------------------------------------------------------------
--
-- Module Name: rtl_demux_NVMe
-- Create Date: 10/03/2019 (MM/DD/YYYY)
-- Revision: 0
--
-- Company: ittChip - Unisinos
-- Engineer: Vinícius Gabriel LINDEN
-- 
-- Description: Real-time logic demultiplexer for the NVMe stream convention.
--              Demultiplexer means here that it has two NVMe stream destinations
--              & only one source.
--              "destin1" is selected when sel = '0'; "destin2", sel = '1'.
-- 
--------------------------------------------------------------------------------
-- TODO:

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

library UNISIM;
use UNISIM.VComponents.all;

--library work;
--use work.common_pkg.all;

entity rtl_demux_NVMe is
	port (
			 stream_source  : in std_logic_vector(7 downto 0);
			 valid_source        : in std_logic;
			 ready_source        : out std_logic;

			 stream_destin1 : out std_logic_vector(7 downto 0);
			 valid_destin1       : out std_logic;
			 ready_destin1       : in std_logic;

			 stream_destin2 : out std_logic_vector(7 downto 0);
			 valid_destin2       : out std_logic;
			 ready_destin2       : in std_logic;

			 sel                 : in std_logic
		 );
end rtl_demux_NVMe;

architecture rtl of rtl_demux_NVMe is

begin

	DEMUX_NVME_STREAM : entity work.rtl_demux_2
	generic map(
				   DATA_WIDTH => 8,
				   NO_IDLE_VALUE => true,
				   UNSELECTED_VALUE_1 => "00000000", -- doesn't matter
				   UNSELECTED_VALUE_2 => "00000000" -- doesn't matter
			   )
	port map (
				 sel => sel,
				 iput => stream_source,
				 oput1 => stream_destin1,
				 oput2 => stream_destin2
			 );

	DEMUX_VALID : entity work.rtl_demux_2
	generic map(
				   DATA_WIDTH => 1,
				   NO_IDLE_VALUE => false,
				   UNSELECTED_VALUE_1 => "0",
				   UNSELECTED_VALUE_2 => "0"
			   )
	port map (
				 sel => sel,
				 iput(0) => valid_source,
				 oput1(0) => valid_destin1,
				 oput2(0) => valid_destin2
			 );

	MUX_READY : entity work.rtl_mux_2
	generic map(
				   DATA_WIDTH => 1
			   )
	port map (
				 sel => sel,
				 iput1(0) => ready_destin1,
				 iput2(0) => ready_destin2,
				 oput(0) => ready_source
			 );

end rtl;


