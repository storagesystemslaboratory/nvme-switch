--------------------------------------------------------------------------------
--
-- Module Name: sim_arbitrator_comp_config
-- Create Date: 09/19/2019 (MM/DD/YYYY)
--
-- Company: ittChip - Unisinos
-- Engineer: Vinícius Gabriel LINDEN
-- 
-- Description: Stimulae arbitrator_comp_config
-- 
--------------------------------------------------------------------------------
-- Recommended span of 300 ns for period of 10 ns
-- TODO:


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
--use IEEE.NUMERIC_STD.ALL;

library UNISIM;
use UNISIM.VComponents.all;

library work;
use work.NVMe_pkg.all;
--use work.common_pkg.all;
--use work.kratus_pkg.all;

entity sim_arbitrator_comp_config is
	end sim_arbitrator_comp_config;


architecture Behavioral of sim_arbitrator_comp_config is

	-- DO NOT DELETE
	constant period                 : time := 5 ns;
	constant hp                     : time := period/2; -- half period

	signal index                    : natural := 1; -- index for redability
	signal clk                      : std_logic := '1'; -- system base clock

	procedure wait_clk(constant T : in natural) is
	begin
		wait for t*period;
	end procedure;
	-- DELETION RESTRICTION ENDED

	-- UUT
	signal config_ac                : std_logic_vector(7 downto 0) := (others => '0');
	signal config_comparison        : std_logic_vector(7 downto 0);
	signal value1                   : std_logic_vector(63 downto 0);
	signal value2                   : std_logic_vector(63 downto 0);
	-- END OF UUT

	-- SIMULATION
	signal test_case                : string(1 to 20) := "waiting             ";
-- END OF SIMULATION

begin

	UUT : entity work.rtl_arbitrator_comp_config
	port map (
				 config_ac => config_ac,
				 config_comparison => config_comparison,
				 value1 => value1,
				 value2 => value2
			 );

	-- DO NOT DELETE
	clk <= not clk after hp;
	index <= index + 1 after period;
	-- DELETION RESTRICTION ENDED

	process
	begin
		wait_clk(3);

		test_case <= "clear               ";
		wait_clk(10);

		test_case <= "test each config    ";
		config_ac <= AC_IDLE;
		wait_clk(5);
		config_ac <= AC_SEND_GENERIC;
		wait_clk(5);
		config_ac <= AC_SEND_SSD;
		wait_clk(5);
		config_ac <= AC_TEST;
		wait_clk(5);

		test_case <= "clear               ";
		wait_clk(10);

		test_case <= "simulation ended    ";
		wait;
	end process;

end Behavioral;


