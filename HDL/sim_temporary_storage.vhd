--------------------------------------------------------------------------------
--
-- Module Name: sim_temporary_storage
-- Create Date: 09/11/2019 (MM/DD/YYYY)
--
-- Company: ittChip - Unisinos
-- Engineer: Vinícius Gabriel LINDEN
-- 
-- Description: Stimulae temporary_storage
-- 
--------------------------------------------------------------------------------
-- Recommended span of 300 ns for period of 10 ns
-- TODO:


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library UNISIM;
use UNISIM.VComponents.all;

--library STD;
--use STD.TEXTIO.ALL;

library work;
--use work.common_pkg.all;
use work.kratus_pkg.all;

entity sim_temporary_storage is
	end sim_temporary_storage;


architecture Behavioral of sim_temporary_storage is

	-- DO NOT DELETE
	constant period                 : time := 5 ns;
	constant hp                     : time := period/2; -- half period

	signal index                    : natural := 1; -- index for redability
	signal clk                      : std_logic := '1'; -- system base clock

	procedure wait_clk(constant T : in natural) is
	begin
		wait for t*period;
	end procedure;
	-- DELETION RESTRICTION ENDED

	-- UUT
	constant DATA_WIDTH         : natural := 64;
	constant CLEAR_ON_BOTH      : boolean := true;
	constant CLOCK_EDGE         : std_logic := '1';
	signal config_updated       : std_logic := '0';
	signal stream          : std_logic_vector(7 downto 0) := (others => '0');
	signal trigger              : std_logic := '0';
	signal count                : std_logic_vector(5 downto 0) := (others => '0');
	signal clear                : std_logic := '0';
	signal field                : std_logic_vector(DATA_WIDTH-1 downto 0);
	signal rst                  : std_logic := '1';
	signal error                : std_logic;
	-- END OF UUT

	-- SIMULATION
	signal test_case                : string(1 to 20) := "waiting             ";
	signal data                     : unsigned(7 downto 0) := x"ac";
-- END OF SIMULATION

begin

	UUT : entity work.rtl_temporary_storage
	generic map (
		--DATA_WIDTH => DATA_WIDTH,
					CLEAR_ON_BOTH => CLEAR_ON_BOTH,
					CLOCK_EDGE => CLOCK_EDGE
				)
	port map (
				 config_updated => config_updated,
				 stream => stream,
				 trigger => trigger,
				 count => count,
				 clear => clear,
				 field => field,
				 rst => rst,
				 error => error,
				 clk => clk
			 );

	-- DO NOT DELETE
	clk <= not clk after hp;
	index <= index + 1 after period;
	-- DELETION RESTRICTION ENDED

	-- SEQUENTIAL DATA GENERATOR
	process
	begin
		wait for period;
		IncrementWrapFull(data);
	end process;
	stream <= std_logic_vector(data);
	-- END OF SEQUENTIAL DATA GENERATOR

	process
	begin
		wait_clk(2);
		rst <= '0';
		wait_clk(2);

		test_case <= "standard operation  ";
		trigger <= '1';
		wait_clk(1);
		trigger <= '0';
		wait_clk(8);
		toggle(clear);

		test_case <= "clearing            ";
		wait_clk(10);

		test_case <= "missed error        ";
		trigger <= '1';
		wait_clk(1);
		trigger <= '0';
		wait_clk(9); -- field has been captured
		trigger <= '1';
		wait_clk(1);
		trigger <= '0';
		wait_clk(8); -- error missed
		toggle(clear);
		wait_clk(1);
		trigger <= '1';
		wait_clk(1);
		trigger <= '0';
		wait_clk(8); -- returns to normal operation without reset
		toggle(clear);

		test_case <= "clearing            ";
		wait_clk(10);

		test_case <= "clear error         ";
		trigger <= '1';
		wait_clk(1);
		trigger <= '0';
		wait_clk(7); -- field is being captured
		toggle(clear); -- error has occured
		wait_clk(2);
		rst <= '1'; -- error only goes away with reset
		wait_clk(1);
		rst <= '0';

		test_case <= "clearing            ";
		wait_clk(10);

		test_case <= "trigger error       ";
		trigger <= '1';
		wait_clk(1);
		trigger <= '0';
		wait_clk(5); -- field is being captured
		trigger <= '1';
		wait_clk(1); -- process triggers again before it ended
		trigger <= '0';
		wait_clk(8);
		rst <= '1'; -- error only goes away with reset
		wait_clk(1);
		rst <= '0';

		test_case <= "clearing            ";
		wait_clk(10);

		test_case <= "configuration update";
		config_updated <= '1';
		wait_clk(1);
		config_updated <= '0';
		wait_clk(2);
		trigger <= '1';
		wait_clk(1);
		trigger <= '0';
		wait_clk(5); -- field is being captured
		config_updated <= '1';
		wait_clk(1);
		config_updated <= '0';

		test_case <= "clearing            ";
		wait_clk(10);

		test_case <= "simulation ended    ";
		wait;
	end process;

end Behavioral;


