--------------------------------------------------------------------------------
--
-- Module Name: rtl_mux_2
-- Create Date: 08/26/2019 (MM/DD/YYYY)
-- Revision: 0
--
-- Company: ittChip - Unisinos
-- Engineer: Vinícius Gabriel LINDEN
-- 
-- Description: Real-time logic for 2 with W bit width multiplexer
-- 
--------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

library UNISIM;
use UNISIM.VComponents.all;

entity rtl_mux_2 is
	generic
	(
		DATA_WIDTH	: natural := 8 -- I/O width [bit]
	);
	port
	(
		sel         : in std_logic; -- input selection, '0' for 1st, '1' for 2nd
		iput1       : in std_logic_vector(DATA_WIDTH-1 downto 0); -- first input (sel = '0')
		iput2       : in std_logic_vector(DATA_WIDTH-1 downto 0); -- second input (sel = '1')
		oput        : out std_logic_vector(DATA_WIDTH-1 downto 0) -- output
	);
end rtl_mux_2;

architecture rtl of rtl_mux_2 is

begin

	oput <= iput2 when sel = '1' else iput1;

end rtl;

