--------------------------------------------------------------------------------
--
-- Module Name: sim_sub_opcode_counter
-- Create Date: 09/18/2019 (MM/DD/YYYY)
--
-- Company: ittChip - Unisinos
-- Engineer: Vinícius Gabriel LINDEN
-- 
-- Description: Stimulae sub_opcode_counter
-- 
--------------------------------------------------------------------------------
-- Recommended span of 300 ns for period of 10 ns
-- TODO:


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library UNISIM;
use UNISIM.VComponents.all;

library work;
--use work.common_pkg.all;
--use work.kratus_pkg.all;
use work.NVMe_pkg.all;

entity sim_sub_opcode_counter is
	end sim_sub_opcode_counter;


architecture Behavioral of sim_sub_opcode_counter is

	-- DO NOT DELETE
	constant period			: time := 5 ns;
	constant hp				: time := period/2; -- half period

	signal index			: natural := 1; -- index for redability
	signal clk				: std_logic := '1'; -- system base clock

	procedure wait_clk(constant T : in natural) is
	begin
		wait for t*period;
	end procedure;
	-- DELETION RESTRICTION ENDED

	-- UUT
	constant CLOCK_EDGE		: std_logic := '1';
	signal config			: std_logic_vector(7 downto 0) := (others => '0');
	signal config_updated	: std_logic := '0';
	signal valid			: std_logic := '0';
	signal ready			: std_logic := '0';
	signal opcode			: std_logic_vector(7 downto 0) := (others => '0');
	signal result			: std_logic_vector(7 downto 0);
	signal error			: std_logic;
	signal rst				: std_logic := '1';
	-- END OF UUT

	-- SIMULATION
	signal test_case		: string(1 to 20) := "waiting             ";
	signal config_edge		: std_logic_vector(7 downto 0);
-- END OF SIMULATION

begin

	UUT : entity work.rtl_sub_opcode_counter
	generic map
	(
		CLOCK_EDGE => CLOCK_EDGE
	)
	port map
	(
		config => config,
		config_updated => config_updated,
		stream_delayed => x"00", -- not used in sim (only tb)
		valid => valid,
		ready => ready,
		count => "000000", -- not used in sim (only tb)
		opcode => opcode,
		last_delayed => '0', -- not used in sim (only tb)
		result => result,
		error => error,
		rst => rst,
		clk => clk
	);

	-- DO NOT DELETE
	clk <= not clk after hp;
	index <= index + 1 after period;
	-- DELETION RESTRICTION ENDED

	-- CONFIG_UPDATED AUTOMATIC CONTROL
	config_edge <= config after period;
	config_updated <= '1' when config_edge /= config else '0';
	-- END OF CONFIG_UPDATED AUTOMATIC CONTROL

	process
	begin
		wait_clk(2);
		rst <= '0';
		wait_clk(2);

		test_case <= "clear               ";
		wait_clk(10);

		test_case <= "write opcode test   ";
		opcode <= NVME_WRITE;
		config <= OC_WRITE;
		for i in 0 to 5 loop
			wait_clk(5);
			ready <= '1';
			wait_clk(2);
			valid <= '1';
			wait_clk(1);
			ready <= '0';
			valid <= '0';
		end loop;

		test_case <= "clear               ";
		wait_clk(10);

		test_case <= "proprietary opcode t";
		config <= OC_PROPRIETARY;
		for i in 127 to 130 loop -- starts at "01111111", 130 - 127 = 3
			opcode <= std_logic_vector(to_unsigned(i,8));
			wait_clk(1);
			ready <= '1';
			wait_clk(2);
			valid <= '1';
			wait_clk(1);
			ready <= '0';
			valid <= '0';
		end loop;

		test_case <= "clear               ";
		wait_clk(10);

		test_case <= "simulation ended    ";
		wait;
	end process;

end Behavioral;


