--------------------------------------------------------------------------------
--
-- Module Name: rtl_field_capture
-- Create Date: 09/05/2019 (MM/DD/YYYY)
-- Revision: 3
--
-- Company: ittChip - Unisinos
-- Engineer: Vinícius Gabriel LINDEN
--
-- Description: Real-time logic for field capture.
--				This module must be enclosed within the statistics module.
--
--------------------------------------------------------------------------------
-- TODO:

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

library UNISIM;
use UNISIM.VComponents.all;

library work;
use work.common_pkg.all;

entity rtl_field_capture is
	generic
	(
		EN_CLEAR		: boolean := true; -- enables the new_data & clear mechanism
		CONFIG_NUMBER	: integer := 1; -- number of simultanous fields to capture
		CLEAR_ON_BOTH	: boolean := false; -- true: clear is activated on both edge; false: only the CLOCK_EDGE is active
		CLOCK_EDGE		: std_logic := '1' -- '1' for rising, '0' for falling
	);
	port
	(
		-- CONFIGURATION REGISTER
		config			: in std_logic_vector_8(CONFIG_NUMBER-1 downto 0); -- config for field capture
		config_updated	: in std_logic_vector(CONFIG_NUMBER-1 downto 0); -- '1' if new config

		-- FRAMEWORK
		stream_delayed	: in std_logic_vector(7 downto 0); -- byte data size
		opcode			: in std_logic_vector(7 downto 0); -- NVMe command
		count			: in std_logic_vector(5 downto 0); -- byte count
		last_delayed	: in std_logic;

		-- STORAGE INTERFACE
		new_data		: out std_logic_vector(CONFIG_NUMBER-1 downto 0); -- new data to store
		clear			: in std_logic_vector(CONFIG_NUMBER-1 downto 0); -- send new data
		field			: out std_logic_vector_64(CONFIG_NUMBER-1 downto 0); -- captured data

		-- GENERAL PURPOSE
		rst				: in std_logic;
		error			: out std_logic;
		clk				: in std_logic

	);
end rtl_field_capture;

architecture rtl of rtl_field_capture is

	signal error_fc		: std_logic_vector(CONFIG_NUMBER-1 downto 0);
	signal error_fc_red	: std_logic;

begin

	error <= error_fc_red;

	OR_ERROR : entity work.rtl_reduce_operations
	generic map
	(
		DATA_WIDTH => CONFIG_NUMBER,
		OPERATION => 0
	)
	port map
	(
		data => error_fc,
		result => error_fc_red
	);

	SUB_FIELD_CAPTURES : for i in 0 to CONFIG_NUMBER-1 generate

		SUB_FIELD_CAPTURE : entity work.rtl_sub_field_capture
		generic map
		(
			USE_CONFIG => 1,
			EN_CLEAR => EN_CLEAR,
			CLEAR_ON_BOTH => CLEAR_ON_BOTH,
			CLOCK_EDGE => CLOCK_EDGE
		)
		port map
		(
			config => config(i),
			config_updated => config_updated(i),
			stream_delayed => stream_delayed,
			count => count,
			opcode => opcode,
			last_delayed => last_delayed,
			opcode_match => open,
			new_data => new_data(i),
			clear => clear(i),
			field => field(i),
			rst => rst,
			error => error_fc(i),
			clk => clk
		);

	end generate SUB_FIELD_CAPTURES;

end rtl;


