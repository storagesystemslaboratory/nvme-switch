--------------------------------------------------------------------------------
--
-- Module Name: sim_bus_splitter
-- Create Date: 09/18/2019 (MM/DD/YYYY)
--
-- Company: ittChip - Unisinos
-- Engineer: Vinícius Gabriel LINDEN
-- 
-- Description: Stimulae for bus_splitter module
-- 
--------------------------------------------------------------------------------
-- Recommended span of 300 ns for period of 10 ns
-- TODO:


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
--use IEEE.NUMERIC_STD.ALL;

library UNISIM;
use UNISIM.VComponents.all;

library work;
--use work.common_pkg.all;
--use work.kratus_pkg.all;

entity sim_bus_splitter is
	end sim_bus_splitter;


architecture Behavioral of sim_bus_splitter is

	-- DO NOT DELETE
	constant period                 : time := 5 ns;
	constant hp                     : time := period/2; -- half period

	signal index                    : natural := 1; -- index for redability
	signal clk                      : std_logic := '1'; -- system base clock

	procedure wait_clk(constant T : in natural) is
	begin
		wait for t*period;
	end procedure;
	-- DELETION RESTRICTION ENDED

	-- UUT
	constant RAMIFICATIONS      : integer := 2;
	signal valid_source         : std_logic := '0';
	signal ready_source         : std_logic;
	signal valid_destination    : std_logic_vector(RAMIFICATIONS-1 downto 0) := (others => '0');
	signal ready_destination    : std_logic_vector(RAMIFICATIONS-1 downto 0);
	signal halt                 : std_logic := '0';
	-- END OF UUT

	-- SIMULATION
	constant PACKAGE_LENGTH     : integer := 65;
	signal test_case            : string(1 to 20) := "waiting             ";
	signal rst                  : std_logic_vector(RAMIFICATIONS downto 0) := (others => '1'); -- last one is for the source
	signal unload               : std_logic := '0';
-- END OF SIMULATION

begin

	UUT : entity work.rtl_bus_splitter
	generic map (
					RAMIFICATIONS => RAMIFICATIONS
				)
	port map (
				 valid_source => valid_source,
				 ready_source => ready_source,
				 valid_destination => valid_destination,
				 ready_destination => ready_destination,
				 halt => halt
			 );

	-- DO NOT DELETE
	clk <= not clk after hp;
	index <= index + 1 after period;
	-- DELETION RESTRICTION ENDED

	TEST_SOURCE : entity work.rtl_FIFO_NVMe
	generic map(
				   SUBMISSION_CMD => true,
				   SHORT => true
			   )
	port map (
				 stream_wr => (others => '0'),
				 valid_wr => '1',
				 ready_wr => open,
				 stream_rd => open,
				 valid_rd => valid_source,
				 ready_rd => ready_source,
				 full => open,
				 empty => open,
				 unload => unload,
				 unloaded => open,
				 processing => open,
				 number_pkgs => open,
				 error => open,
				 rst => rst(RAMIFICATIONS),
				 clk => clk
			 );

	TEST_DESTINATION : for i in 0 to RAMIFICATIONS-1 generate

		NVME_STREAM_TESTER : entity work.rtl_field_capture_framework
		generic map (
						CLOCK_EDGE => '1'
					)
		port map (
					 stream => (others => '0'),
					 valid => valid_destination(i),
					 ready => ready_destination(i),
					 stream_delayed => open,
					 opcode => open,
					 count => open,
					 error => open,
					 rst => rst(i),
					 clk => clk
				 );

	end generate TEST_DESTINATION;

	process
	begin
		wait_clk(5);
		rst <= (others => '0');

		test_case <= "clear               ";
		wait_clk(10);

		test_case <= "load cmd(s) 2 source";
		wait_clk(1*PACKAGE_LENGTH);

		test_case <= "clear               ";
		wait_clk(10);

		test_case <= "try until success   ";
		rst(RAMIFICATIONS-1 downto 0) <= (others => '1'); -- reset both destinations
		wait_clk(1);
		unload <= '1'; -- source needs to send cmd
		wait_clk(2);
		unload <= '0';
		wait_clk(2);
		rst(0) <= '0'; -- only the second one is available
		wait_clk(2);
		rst(0) <= '1';
		rst(1) <= '0'; -- only the first one is available
		wait_clk(2);
		rst <= (others => '0'); -- free both destinations, package should unload
		wait_clk(PACKAGE_LENGTH);

		test_case <= "clear               ";
		wait_clk(10);

		test_case <= "halt test           ";
		halt <= '1';
		wait_clk(1);
		unload <= '1';
		wait_clk(2);
		unload <= '0';
		wait_clk(1);
		halt <= '0';
		wait_clk(PACKAGE_LENGTH);

		test_case <= "clear               ";
		wait_clk(10);

		test_case <= "simulation ended    ";
		wait;
	end process;

end Behavioral;


