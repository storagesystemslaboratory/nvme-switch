--------------------------------------------------------------------------------
--
-- Module Name: sim_activate_capture
-- Create Date: 01/22/2019 (MM/DD/YYYY)
--
-- Company: ittChip - Unisinos
-- Engineer: Vinícius Gabriel LINDEN
-- 
-- Description: Stimulae activate capture submodule
-- 
--------------------------------------------------------------------------------
-- Recommended span of 300 ns for period of 10 ns
-- TODO:


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library UNISIM;
use UNISIM.VComponents.all;

--library STD;
--use STD.TEXTIO.ALL;

library work;
use work.common_pkg.all;
use work.NVMe_pkg.all;

entity sim_activate_capture is
end sim_activate_capture;


architecture Behavioral of sim_activate_capture is

	-- DO NOT DELETE
	constant period					: time := 5 ns;
	constant hp						: time := period/2; -- half period

	signal index					: natural := 1; -- index for redability
	signal clk						: std_logic := '1'; -- system base clock

	procedure wait_clk(constant T : in natural) is
	begin
		wait for t*period;
	end procedure;
	-- DELETION RESTRICTION ENDED

	-- UUT
	constant USE_CONFIG				: natural := 3;
	signal config 					: std_logic_vector(7 downto 0) := (others => '0');
	signal opcode 					: std_logic_vector(7 downto 0) := (others => '0');
	signal count 					: std_logic_vector(5 downto 0);
	signal opcode_match				: std_logic;
	signal trigger_capture 			: std_logic;
	signal field_mask 				: std_logic_vector(63 downto 0);
	-- END OF UUT

	-- SIMULATION
	constant OPCODE_LIST			: std_logic_vector_8 := (
		0 => NVME_FLUSH, 1 => NVME_WRITE, 2 => NVME_READ, 3 => NVME_WRITE_UNCORRECTABLE,
		4 => NVME_COMPARE, 5 => NVME_WRITE_ZEROS, 6 => NVME_DATASET_MANAGEMENT,
		7 => NVME_RESERVATION_REGISTER, 8 => NVME_RESERVATION_REPORT,
		9 => NVME_RESERVATION_ACQUIRE, 10 => NVME_RESERVATION_RELEASE, 11 => NVME_RANDOM);
	constant FC_CONFIG_LIST			: std_logic_vector_8 := (
		0 => FC_IDLE, 1 => FC_SLBA, 2 => FC_PRP1, 3 => FC_PRP2, 4 => FC_FUA, 5 => FC_DSM,
		6 => FC_TEST);
	constant OC_CONFIG_LIST			: std_logic_vector_8 := (
		0 => OC_IDLE, 1 => OC_WRITE, 2 => OC_READ, 3 => OC_PROPRIETARY, 4 => OC_RD_RANGE,
		5 => OC_RW_RANGE);
	constant AC_CONFIG_LIST			: std_logic_vector_8 := (
		0 => AC_IDLE, 1 => AC_SEND_GENERIC, 2 => AC_SEND_SSD, 3 => AC_TEST);
	-- END OF SIMULATION

begin

	UUT : entity work.rtl_activate_capture
	generic map
	(
		USE_CONFIG => USE_CONFIG
	)
	port map
	(
		config => config,
		opcode => opcode,
		count => count,
		opcode_match => opcode_match,
		trigger_capture => trigger_capture,
		field_mask => field_mask
	);

	-- DO NOT DELETE
	clk <= not clk after hp;
	index <= index + 1 after period;
	-- DELETION RESTRICTION ENDED

	TEST_FC : if USE_CONFIG = 1 generate
		process
		begin
			report "simulation began";
			for config_loop in FC_CONFIG_LIST'range loop
				config <= FC_CONFIG_LIST(config_loop);
				for opcode_loop in OPCODE_LIST'range loop
					opcode <= OPCODE_LIST(opcode_loop);
					for byte_loop in 0 to 63 loop
						count <= std_logic_vector(to_unsigned(byte_loop,count'length));
						wait_clk(1);
					end loop;
				end loop;
			end loop;
			report "simulation ended";
			wait;
		end process;
	end generate TEST_FC;

	TEST_OC : if USE_CONFIG = 2 generate
		process
		begin
			for config_loop in OC_CONFIG_LIST'range loop
				config <= OC_CONFIG_LIST(config_loop);
				for opcode_loop in OPCODE_LIST'range loop
					opcode <= OPCODE_LIST(opcode_loop);
					for byte_loop in 0 to 63 loop
						count <= std_logic_vector(to_unsigned(byte_loop,count'length));
						wait_clk(1);
					end loop;
				end loop;
			end loop;
			report "simulation ended";
			wait;
		end process;
	end generate TEST_OC;

	TEST_AC : if USE_CONFIG = 3 generate
		process
		begin
			for config_loop in AC_CONFIG_LIST'range loop
				config <= AC_CONFIG_LIST(config_loop);
				for opcode_loop in OPCODE_LIST'range loop
					opcode <= OPCODE_LIST(opcode_loop);
					for byte_loop in 0 to 63 loop
						count <= std_logic_vector(to_unsigned(byte_loop,count'length));
						wait_clk(1);
					end loop;
				end loop;
			end loop;
			report "simulation ended";
			wait;
		end process;
	end generate TEST_AC;

end Behavioral;


