--------------------------------------------------------------------------------
--
-- Module Name: sim_assertion_comp_eval
-- Create Date: 10/31/2019 (MM/DD/YYYY)
--
-- Company: ittChip - Unisinos
-- Engineer: Vinícius Gabriel LINDEN
-- 
-- Description: Stimulae assertion_comp_eval
-- 
--------------------------------------------------------------------------------
-- Recommended span of 300 ns for period of 10 ns
-- TODO:


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
--use IEEE.NUMERIC_STD.ALL;

library UNISIM;
use UNISIM.VComponents.all;

library work;
use work.NVMe_pkg.all;
use work.common_pkg.all;
--use work.kratus_pkg.all;

entity sim_assertion_comp_eval is
	end sim_assertion_comp_eval;


architecture Behavioral of sim_assertion_comp_eval is

	-- DO NOT DELETE
	constant period                 : time := 5 ns;
	constant hp                     : time := period/2; -- half period

	signal index                    : natural := 1; -- index for redability
	signal clk                      : std_logic := '1'; -- system base clock

	procedure wait_clk(constant T : in natural) is
	begin
		wait for t*period;
	end procedure;
	-- DELETION RESTRICTION ENDED

	-- UUT
	constant DEFAULT_DIRECTION      : std_logic := TO_SSD;
	signal config_ac                : std_logic_vector(7 downto 0) := (others => '0');
	signal opcode                   : std_logic_vector(7 downto 0) := NVME_READ;
	signal direction                : std_logic := '0';
	signal eval_direction           : std_logic;
	-- END OF UUT

	-- SIMULATION
	signal test_case 				: string(1 to 20) := "waiting             ";
	constant opcode_list 			: std_logic_vector_8(10 downto 0) := (0 => NVME_FLUSH,
			1 => NVME_WRITE,
			2 => NVME_READ,
			3 => NVME_WRITE_UNCORRECTABLE,
			4 => NVME_COMPARE,
			5 => NVME_WRITE_ZEROS,
			6 => NVME_DATASET_MANAGEMENT,
			7 => NVME_RESERVATION_REGISTER,
			8 => NVME_RESERVATION_REPORT,
			9 => NVME_RESERVATION_ACQUIRE,
			10 => NVME_RESERVATION_RELEASE);
		--when others => (others => '0'));
	-- END OF SIMULATION

begin

	UUT : entity work.rtl_assertion_comp_eval
	generic map
	(
		DEFAULT_DIRECTION => DEFAULT_DIRECTION
	)
	port map
	(
		config_ac => config_ac,
		opcode => opcode,
		direction => direction,
		eval_direction => eval_direction
	);

	-- DO NOT DELETE
	clk <= not clk after hp;
	index <= index + 1 after period;
	-- DELETION RESTRICTION ENDED

	direction <= clk;

	process
	begin
		wait_clk(3);

		test_case <= "clear               ";
		wait_clk(10);

		for i in opcode_list'range loop
			test_case <= "test each config FL ";
			opcode <= opcode_list(i);
			config_ac <= AC_IDLE;
			wait_clk(5);
			config_ac <= AC_SEND_GENERIC;
			wait_clk(5);
			config_ac <= AC_SEND_SSD;
			wait_clk(5);
			config_ac <= AC_TEST;
			wait_clk(5);

			test_case <= "clear               ";
			wait_clk(10);
		end loop;

		test_case <= "simulation ended    ";
		wait;
	end process;

end Behavioral;


