--------------------------------------------------------------------------------
--
-- Module Name: sim_assertion
-- Create Date: 09/19/2019 (MM/DD/YYYY)
--
-- Company: ittChip - Unisinos
-- Engineer: Vinícius Gabriel LINDEN
-- 
-- Description: Stimulae for the assertion module
-- 
--------------------------------------------------------------------------------
-- Recommended span of 300 ns for period of 10 ns
-- TODO:


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library UNISIM;
use UNISIM.VComponents.all;

library work;
use work.NVMe_pkg.all;

entity sim_assertion is
end sim_assertion;

architecture Behavioral of sim_assertion is

	-- DO NOT DELETE
	constant period					: time := 5 ns;
	constant hp						: time := period/2; -- half period

	signal index					: natural := 1; -- index for redability
	signal clk						: std_logic := '1'; -- system base clock

	procedure wait_clk(constant T : in natural) is
	begin
		wait for t*period;
	end procedure;
	-- DELETION RESTRICTION ENDED

	-- UUT
	constant CLOCK_EDGE 			: std_logic := '1';
	signal config 					: std_logic_vector(7 downto 0) := (others => '0');
	signal config_updated 			: std_logic := '0';
	signal opcode 					: std_logic_vector(7 downto 0) := NVME_FLUSH;
	signal evaluate 				: std_logic := '0';
	signal field 					: std_logic_vector(63 downto 0) := (others => '0');
	signal cmd_id 					: std_logic_vector(15 downto 0) := (others => '0');
	signal direction 				: std_logic;
	signal cmd_id_adm 				: std_logic_vector(15 downto 0);
	signal new_available 			: std_logic;
	signal next_direction 			: std_logic := '0';
	signal error 					: std_logic;
	signal rst 						: std_logic := '1';
	-- END OF UUT

	-- SIMULATION
	signal test_case 				: string(1 to 20) := "waiting             ";
	signal config_edge 				: std_logic_vector(7 downto 0) := (others => '0'); -- edge detection
	constant OFFSET 				: unsigned := x"07060504030200FC";
	constant ZEROS 					: std_logic_vector(9 downto 0) := (others => '0');
-- END OF SIMULATION

begin

	UUT : entity work.rtl_assertion
	generic map
	(
		CLOCK_EDGE => CLOCK_EDGE
	)
	port map
	(
		config => config,
		config_updated => config_updated,
		opcode => opcode,
		evaluate => evaluate,
		field => field,
		cmd_id => cmd_id,
		direction => direction,
		cmd_id_adm => cmd_id_adm,
		new_available => new_available,
		next_direction => next_direction,
		error => error,
		rst => rst,
		clk => clk
	);

	-- DO NOT DELETE
	clk <= not clk after hp;
	index <= index + 1 after period;
	-- DELETION RESTRICTION ENDED

	-- CONFIG_UPDATED AUTOMATIC CONTROL
	config_edge <= config after period;
	config_updated <= '1' when config_edge /= config else '0';
	-- END OF CONFIG_UPDATED AUTOMATIC CONTROL

	-- SEQUENTIAL COMMAND ID GENERATOR
	process
	begin
		for i in 0 to 65535 loop
			wait for period;
			cmd_id <= std_logic_vector(to_unsigned(i,16));
		end loop;
	end process;
	-- END OF SEQUENTIAL COMMAND ID GENERATOR

	process
	begin
		report "simulation started";
		wait_clk(2);
		rst <= '0';
		wait_clk(2);

		test_case <= "clear               ";
		wait_clk(10);

		test_case <= "idle        flush   ";
		wait_clk(1);
		report test_case;
		opcode <= NVME_FLUSH;
		config <= AC_IDLE;
		wait_clk(4);
		evaluate <= '1';
		wait_clk(1);
		evaluate <= '0';
		field(7 downto 0) <= "00000001";
		wait_clk(6);
		assert new_available = '1' report "new_available" severity warning;
		next_direction <= '1';
		wait_clk(1);
		next_direction <= '0';
		wait_clk(1);
		assert direction = TO_SSD report "direction" severity warning;

		test_case <= "clear               ";
		wait_clk(10);

		test_case <= "send to gen flush   ";
		wait_clk(1);
		report test_case;
		config <= AC_SEND_GENERIC;
		wait_clk(2);
		evaluate <= '1';
		wait_clk(1);
		evaluate <= '0';
		wait_clk(5);
		assert new_available = '1' report "new_available" severity warning;
		next_direction <= '1';
		wait_clk(1);
		next_direction <= '0';
		wait_clk(1);
		assert direction = TO_GENERIC report "direction" severity warning;

		test_case <= "clear               ";
		wait_clk(10);

		test_case <= "send to ssd flush   ";
		wait_clk(1);
		report test_case;
		config <= AC_SEND_SSD;
		wait_clk(2);
		wait_clk(2);
		evaluate <= '1';
		wait_clk(1);
		evaluate <= '0';
		wait_clk(1);
		wait_clk(5);
		assert new_available = '1' report "new_available" severity warning;
		next_direction <= '1';
		wait_clk(1);
		next_direction <= '0';
		wait_clk(1);
		assert direction = TO_SSD report "direction" severity warning;

		test_case <= "clear               ";
		wait_clk(10);

		test_case <= "test conf flush     ";
		wait_clk(1);
		report test_case;
		config <= AC_TEST;
		for i in 1 to 8 loop
			wait_clk(3);
			evaluate <= '1';
			wait_clk(1);
			evaluate <= '0';
			field <= std_logic_vector(to_unsigned(i,64) + OFFSET);
			wait_clk(1);
			wait_clk(5);
			assert new_available = '1' report "new_available" severity warning;
			next_direction <= '1';
			wait_clk(1);
			next_direction <= '0';
			wait_clk(1);
			assert direction = TO_SSD report "direction" severity warning;
		end loop;

		test_case <= "clear               ";
		wait_clk(10);

		------------READ

		test_case <= "idle        read    ";
		wait_clk(1);
		report test_case;
		opcode <= NVME_READ;
		config <= AC_IDLE;
		wait_clk(4);
		evaluate <= '1';
		wait_clk(1);
		evaluate <= '0';
		field(7 downto 0) <= "00000001";
		wait_clk(6);
		assert new_available = '1' report "new_available" severity warning;
		next_direction <= '1';
		wait_clk(1);
		next_direction <= '0';
		wait_clk(1);
		assert direction = TO_SSD report "direction" severity warning;

		test_case <= "clear               ";
		wait_clk(10);

		test_case <= "send to gen read    ";
		wait_clk(1);
		report test_case;
		config <= AC_SEND_GENERIC;
		wait_clk(2);
		evaluate <= '1';
		wait_clk(1);
		evaluate <= '0';
		wait_clk(5);
		assert new_available = '1' report "new_available" severity warning;
		next_direction <= '1';
		wait_clk(1);
		next_direction <= '0';
		wait_clk(1);
		assert direction = TO_GENERIC report "direction" severity warning;

		test_case <= "clear               ";
		wait_clk(10);

		test_case <= "send to ssd read    ";
		wait_clk(1);
		report test_case;
		config <= AC_SEND_SSD;
		wait_clk(2);
		wait_clk(2);
		evaluate <= '1';
		wait_clk(1);
		evaluate <= '0';
		wait_clk(1);
		wait_clk(5);
		assert new_available = '1' report "new_available" severity warning;
		next_direction <= '1';
		wait_clk(1);
		next_direction <= '0';
		wait_clk(1);
		assert direction = TO_SSD report "direction" severity warning;

		test_case <= "clear               ";
		wait_clk(10);

		test_case <= "test conf flush     ";
		wait_clk(1);
		report test_case;
		config <= AC_TEST;
		-- FIRST TEST
		wait_clk(3);
		evaluate <= '1';
		wait_clk(1);
		evaluate <= '0';
		field <= std_logic_vector(unsigned(AC_TEST_MIN) - 1);
		wait_clk(1);
		wait_clk(5);
		assert new_available = '1' report "new_available" severity warning;
		next_direction <= '1';
		wait_clk(1);
		next_direction <= '0';
		wait_clk(1);
		assert direction = TO_SSD report "direction" severity warning;
		wait_clk(1);
		-- SECOND TEST
		config <= AC_TEST;
		wait_clk(3);
		evaluate <= '1';
		wait_clk(1);
		evaluate <= '0';
		field <= AC_TEST_MIN;
		wait_clk(1);
		wait_clk(5);
		assert new_available = '1' report "new_available" severity warning;
		next_direction <= '1';
		wait_clk(1);
		next_direction <= '0';
		wait_clk(1);
		assert direction = TO_GENERIC report "direction" severity warning;
		-- THIRD TEST
		config <= AC_TEST;
		wait_clk(3);
		evaluate <= '1';
		wait_clk(1);
		evaluate <= '0';
		field <= AC_TEST_MAX;
		wait_clk(1);
		wait_clk(5);
		assert new_available = '1' report "new_available" severity warning;
		next_direction <= '1';
		wait_clk(1);
		next_direction <= '0';
		wait_clk(1);
		assert direction = TO_GENERIC report "direction" severity warning;
		-- FOURTH TEST
		wait_clk(3);
		evaluate <= '1';
		wait_clk(1);
		evaluate <= '0';
		field <= std_logic_vector(unsigned(AC_TEST_MAX) + 1);
		wait_clk(1);
		wait_clk(5);
		assert new_available = '1' report "new_available" severity warning;
		next_direction <= '1';
		wait_clk(1);
		next_direction <= '0';
		wait_clk(1);
		assert direction = TO_SSD report "direction" severity warning;
		wait_clk(1);

		test_case <= "clear               ";
		wait_clk(10);

		test_case <= "simulation ended    ";
		report "simulation ended";
		wait;
	end process;

end Behavioral;


