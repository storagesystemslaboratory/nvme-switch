--------------------------------------------------------------------------------
--
-- Module Name: sim_mux_2
-- Create Date: 08/27/2019 (MM/DD/YYYY)
--
-- Company: ittChip - Unisinos
-- Engineer: Vinícius Gabriel LINDEN
-- 
-- Description: Stimulae for mux_2
-- 
--------------------------------------------------------------------------------
-- Recommended span of 300 ns for period of 10 ns
-- TODO:


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
--use IEEE.NUMERIC_STD.ALL;

library UNISIM;
use UNISIM.VComponents.all;

--library STD;
--use STD.TEXTIO.ALL;

--library work;
--use work.common_pkg.all;
--use work.kratus_pkg.all;

entity sim_mux_2 is
end sim_mux_2;


architecture Behavioral of sim_mux_2 is

	-- DO NOT DELETE
	constant period                 : time := 5 ns;
	constant hp                     : time := period/2; -- half period

	signal index                    : natural := 1; -- index for redability
	signal clk                      : std_logic := '1'; -- system base clock

	procedure wait_clk(constant T : in natural) is
	begin
		wait for t*period;
	end procedure;
	-- DELETION RESTRICTION ENDED

	-- SIMULATION
	-- END OF SIMULATION

	-- UUT
	constant DATA_WIDTH     : natural := 2;
	signal sel              : std_logic;
	signal iput1            : std_logic_vector(DATA_WIDTH-1 downto 0);
	signal iput2            : std_logic_vector(DATA_WIDTH-1 downto 0);
	signal oput             : std_logic_vector(DATA_WIDTH-1 downto 0);
-- END OF UUT

begin

	UUT : entity work.rtl_mux_2
	generic map
	(
		DATA_WIDTH => DATA_WIDTH
	)
	port map
	(
		sel => sel,
		iput1 => iput1,
		iput2 => iput2,
		oput => oput
	);

	-- DO NOT DELETE
	clk <= not clk after hp;
	index <= index + 1 after period;
	-- DELETION RESTRICTION ENDED

	process
	begin
		iput1 <= (others => '0');
		iput2 <= (others => '0');
		sel <= '0';
		wait_clk(1);
		iput1(0) <= '1';
		wait_clk(1);
		iput2(0) <= '1';
		wait_clk(1);
		sel <= '1';
		wait_clk(1);
		iput1(0) <= '0';
		wait_clk(1);
		iput2(0) <= '0';
		wait;
	end process;

end Behavioral;


