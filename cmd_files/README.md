
# INTRODUCTION

This file gives information about each packet and describes the expected output for typical tests

|    Title    |  Function   |   ID   |        SLBA         | AC_TEST result | OC_RW_RANGE result |
| :---------: | :---------: | :----: | :-----------------: | :------------: | :----------------: |
|    flush    |    flush    | 0xF3C7 |         NA          |      SSD       |         no         |
|    write    |    write    | 0xB690 | 0x7060504_030200FE  |      SSD       |        yes         |
|    read1    |    read     | 0xB296 | 0x92DC3F96_11BDFCD6 |      SSD       |         no         |
|    read2    |    read     | 0xB397 | 0x7060504_03020100  |    generic     |        yes         |
| NVMe_SQ_B01 | proprietary | 0xBB9E |         NA          |      SSD       |         no         |
| NVMe_SQ_B02 | proprietary | 0x2F15 |         NA          |      SSD       |         no         |

One may choose to implement the automatic packet insertion with constants.

For now, SSD = '1' \& generic = '0'

# AC_TEST

OPCODE = READ
x = SLBA

$$
v_1 \leqslant x \leqslant  v_2 \\
v_1 = \text{0x07060504030200FE} \\
v_2 = \text{0x0706050403020102}
$$

# OC_RD_RANGE

OPCODE = READ
x = SLBA

$$
v_1 \leqslant x \leqslant  v_2 \\
v_1 = \text{0x07060504030200FE} \\
v_2 = \text{0x0706050403020102}
$$

# OC_RW_RANGE

OPCODE = READ, WRITE
x = SLBA

$$
v_1 \leqslant x \leqslant  v_2 \\
v_1 = \text{0x07060504030200FE} \\
v_2 = \text{0x0706050403020102}
$$


